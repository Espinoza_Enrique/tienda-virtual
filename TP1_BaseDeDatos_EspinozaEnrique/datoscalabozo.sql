-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-06-2019 a las 01:30:21
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `datoscalabozo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_producto`
--

CREATE TABLE `categoria_producto` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombreCat` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria_producto`
--

INSERT INTO `categoria_producto` (`id`, `nombreCat`) VALUES
(1, 'mesa'),
(2, 'cartas'),
(3, 'miniatura'),
(4, 'accesorios'),
(5, 'rol');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consultas`
--

CREATE TABLE `consultas` (
  `id_consulta` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `motivo` tinyint(3) UNSIGNED NOT NULL,
  `leido` tinyint(1) NOT NULL,
  `consulta` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `consultas`
--

INSERT INTO `consultas` (`id_consulta`, `nombre`, `mail`, `fecha`, `motivo`, `leido`, `consulta`) VALUES
(2, 'fabricio', 'yoyo.87@gmail.com', '2019-06-04', 2, 1, 'Faltan productos'),
(3, 'Rodrigo', 'wilbur.33@gmail.com', '2019-06-11', 2, 1, 'faltan varios productos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_pedido`
--

CREATE TABLE `detalle_pedido` (
  `id_detalle` int(10) UNSIGNED NOT NULL,
  `idOrden` int(10) UNSIGNED NOT NULL,
  `idProducto` int(10) UNSIGNED NOT NULL,
  `cantidad` int(10) UNSIGNED NOT NULL,
  `detalle_precio` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_pedido`
--

INSERT INTO `detalle_pedido` (`id_detalle`, `idOrden`, `idProducto`, `cantidad`, `detalle_precio`) VALUES
(4, 7, 10, 1, '50'),
(5, 8, 11, 1, '300'),
(6, 8, 19, 1, '400'),
(7, 9, 11, 1, '300'),
(8, 10, 19, 1, '400'),
(9, 11, 16, 1, '400'),
(10, 12, 8, 1, '80'),
(11, 13, 20, 1, '500'),
(12, 14, 17, 2, '400'),
(13, 15, 18, 2, '400'),
(14, 17, 20, 1, '500'),
(15, 18, 28, 1, '300'),
(16, 19, 28, 1, '300'),
(17, 19, 10, 2, '50'),
(18, 19, 27, 1, '250'),
(19, 19, 29, 1, '200'),
(20, 19, 30, 1, '200'),
(21, 19, 31, 1, '200'),
(22, 19, 22, 1, '350'),
(23, 19, 23, 1, '10'),
(24, 19, 24, 1, '200'),
(25, 19, 25, 1, '187'),
(26, 19, 26, 1, '250'),
(27, 19, 32, 1, '500'),
(28, 19, 33, 1, '550'),
(29, 19, 34, 1, '600'),
(30, 19, 35, 1, '600'),
(31, 19, 36, 1, '600'),
(32, 19, 37, 1, '350'),
(33, 19, 11, 1, '300'),
(34, 19, 14, 1, '350'),
(35, 19, 16, 1, '400'),
(36, 19, 17, 1, '400'),
(37, 19, 18, 1, '400'),
(38, 19, 19, 1, '400'),
(39, 19, 20, 1, '500'),
(40, 19, 21, 1, '550'),
(41, 19, 38, 1, '500'),
(42, 19, 39, 1, '800'),
(43, 19, 40, 1, '600'),
(44, 19, 41, 1, '400'),
(45, 19, 42, 1, '300'),
(46, 19, 43, 1, '80'),
(47, 19, 44, 1, '80'),
(48, 19, 45, 1, '250'),
(49, 19, 46, 1, '300'),
(50, 19, 47, 1, '40'),
(51, 19, 48, 1, '150'),
(52, 19, 49, 1, '200'),
(53, 19, 50, 1, '80'),
(54, 19, 54, 1, '40'),
(55, 19, 55, 1, '80'),
(56, 19, 57, 1, '200'),
(57, 20, 19, 1, '400'),
(58, 21, 19, 1, '400'),
(59, 22, 58, 1, '1200'),
(60, 22, 14, 1, '350'),
(61, 23, 14, 1, '350'),
(62, 23, 25, 1, '187'),
(63, 24, 14, 1, '350'),
(64, 24, 41, 1, '400'),
(65, 25, 16, 1, '400'),
(66, 25, 50, 1, '80'),
(67, 26, 40, 1, '600'),
(68, 27, 58, 1, '1200');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `motivo_consulta`
--

CREATE TABLE `motivo_consulta` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `nombreMotivo` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `motivo_consulta`
--

INSERT INTO `motivo_consulta` (`id`, `nombreMotivo`) VALUES
(1, 'Quejas'),
(2, 'Productos Faltantes'),
(3, 'Sugerencias'),
(4, 'Consultas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_pedido`
--

CREATE TABLE `orden_pedido` (
  `orden_id` int(10) UNSIGNED NOT NULL,
  `idCliente` int(10) UNSIGNED NOT NULL,
  `orden_fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `orden_pedido`
--

INSERT INTO `orden_pedido` (`orden_id`, `idCliente`, `orden_fecha`) VALUES
(7, 10, '2019-05-25'),
(8, 10, '2019-05-25'),
(9, 10, '2019-05-25'),
(10, 10, '2019-05-25'),
(11, 10, '2019-05-25'),
(12, 10, '2019-05-25'),
(13, 10, '2019-05-28'),
(14, 10, '2019-05-28'),
(15, 10, '2019-05-28'),
(16, 10, '2019-05-28'),
(17, 10, '2019-05-28'),
(18, 10, '2019-06-07'),
(19, 10, '2019-06-09'),
(20, 10, '2019-06-09'),
(21, 10, '2019-06-09'),
(22, 10, '2019-06-09'),
(23, 10, '2019-06-09'),
(24, 10, '2019-06-09'),
(25, 10, '2019-06-09'),
(26, 10, '2019-06-09'),
(27, 19, '2019-06-10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil`
--

CREATE TABLE `perfil` (
  `id` int(10) UNSIGNED NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfil`
--

INSERT INTO `perfil` (`id`, `descripcion`) VALUES
(1, 'Administrador'),
(2, 'Usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `Id` int(10) UNSIGNED NOT NULL COMMENT 'El id que identifica a la persona',
  `apellido` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `dni` int(10) UNSIGNED NOT NULL,
  `mail` varchar(30) NOT NULL,
  `telefono` int(10) UNSIGNED NOT NULL,
  `calle` varchar(50) NOT NULL,
  `altura` smallint(6) NOT NULL,
  `IdPerfil` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`Id`, `apellido`, `nombre`, `dni`, `mail`, `telefono`, `calle`, `altura`, `IdPerfil`) VALUES
(5, 'espinoza', 'kike', 32837262, 'eme.19.87@hotmail.com', 15662458, '', 0, 1),
(10, 'Smith', 'Wilbur', 38987256, 'wilbur.33@gmail.com', 15765324, 'Miriñay', 1234, 2),
(18, 'Lala', 'Ruperto', 15467234, 'rupert87@gmail.com', 15456718, 'Los Sauces', 1698, 2),
(19, 'Elrich', 'Eduardo', 38765234, 'elrich87@gmail.com', 15678906, 'Campos Eliseos', 666, 2),
(20, 'Caballero', 'Wilfredo', 38976213, 'willycaballero@gmail.com', 15456718, 'Dorrego', 791, 2),
(21, 'Olavarria', 'Federico', 76543218, 'fedeola@gmail.com', 15675432, 'Dorrego', 890, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(10) UNSIGNED NOT NULL,
  `nombre_producto` varchar(60) NOT NULL,
  `precio` decimal(10,0) UNSIGNED NOT NULL,
  `fecha` date NOT NULL,
  `subcategoria` int(10) UNSIGNED NOT NULL,
  `nombreFoto` varchar(50) NOT NULL,
  `stock` int(10) UNSIGNED NOT NULL,
  `stockMinimo` int(10) UNSIGNED NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `descripcion` text NOT NULL,
  `garantia` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `nombre_producto`, `precio`, `fecha`, `subcategoria`, `nombreFoto`, `stock`, `stockMinimo`, `estado`, `descripcion`, `garantia`) VALUES
(8, ' Yu-Gi-Oh Soul Fusion', '80', '2019-05-10', 4, 'SoulFusion.jpg', 46, 10, 0, '¡El set de refuerzo de 100 cartas, Soul Fusion, vuelve a las raíces de Yu-Gi-Oh! con nuevas estrategias de Invocación Fusión! Estrategias clásicas del juego recibirán una nueva oportunidad en la victoria, y también hay más cartas nuevas para reforzar las estrategias de los lanzamientos recientes. ¡Aquí hay algo de lo que puedes encontrar en Soul Fusion!\r\n\r\nTwin-Headed Thunder Dragon fue uno de los primeros Monstruos de Fusión, alabado por la facilidad con la que pueden conseguir sus materiales, ¡este monstruo clásico ha renacido para una nueva era! Pero eso no es todo: ¡una nueva familia completa de Thunder Dragons basada en el efecto fuerte pero simple del original llega para agregar una carga a tus duelos!\r\nNuevas cartas de la serie animada Yu-Gi-Oh! VRAINS usadas por Soulburner y su compañero de IA, Flame. Además, el Cyberse Clock Dragon del Playmaker también hace su debut en la vida real. ¡El ATK de este monstruo puede alcanzar los 10.000 si eliges los Materiales Fusion adecuados!\r\n¡12 cartas de Premiere Mundial continúan la historia de los Caballeros Nobles y permiten a los Duelistas regresar a un reino lleno de Peligro!\r\nTambién puede encontrar tarjetas para estrategias de Dark Saviors, Cybernetic Horizon, Structure Deck: Powercode Link, ¡y más!\r\nEl set de refuerzo Soul Fusion contiene en total 100 cartas:\r\n\r\n48 Cartas Comunes\r\n20 Cartas raras\r\n14 Cartas Super Raras\r\n10 Cartas Ultra Raras\r\n8 Raras Secretas\r\n\r\n* Nombre y contenido sujetos a cambios.      ', 180),
(9, ' Yu-Gi-Oh Extreme Force', '70', '2019-05-10', 4, 'ExtremeForce.jpg', 40, 10, 0, ' Para los Duelistas que llegaron tarde a la incorporación de los Monstruos de Enlace en su Mazo Extra, este set tiene varios poderosos nuevos Monstruos de Enlace para ayudarlos a ponerse al día, incluyendo un monstruo LINK-4 que gana efectos adicionales dependiendo del número de monstruos usados ??para vincular su Invocación.\r\n\r\n¡Caballeros del metal vivo que hacen cumplir la ley del planeta máquina! Pueden ser Invocados Especialmente en cualquier columna que ya tenga 2 o más cartas en ella. Estos monstruos y las nuevas cartas “World Legacy” en Extreme Force se combinan para atrapar a tu oponente reduciendo el número de zonas con las que pueden jugar con éxito sus cartas.\r\nInspirado por Mythical Beast Cerberus, un favorito de los duelistas en la vida real y en Yu-Gi-Oh! Duel Links, nuevas bestias míticas emergen de la niebla del tiempo! Al igual que el original Mythical Beast Cerberus, estos monstruos ganan contadores de hechizos cuando se usan hechizos que puedes utilizar para alimentar sus increíbles efectos monstruosos, ¡pero eso no es todo! Esta vez, cada bestia mítica es un monstruo del péndulo con su propio efecto del péndulo!\r\n\r\nAdemás, Extreme Force presenta el Deck de terrores extradimensionales, utilizado por Akira Zaizen en Yu-Gi-Oh! VRAINS, además de añadir más monstruos Cyberse para emparejar con la estructura Deck: Cyberse Link, así como Altergeists y monstruos Rokket para ir con las estrategias que se introdujeron en Circuit Break. Puedes ver cada episodio de la última serie de Yu-Gi-Oh! tan pronto después de que termine su emisión en Japón en http://www.crunchyroll.com/ (Simulcast actualmente disponible en los EEUU y América latina).\r\n ', 180),
(10, ' Yu-Gi-Oh Raging Tempest', '50', '2019-05-10', 4, 'RagingTempest.jpg', 27, 10, 1, ' Raging Tempest presenta tres nuevas estrategias de juego y mejora otras previamente lanzadas en las expansiones Dimension of Chaos, Breakers of Shadow, Hihg-Speed Riders, Wing Raiders, Dragons of Legend -Unleashed-, el Pendulum Domination Structure Deck e incluso un Structure Deck que está por llegar.\r\n\r\nRoyal Spirits que conjuran la fuerza del viento para hacer volar las cartas de tu oponente de su mano o Deck. Estos poderosos Monstruos Rituales funcionan bien con las cartas de Dragons of Legend -Unleashed- y Shining Victories.\r\nAdemás de geniales cartas nuevas para gran una variedad de estrategias, Raging Tempest también tiene muchas poderosas cartas que se pueden utilizar en muchos diferentes Decks. Desde un nuevo tipo de cartas “Virus”, extremadamente potente, que comenzó con el Crush Card Virus de Kaiba a un gigantesco monstruo que puede cambiar la realidad, Raging Tempest seguramente calentará las cosas el 10 de febrero. ¡Aquí está un poco más lo que puedes esperar!\r\n¡Un nuevo equipo de Guerreros Bestias que redefinen la invocación Xyz! Cualquiera de los 4 Monstruos Xyz en esta nueva estrategia de juego puede ser convocado usando cualquier otro monstruo compatible… ¡incluso si no es un Monstruo Xyz! Además, cada uno de los monstruos que no son Xyz de esta nueva estrategia puede añadir su ATK, DEF y efecto especial a los Monstruos Xyz, añadiendo nuevas maneras de agregar materiales Xyz. ¡Sin duda una gran oportunidad para los Duelistas creativos que gustan de inventar nuevas estrategias para convocar a los monstruos más grandes y difíciles posibles!\r\n¡Los Crystrons de Invasion: Vengeance regresan nuevamente para altera la línea de tiempo y esta vez están reescribiendo las reglas de la invocación de sincronía! Normalmente utilizas 1 monstruo tnuer y 1 o más que no lo sean, pero el nuevo monstruo Crynston utiliza más de 1 tuner y exactamente 1 que no lo sea.\r\n¡Los Subterror Behemonths continúan su ataque contra The Hidden City! Los gritos hipnóticos de estos les permiten controlar temporalmente suprimir la interferencia de los efectos de cartas, obligando a Subterror Nemesis Warrior y sus compañeros a luchar solos contra los monstruos.\r\n¡Nuevos ordenes! SPYRAL ha identificado a los líderes del sindicato internacional que han estado investigando, y ahora SPYRAL Super Agent está reuniendo un equipo de agentes para capturarlos. Estas nuevas cartas aprovechan las habilidades de espionaje de élite de SPYRAL, introducen un nuevo gadget y dan a sus agentes la habilidad para traer monstruos enemigos para ser interrogados. ', 90),
(11, ' Yu-Gi-Oh Started Deck Codebr', '300', '2019-05-10', 4, 'Codbreaker.jpg', 0, 10, 1, ' Konami Digital Entertainment Inc, se enorgullece de ofrecerle en presentar el nuevo Starter Deck: Codebreaker. Este nuevo mazo presenta los conceptos básicos de Duelo con cartas que son fuertes y fáciles de entender. Starter Deck: Codebreaker incluye incorporaciones como Linkuriboh, anteriormente inédito en los territorios de América Latina y Europa.\r\n\r\nLos Link Monsters son la última incorporación al juego de cartas Yu-Gi-Oh!, ofreciendo a los Duelistas una nueva forma para demostrar sus habilidades al dominar no solo cómo juegan sus cartas, sino también dónde las juegan. Starter Deck: Codebreaker contiene 5 Link Monsters para dar una ventaja a los nuevos Duelistas en la competencia.\r\n\r\nPlataforma de inicio: Codebreaker contiene 45 cartas:\r\n\r\n2 Ultra Rares\r\n3 Super Rares\r\n40 Comunes ', 7),
(12, ' Yu-Gi-Oh! Flames of Destructi', '90', '2019-05-10', 4, 'FlamesOfDestruction.jpg', 30, 10, 0, '¡Ib the World Chalice Priestess, conocida por sus habilidades protectoras, se vuelve hacia el lado oscuro y arruina su mundo con 6 encarnaciones de destrucción! Estos nuevos Monstruos de Enlace tienen Materiales de Enlace flexibles que te permiten usarlos en cualquier Deck, y cada uno tiene dos habilidades: una que utilizas cuando invocas Link al monstruo y otra que siempre está activa. Estos poderes van desde recuperar cualquier carta de hechizo o trampa de tu cementerio hasta cambiar la cantidad de cartas que obtienes en cada turno. Con una colocación estratégica de cartas, ¡podrías invocar hasta 6 cartas al comienzo de tu turno en lugar de solo 1!\r\n\r\nPara aprovechar al máximo estas habilidades, querrás mantener tus Link Monsters enlazados. Si lo haces, obtendrás un reembolso por el coste de las habilidades de disparo de los nuevos monstruos. Intenta combinar estos monstruos con cartas como Tri-Gate Wizard de Structure Deck: Cyberse Link o Firewall Dragon de Code of the Duelist para obtener aún más de la vinculación de tus monstruos.\r\n\r\nDurante más de 5 años, los cuatro Señores Elementales se han mantenido solos como guardianes de algunos de los efectos más fuertes permitidos en Duelo. ¡En Flames of Destruction, el quinto Lord Elemental finalmente llega, junto con los monstruos encargados de proteger sus grandes poderes de aquellos que los maltratarían! ¡El Señor Elemental más nuevo solo puede ser invocado mientras tienes exactamente 5 monstruos LIGHT en tu Cementerio y tiene el poder de Raigeki, el poder de destruir a todos los monstruos enemigos a la vez! Este Señor Elemental encaja perfectamente en las cubiertas construidas con los monstruos LIGHT de Structure Deck: Wave of Light.\r\n\r\nAdemás de todo eso, Flames of Destruction está lleno de cartas que son útiles en cualquier mazo y cartas populares. Podrás encontrar…\r\n\r\nPoderosas cartas de trampas que puedes activar desde tu mano.\r\nUn nuevo “Agujero de trampa” que destierra a los monstruos boca abajo.\r\nUn hechizo que revive a uno de tus monstruos.\r\nUn nuevo monstruo de enlace “topológico” que destierra todos los hechizos y trampas en el campo cuando un monstruo es convocado especialmente a una zona a la que apunta.\r\nUn Dragón que puede borrar todas las cartas en una sola columna y luego dejar inutilizable esa columna.\r\nUn solista “Trickstar” que ignora los efectos de cartas activados e inflige un gran daño en cada turno.\r\nUna vuelta de “Altergeist” en la Guía del Mundo Subterráneo…\r\n… ¡y más! ¡Este último set de refuerzo de 100 cartas de la temporada de duelos 2017-2018 está preparado para calentar las cosas todo el verano!\r\n¡Nuevas cartass para muchas estrategias! La edición de refuerzo Raging Tempest contiene 100 cartas: 48 Commons, 20 Rares, 14 Super Rares, 10 Ultra Rares 8 Secret Rare.', 10),
(13, ' Yu-Gi-Oh Spirit Warrior', '65', '2019-05-10', 4, 'SpiritWarrior.jpg', 50, 10, 0, ' ¡Los Seis Samurai regresan! Ya conoces la historia de los Seis Samurai y sus legendarios antepasados. ¡Ahora, toma el comando de los guerreros que los apoyan de las sombras cada paso del camino! Al usar la nueva Zona Monstruo Extra con un nuevo Monstruo de Fusión “Seis Samurai”.\r\n\r\n¡Es tu oportunidad de unirte a los Seis Samurai con dos nuevos monstruos!\r\n\r\n– Tiradores expertos que activan poderosas habilidades cuando una carta de Magia/Trampa se activa cerca de sus monstruos. Colocando cuidadosamente a sus monstruos y combinándolos con las cartas de trampa basadas en Circuit Break, ¡puedes acorralar a tu oponente y obligarlo a activar sus efectos para tu beneficio!\r\n\r\n– ¡Manipula el campo con espíritus que controlan el clima! Reúne 3 de estos espíritus en el campo y podrás Invocar a un monstruo que concede a los monstruos que apunta a dominio total sobre las cartas y efectos de tu oponente! ', 10),
(14, ' Yu-Gi-Oh Started Deck Linkst', '350', '2019-05-10', 4, 'Linkstrike.jpg', 61, 10, 1, ' ¡Captura la siguiente ola de monstruos de Yu-Gi-Oh! TRADING CARD GAME con el nuevo Starter Deck: Link Strike!\r\n\r\nStarter Deck: Link Strike incluye 3 Link Monsters para añadir a su Extra Deck, además de una. Este  Deck también introduce un nuevo tipo de monstruo para unirse a los dragones, guerreros, conjuradores y todo lo demás. ¡Inspirado por la maquinaria del establecimiento de una red, este nuevo tipo de monstruo toma la tecnología digital de hoy y la convierte en tecnología de duelo!\r\n\r\nAdemás, Starter Deck: Link Strike está lleno de poderosas cartas que cualquier Duelista querrá añadir a su colección lo antes posible. Las cartas Effect Veiler, Dark Hole, Mystical Space Typhoon, Book of Moon, Pot of Duality, Mirror Force, y más han visto el juego por la totalidad de su existencia, y tanto los nuevos como los Duelistas regresantes pueden encontrarlos todos en este Deck.\r\nStarter Deck: Link Strike contiene 43 cartas: 38 Comunes, 2 Ultra Raras, 3 Super Raras, 1 Guía para Principiantes y 1 zona de juego actualizada. ', 7),
(16, 'Pokemon TCG: Detective Pikachu', '400', '2019-05-10', 6, 'DetectivePikachuCharizardGX.jpg', 57, 10, 1, '¡Un caso misterioso en Ryme City con Charizard-GX!\r\nEs hora de llevar a cabo una seria investigación Pokémon, ¡porque ha entrado en escena un increíble detective Pokémon! Celebra la película Pokémon Detective Pikachu con una carta holográfica de promoción y una carta de gran tamaño de Charizard-GX, valiosas adiciones para cualquier colección de JCC Pokémon. ¡Y eso no es todo! Puedes hacerte con 4 paquetes de mejora especiales de Detective Pikachu de JCC Pokémon, más 2 paquetes de mejora adicionales de JCC Pokémon para ampliar tu colección y descubrir algunos Pokémon más de Ryme City y más allá.\r\n\r\nEl archivo del caso Charizard-GX de Detective Pikachu de JCC Pokémon incluye:\r\n\r\n1 carta holográfica de promoción de Charizard-GX\r\n1 carta holográfica de gran tamaño de Charizard-GX\r\n4 paquetes de mejora de Detective Pikachu de JCC Pokémon\r\n2 paquetes de mejora adicionales de JCC Pokémon\r\n1 carta con código para el Juego de Cartas Coleccionables Pokémon Online', 7),
(17, 'Detective Pikachu Collector Charizard', '400', '2019-05-10', 6, 'Detective-Pikachu-Collectors-Chest.jpg', 0, 10, 1, ' ¡El cofre del tesoro de un detective Pokémon!\r\n\r\nEsta caja de metal resistente contiene pistas, cartas y recuerdos Pokémon, incluidos algunos que no se encuentran en ningún otro lugar:\r\n\r\n7 paquetes de mejora de Detective Pikachu de JCC Pokémon\r\n2 paquetes de mejora adicionales de JCC Pokémon\r\nUn genial pin de coleccionista inspirado en el detective Pikachu.\r\nUn bloc de notas y una hoja de pegatinas de Pokémon. ', 7),
(18, 'Detective Pikachu Case File ', '400', '2019-05-10', 6, 'Detective-Pikachu-TCG-Case-File.jpg', 46, 10, 1, '¡Únete al caso de Detective Pikachu!\r\nEl juego definitivamente ha comenzado, ¡porque ha entrado en escena un increíble detective Pokémon! Celebra la película Pokémon Detective Pikachu con una carta de promoción y una moneda de Detective Pikachu. ¡Y hay más que revelar! Hazte con 2 paquetes de mejora especiales de Detective Pikachu de JCC Pokémon y otro paquete de mejora de JCC Pokémon para ampliar tu colección y celebrar el arte de la investigación en Ryme City y más allá.\r\nEl archivo del caso Detective Pikachu de JCC Pokémon incluye:\r\n1 carta de promoción de Detective Pikachu\r\n1 moneda inspirada en Detective Pikachu\r\n2 paquetes de mejora de Detective Pikachu de JCC Pokémon\r\n1 paquete de mejora adicional de JCC Pokémon\r\n1 carta con código para el Juego de Cartas Coleccionables Pokémon Online', 7),
(19, 'Detective Pikachu Greninja-GX ', '400', '2019-05-10', 6, 'Detective-Pikachu-TCG-Greninja.jpg', 45, 10, 1, 'Un sombrío Pokémon aparece en Ryme City: ¡Greninja-GX!\r\nHace falta tener buen ojo para tener controlado a Greninja-GX, ¡pero afortunadamente ha entrado en escena un increíble detective Pokémon! Celebra la película Pokémon Detective Pikachu con una carta holográfica de promoción y una carta de gran tamaño de Greninja-GX, valiosas adiciones para cualquier colección de JCC Pokémon. ¡Y eso no es todo! También puedes hacerte con un pin inspirado en Greninja\r\ny con 5 paquetes de mejora especiales de Detective Pikachu de JCC Pokémon, más 2 paquetes de mejora adicionales de JCC Pokémon para ampliar tu colección y descubrir algunos Pokémon más que pueden haber estado merodeando en las sombras.\r\nEl archivo del caso Greninja-GX de Detective Pikachu de JCC Pokémon incluye:\r\n?1 carta holográfica de promoción de Greninja-GX\r\n? 1 carta holográfica de gran tamaño de Greninja-GX\r\n? 1 pin de Greninja\r\n? 5 paquetes de mejora de Detective Pikachu de JCC Pokémon\r\n? 2 paquetes de mejora adicionales de JCC Pokémon\r\n? 1 carta con código para el Juego de Cartas Coleccionables Pokémon Online', 7),
(20, 'Arena Deck Mega Charizard X vs MagaBlastoise', '500', '2019-05-10', 6, 'battlearena-charizardmblastoise.jpg', 57, 10, 1, ' Los Pokémon Mega Evolucionados están listos para calentar el campo de batalla en el nuevo Pokémon TCG: Battle Arena Decks. Estas barajas de 60 cartas están listas para jugar de inmediato, con cartas poderosas y estrategias ganadoras dignas de un Entrenador de alto nivel. Cada mazo está liderado por un excepcional Pokémon-EX Mega Evolucionado. ¡Que comience la batalla!\r\nEn cada mazo de 60 cartas, encontrarás:\r\n1 carta de Mega Charizard-EX o Mega Blastoise-EX\r\n1 carta de Charizard-EX o Blastoise-EX\r\n8 cartas de Energía básica brillantes\r\n¡Cartas de nivel de torneo, incluyendo Brigette, Lysandre, Ultra Ball, y más!\r\nAdemás, cada caja contiene:\r\n1 moneda metálica\r\nTapete de juego para 2 jugadores y hoja de reglas\r\n1 tarjeta de código para jugar esta baraja en línea\r\n1 caja de para llevar tu baraja\r\nContadores de daño\r\n1 Guía rápida para desbloquear las poderosas estrategias de las barajas ', 7),
(21, 'Detective Pikachu Mewtwo-GX Bo', '550', '2019-05-10', 6, 'Detective-Pikachu-Mewtwo-GX.jpg', 59, 10, 1, ' ¡Un caso misterioso en Ryme City!\r\nEs hora de llevar a cabo una seria investigación Pokémon, ¡porque ha entrado en escena un increíble detective Pokémon! Celebra la película Pokémon Detective Pikachu con una carta holográfica de promoción y una carta de gran tamaño de Mewtwo-GX, valiosas adiciones para cualquier colección de JCC Pokémon. ¡Y eso no es todo! Puedes hacerte con 4 paquetes de mejora especiales de Detective Pikachu de JCC Pokémon, más 2 paquetes de mejora adicionales de JCC Pokémon para ampliar tu colección y descubrir algunos Pokémon más de Ryme City y más allá.\r\nLa colección de Mewtwo-GX de Detective Pikachu de JCC Pokémon incluye:\r\n1 carta holográfica de promoción de Mewtwo-GX\r\n1 carta holográfica de gran tamaño de Mewtwo-GX\r\n4 paquetes de mejora de Detective Pikachu de JCC Pokémon\r\n2 paquetes de mejora adicionales de JCC Pokémon\r\n1 carta con código para el Juego de Cartas Coleccionables Pokémon Online ', 7),
(22, 'Difference', '350', '2019-06-05', 2, 'Difference.jpg', 39, 10, 1, '          Encuentre el error.\r\nCada jugador tiene una pila de cartas enfrente de él con la misma ilustración… o casi! \r\nTodo el mundo debe encontrar antes que sus oponentes las dos diferencias entre la primera \r\ncarta de su pila y la que está en el centro de la mesa. El primero en hacerlo descarta su \r\ncarta. El primer jugador en descartar todas sus cartas gana la partida.\r\nDIFFERENCE: es un juego para toda la familia que pondrá sus habilidades de observación a prueba.\r\n    ', 45),
(23, 'Sagrada', '10', '2019-06-05', 2, 'Sagrada.png', 29, 10, 1, 'Como experto artesano, tienes en mente realizar una obra de arte con tu próxima vidriera, \r\nque será colocada en la Sagrada Familia.\r\n Los jugadores alternarán turnos escogiendo piezas de cristal, representadas mediante \r\ndados. Tendrán que escoger cuidadosamente, ya que las vidrieras tienen colores y \r\nopacidades únicas, de forma que un dado nunca podrá colocarse adyacente a otro que \r\ncomparta alguna de estas características. \r\nDispondrás de herramientas que te ayudarán a lograr completar esta importante tarea. \r\nAumenta tu prestigio adaptando la obra a los deseos de los admiradores, pero siempre \r\nmanteniendo un estilo propio mientras completas tu próxima obra maestra del vidrio.\r\nJugadores:1-4  Tiempo:30-45 minutos Edad Recomendada: 14 años', 30),
(24, 'Quixo', '200', '2019-06-05', 2, 'Quixo.jpg', 9, 20, 1, 'Quixo es un juego simple, pero no simplista, cuyas reglas se pueden aprender en sólo \r\ntreinta segundos. Cualquiera puede jugar, pon hasta 5 cubos en una fila empujando sus \r\nde cubos de forma horizontal o vertical. Quixo ha sido reconocido como uno de los mejores \r\njuegos en Francia y en América del Norte .\r\nContenido: 1 tablero de juego, 25 cubos de madera, 1 bolsa de tela, las reglas del juego.', 30),
(25, 'Quarto', '187', '2019-06-05', 2, 'Quarto.jpg', 48, 20, 1, 'Cada una de las 16 piezas tiene 4 diferentes atributos, el objetivo es unir en una línea\r\n de hasta 4 piezas con el mismo atributo. El problema es que no podrás escoger cuál unir, \r\nya que tu oponente escoge por ti.\r\nContiene: 1 tablero de juego, 16 piezas de madera, reglamento.', 30),
(26, 'Panic Lab', '250', '2019-06-05', 2, 'PanicLab.jpg', 68, 20, 1, ' Las amebas han escapado y están deslizándose en todas las direcciones! ¡Cógelas rápido! \r\nTodos juegan juntos y al mismo tiempo. Tira el dado que te indica de qué laboratorio \r\nescapó la amebaa atrapar y El primer jugador para encontrar la ameba correcta gana un \r\npunto. Pero ten cuidado, ¡estos pequeños embaucadores no quieren ser atrapados!\r\nContenido: 25 cuadrados ilustrados, 4 dados muy especiales, 30 fichas de juego, \r\nreglas del juego. ', 30),
(27, 'Silk', '500', '2019-06-06', 1, 'batlleforzendikar.jpg', 100, 30, 1, '                   Silk es un juego de puerta de entrada a los sistemas de control de área y colocación de trabajadores. Los jugadores tienen que mover sus orugas de seda a los espacios óptimos para que puedan alimentarse del mejor pasto disponible, mientras empujan las piezas de los otros jugadores a terrenos de alimentación menos deseables. En este juego, los jugadores deben tirar los dados para determinar qué acciones pueden realizar durante su turno. Hay seis tipos de acciones disponibles: criar más gusanos de seda, mover al pastor o su mastín, construir cercas o granjas, mover al monstruo Okami alrededor del tablero y, lo que es más importante, conseguir que los orugas de seda se alimenten. Cuando las orugas de seda se alimentan, generan una serie de puntos de victoria, según el tipo de terreno en el que se encuentren en ese momento. Estos puntos pueden usarse para modificar el resultado de una tirada de dados, volviendo al juego más estratégico.  Por el tipo de juego y mecánicas, Silk se podría considerar como un “destruye amistades”, principalmente por tener la capacidad directa de perjudicar a los demás jugadores, esto lo vuelve un juego sumamente entretenido y competitivo.\r\n\r\nCantidad de Jugadores: 2-4 Tiempo de Juego: 45 minutos Edad Recomendada: 10\r\n                   ', 30),
(28, 'Roll for The Galaxy', '300', '2019-06-06', 1, 'rollforthegalaxy.jpg', 48, 20, 1, 'Roll for the Galaxy es un juego de dados de construcción de imperios espaciales para 2-5 jugadores. Sus dados representan a su población, a quienes usted dirige para desarrollar nuevas tecnologías, establecer mundos y enviar bienes. ¡El jugador que mejor maneja a sus trabajadores y construye el imperio más próspero gana!\r\nEsta versión en dados de Race for the Galaxy lleva a los jugadores en un nuevo viaje a través del Galaxy, pero con la sensación del juego original.\r\n\r\nCantidad de Jugadores: 2-5  Tiempo de Juego: 45 minutos  Edad Recomendada: 13 años\r\n', 30),
(29, 'Catan:JDT. La Hermandad de la Guardia', '200', '2019-06-06', 1, 'CatanJDTs_LaHermandad.jpg', 49, 10, 1, '   Catan: Juego de Tronos – Hermanos de la Guardia está basado en el clásico Catan. En este juego, cada área del Agasajo provee uno de cinco recursos: madera, arcilla, lana, grano o mineral, (Al igual que el desierto de Catan) los estériles Campos de Hielo, no producen nada. Los jugadores toman el rol de los Hermanos de la Guardia Nocturna y utilizan estos recursos para fortalecer su dominio en el norte mediante construir caminos, asentamientos, reclutando guardias para su patrulla; o comprando cartas de desarrollo. Cada una de estas acciones aumenta el poder y reconocimiento de los jugadores mediante puntos de victoria. El objetivo de los jugadores será el mismo que el del Catan original; el primer jugador en obtener 10 Puntos de victoria gana el juego y se convierte en el Lord Comandante de la Guardia Nocturna.\r\nContenido del juego:\r\n124 figuras de los jugadores\r\n20 poblados (5 para cada jugador)\r\n16 fortalezas (4 para cada jugador)\r\n28 guardias (7 para cada jugador)\r\n56 carreteras (14 para cada jugador)\r\n4 tabla de costes de construcción (1 para cada jugador)\r\n        40 figuras de salvajes\r\n	24 guerreros\r\n	8 escaladores\r\n	8 gigantes\r\n	57 fichas de salvajes\r\n	131 cartas\r\n	95 de materias primas\r\n	25 de desarrollo\r\n	11 de héroe\r\n	1 tablero\r\n	6 piezas del marco\r\n	21 hexágonos de terreno\r\n	21 fichas numeradas\r\n	5 puestos comerciales\r\n	2 dados de seis caras\r\n	1 dado de doce caras\r\n	4 porciones del Muro de hielo (bueno… de plástico)\r\n	2 bandejas para cartas\r\n	1 ficha numerada especial «2-12»\r\n	1 reglamento\r\n	1 manual de consulta\r\n\r\nJugadores: 3-4     Tiempo de Juego: 60-75 minutos     Edad Recomendada: 14\r\n   ', 30),
(30, 'Catan Ciudades y Caballeros', '200', '2019-06-06', 1, 'Catan_Ciudad_y_Caballeros.png', 59, 20, 1, 'Ciudades y Caballeros es la expansión definitiva de Los Colonos de Catan, haciéndolo levemente mas estratégico pero manteniendo la diversión, es decir mejorando lo casi inmejorable. En Ciudades y Caballeros la isla de Catan ya se ha desarrollado y requieres de más que sólo recursos básicos, aparte los bárbaros buscan continuamente incursionar en la isla de Catan, por lo que ahora es el momento de resguardarnos por medio de caballeros y proteger nuestras ciudades y poblados.\r\nCiudades y Caballeros utiliza como base el tablero de Los Colonos de Catan, modificando la compra de desarrollos y la llegada del ladrón. La variante consiste ahora en un tercer dado que acercara a los bárbaros cada turno o permitirá recibir cartas de desarrollo de diversos tipos de influencia dependiendo de cuales hubiésemos podido liberar por medio de nuestros nuevos artículos de consumo. En esta variante, la interacción se acentúa permitiéndonos atacar a nuestros oponentes por medio de las cartas de desarrollo, quitándoles caballeros para cuando lleguen los bárbaros se encuentren indefensos o eliminándoles algún camino que no termine en poblado o ciudad, y esas son algunas de las muchas alternativas que nos brindara esta excelente expansión.\r\nSi te gusta los Colonos de Catan, Ciudades y Caballeros no puede faltar en tu colección.\r\n', 30),
(31, 'Tzolkin', '200', '2019-06-07', 1, 'tzolkin.jpg', 59, 20, 1, '  Los Mayas hicieron gala de una civilización muy desarrollada, conocida por sus  incomparables manifestaciones artísticas, compleja arquitectura, sofisticadas matemáticas  y un profundo conocimiento del universo observable. \r\nEn el centro de su sociedad se encontraba el misterioso Tzolk’in: Un calendario de 260 \r\ndías que se apoyaba en el movimiento de los planetas para predecir el momento adecuado en  el que realizar la siembra, organizar ceremonias y construir monumentos.\r\n¿Conseguirás convertirte en un Ajaw, gobernante de la ciudad-estado, encargado de adorar a los dioses y guiar tu civilización hacia la prosperidad?\r\nEste juego gira en torno al calendario Tzolk’in, un conjunto de engranajes que rotan  paulatinamente a medida que las rondas suceden. \r\nEsto significa que, a la hora de trazar tus planes, deberás tener en cuenta un tablero \r\ncontinuo desarrollo. ¡Cosecha, obtén recursos, construye monumentos y procura evitar\r\n la ira de los dioses!\r\n\r\nComponentes del juego\r\n\r\n-1 tablero de juego compuesto por 6 piezas\r\n-6 engranajes de varios tamaños\r\n-1 Hoja con pegatinas\r\n-6 Pasadores de plástico\r\n-24 Trabajadores en 4 colores (6 por jugador)\r\n-28 Marcadores en 4 colores (7 por jugador)\r\n-4 Contadores de puntuación (1 por jugador)\r\n-65 cubos de madera para representar los recursos (madera, piedra y oro)\r\n-13 Calaveras de cristal en plástico\r\n-65 Marcadores de maíz con valores 1 o 5\r\n-21 Losetas de riqueza inicial\r\n-13 Monumentos\r\n-32 Edificios divididos en dos eras\r\n-4 Tableros individuales con doble cara en 4 colores\r\n-1 Marcador de jugador inicial\r\n-1 Reglamento.\r\n\r\nJugadores: 2-4 //Tiempo de Juego: 90 minutos //Edad Recomendada: 14 años  ', 45),
(32, 'Alien vs Predator The Hunt Begins', '500', '2019-06-09', 3, 'AVP.jpg', 0, 10, 1, 'Alien v Predator: The Hunt Begins es un juego táctico de miniaturas para uno o más jugadores que le permite tomar el control de una de las tres fuerzas: Alien Xenomorphs, Predators o Colonial Marines y se enfrentan a unos contra otros en la nave espacial abandonada USCSS Theseus. Alien vs Predator: The Hunt Begins ofrece una campaña que consiste en diez misiones unidas entre sí en una historia narrativa. Una vez que la campaña se ha completado, los jugadores pueden usar las reglas para modificar las misiones existentes e incluso crear sus propias misiones.\r\nContenido:\r\n10 Infant Aliens\r\n5 Stalker Aliens\r\n3 Predators\r\n5 Colonial Marines\r\n3 20-Sided Dice\r\n2 Alien Stat cards\r\n3 Predator Stat cards\r\n5 Colonial Marine Stat card\r\n20 Environmental cards\r\n20 Mission cards\r\n60 Strategy cards\r\n67 Map Tiles\r\n20 Standing Doors\r\n1 Flame/Acid Spit Template\r\n151 Tokens', 30),
(33, 'La Batalla de Los 5 Ejercitos', '550', '2019-06-09', 3, 'Batalla5Ejercitos.jpg', 49, 10, 1, 'La Batalla de los Cinco Ejércitos es un juego de mesa para dos jugadores que te permite recrear la épica conclusión de El Hobbit, la renombrada obra maestra de JRR Tolkien. El jugador de la Sombra comandará los ejércitos de Bolgo, hijo de Azog, el Rey de los Trasgos del Monte Gundabad, mientras que el jugador de los Pueblos Libres controlará a Hombres, Enanos y Elfos liderados por Gandalf para defenderse del ataque de los Trasgos. Basado en las aclamadas mecánicas utilizadas por primera vez en el juego de mesa de La Guerra del Anillo, La Batalla de los Cinco Ejércitos mezcla acción de dados y cartas de eventos para hacer de cada juego una experiencia única.\r\nEl sistema se ha mejorado con el añadido de la mecánica ‘Camino de Destino’, el manejo de la llegada de aliados y el desencadenamiento de eventos especiales. Todos los personajes clave en la batalla, Bilbo, Gandalf, Thorin, Dáin, el Rey Elfo, Bardo, Beorn, Bolgo, se presentan con habilidades especiales y poderosas y figuras individuales.', 30),
(34, 'Risk: Godstorm', '600', '2019-06-09', 3, 'Risk.jpg', 59, 10, 1, '¿Quién gobernará la antigua tierra – y más allá?\r\nEntra en un tiempo de mitos y leyendas. Entra al mundo de Risk Godstorm, donde el miedo y la antigua fe gobiernan. Batalla a través de la Tierra y en sus oscuras regiones subterraneas. Reune fuerzas primales para golpear con un devastante poder. Comanda cinco culturas antigusa – Griegos, Celtas, Babilonios, Noruegos y Egipcios – en una batalla a través de todo un continente que determinará que civilización reina suprema. Y ten por seguro, los dioses no se quedarán sentados durante esta guerra.\r\nDisponible solo en Inglés\r\nComponentes de Juego\r\n\r\n1 tablero de antigua tierra\r\n1 tablero bajo tierra\r\n5 diferentes armadas\r\n5 sets de dioses\r\n5 cartas de panteón\r\n66 cartas de milagro\r\n44 cartas de territorio\r\n12 templos\r\n66 fichas de fé\r\n1 carta de referencia\r\n1 marcador de época\r\n5 marcadores de orden de turno\r\n5 marcadores de plaga\r\n1 marcador de Atlantis hundida\r\n1 marcador de maelstrom\r\n10 dados\r\n1 libro de reglas', 30),
(35, 'Battle Cry', '600', '2019-06-09', 3, 'BattleCry.png', 49, 10, 1, '¡Preparen los cañones! ¡Fijen las bayonetas! ¡Preparen la carga!\r\n\r\nLa Guerra Civil se cierne sobre nosotros, y tu debes tomar el campo de batalla como el lider de las fuerzas de la Unión o los Confederados. Comanda a tus generales y dirige tu infantería, caballería y artillería en 30 escenarios distintos que muestran el terreno y despliegue de tropas de cada batalla histórica, desde First Bull Run y Wilson’s Creek a Prairie Grove y Gettysburg.\r\nPuede que la historia ya haya sido escrita, pero en Battle Cry, el resultado de cada batalla depende de ti. Con tu estrategia y tácticas, puedes cambiar la marea para que Old Glory o Dixie triunfen.\r\nDisponible solo en Inglés\r\nComponentes de Juego\r\n\r\n1 Libro de reglas.\r\n1 Tablero de juego.\r\n60 Cartas de comando.\r\n46 Tiles de terreno de doble lado\r\n9 Tokens de trincheras/trabajo de terreno.\r\n14 Tokens de bandera.\r\n9 Dados de batalla\r\n1 Hoja de etiqueta de bandera.\r\n122 figuras de plástico.', 30),
(36, 'Heroes De Normandia', '600', '2019-06-09', 3, 'heroesdenormandia.jpg', 59, 10, 1, 'Verano de 1944 . El Sol brilla en Normandía. Un viento suave, campos de flores brillantes, y en el fondo, el sonido romántico del fuego de una ametralladora en la mañana. En estos paisajes típicos de la campiña francesa, miles de hombres están a punto de pelear. Y morir. Valientemente como héroes o  como cobardes . Pero sólo los héroes importan realmente . Aquellos que se ven en las películas de guerra  de la época dorada de Hollywood. Aquí se encuentra la inspiración para Heroes of Normandíe; esto es lo que el juego ofrece: batallas explosivas y un ritmo rápido; el placer de masacrar a tus enemigos ; y la capacidad de aplastar bastardos nazis.\r\n\r\nUn juego de miniaturas sin miniaturas, Heroes of Normandie es un juego de estrategia de ritmo rápido inspirado en las películas de guerra de Hollywood. Un juego de mesa que enfrenta a dos jugadores y dos ejércitos, con los alemanes por un lado y los estadounidenses por el otro. Los jugadores usan fichas de mando para determinar la iniciativa. Mientras que un solo dado de seis caras determina el combate, cartas de acción se juegan para añadir salsa al juego. Planifica en secreto tus ataques y burla a tu oponente. Bloquea la estrategia de tu oponente y sorprende a los enemigos. Despliega unidades y no des marcha atrás!\r\n¡Producto ya disponible!', 30),
(37, 'Senderos de Gloria', '350', '2019-06-09', 3, 'SenderosDeGloria.jpg', 59, 10, 1, 'Senderos de Gloria es un juego de estrategia que recrea la primera guerra mundial (1914-1918). Se trata de un juego de guerra, editado originalmente por la prestigiosa empresa estadounidense GMT y está considerado como uno de los mejores juegos de guerra publicados en los últimos años.\r\nComponentes:\r\n1 tablero de juego de 56cm x 86,5 cm\r\n130 cartas\r\n2 láminas de fichas troqueladas\r\n2 dados, 2 hojas de ayuda\r\n1 reglamento de 40 páginas.', 30),
(38, 'Magic: The Gathering. Duel Deck: Elves vs Inventors', '500', '2019-06-09', 5, 'DD-elves-investors.jpg', 59, 10, 1, '¿Te unirás a los inventores y cruzarás los límites de la ciencia para crear dispositivos mágicos de última tecnología? ¿O te unirás a los elfos y canalizarás el poder salvaje del mundo natural para aplastar las creaciones artificiales?\r\nReúne un enorme clan de elfos dispuestos a destruir todas las amenazas mecánicas, o forma un equipo de inventores dedicados a desatar todo el potencial de sus destructivos dispositivos.', 30),
(39, 'Magic: The Gathering. Commander Anthology Volumen II', '800', '2019-06-09', 5, 'cmd-anthology.jpg', 99, 10, 1, 'Desata una batalla legendaria de Magic para 4 jugadores nada más abrir la caja con esta colección de mazos clásicos de Commander. Crea monstruosidades enormes muertas vivientes con “Ansia de poder”, envía gigantes a que aplasten a tus enemigos con “Zambúllete en la batalla”, construye combinaciones letales de artefactos con “Creado desde cero” o engendra un ejército que aumenta inexorablemente con “Engendros letales”. Elige una de estas poderosas estrategias y desafía a tus amigos a una batalla de varios jugadores totalmente épica.\r\n\r\nNombre de la colección: Commander Anthology 2018\r\nCantidad de cartas: 400\r\nFecha de lanzamiento: 8 de junio de 2018', 30),
(40, 'Magic: The Gathering. Dominaria.', '600', '2019-06-09', 5, 'dominaria.jpg', 78, 10, 1, 'El plano de Dominaria es el hogar de algunos de los Planeswalkers más icónicos de toda la historia de Magic. Ahora, estos venerados héroes están de regreso con un nuevo capítulo de sus historias legendarias y con cartas de planeswalker nuevas e imponentes en las que muestran todo su poder.\r\n\r\nDominaria presenta una categoría de cartas totalmente nueva: ¡las cartas históricas! Los artefactos, los encantamientos Saga y las cartas legendarias son cartas históricas, así que puedes obtener ventaja si juegas un mazo que haga honor a la rica historia del plano.\r\n\r\nNombre de la colección: Dominaria\r\nCantidad de cartas: 269\r\nMagic Open House: 14 y 15 de abril de 2018\r\nFin de semana de la Presentación: 21 y 22 de abril de 2018\r\nFecha de Lanzamiento: 27 de abril de 2018', 30),
(41, 'Magic: the Gathering. Signature Spellbook: Jace', '400', '2019-06-09', 5, 'signaturespeelbook-jace.jpg', 58, 10, 1, 'Recurre al poder de Jace Beleren y controla el juego con esta colección de cartas azules esenciales, ahora con nuevos e impresionantes diseños.\r\nContenido:\r\nCada Signature Spellbook: Jace contiene nueve cartas: ocho cartas más una carta premium foil de una de estas ocho cartas incluida al azar.\r\n\r\nCounterspell\r\nBlue Elemental Blast\r\nJace Beleren\r\nMystical Tutor\r\nThreads of Disloyalty\r\nBrainstorm\r\nGifts Ungiven\r\nNegate', 45),
(42, 'Magic: The Gathering. Ixalam', '300', '2019-06-09', 5, 'mtg_ixalan.jpg', 79, 10, 1, 'Durante siglos, las junglas indómitas de Ixalan escondieron un codiciado secreto: Orazca, la ciudad de oro. Pero ningún secreto pude permanecer oculto, ni ningún tesoro puede tomarse sin oposición. Despliega las velas, ensilla un dinosaurio y lucha contra tus rivales mientras te embarcas en un viaje para hacerte con la mayor fortuna del plano.', 45),
(43, 'Cartas de Truco', '80', '2019-06-09', 9, 'truco.jpg', 79, 10, 1, 'El clásico juego de todos los tiempos hace su aparición una vez más. Elige a tu pareja, define tus señas, ensayalas y practica tus mentiras para distraer a tus oponentes. El juego de truco es un clásico argentino que se basa principalmente en engañar a tu oponente. Intenta intimidar a tu oponente para que piense que posees una carta que en realidad...¡No tienes!', 15),
(44, 'Cartas Inglesas', '80', '2019-06-09', 9, 'inglesas.jpg', 29, 10, 1, 'Las 52 cartas inglesas usadas para jugar a la loba, poker, escoba y demás. Pasa un buen rato con tus amigos y familiares.', 45),
(45, 'Legado Perdido: Primera Cronica. La Nave y el Jardin Flotant', '250', '2019-06-09', 9, 'legadoperdido1.jpg', 99, 10, 1, '2 JUEGOS EN 1\r\n\r\nCada serie de Legado Perdido pude jugarse de forma independiente o combinada con otras series para crear exclusivos juegos personalizados.\r\n\r\nEn tiempos inmemoriales, una nave procedente de un lejano planeta apareció en el cielo. Dañada en combate, la nave se desintegró, trazando líneas de fuego por todo el horizonte. Aquellos restos, como estrellas fugaces, se estrellaron contra la superficie, y en los siglos venideros acabarían inscritos en las leyendas como el Legado Perdido. ¡Descubre el paradero del Legado Perdido y gana la partida!', 45),
(46, 'Attack on Titan. Juego de Construccion de Mazos', '300', '2019-06-09', 9, 'attackontitandbg.jpg', 79, 10, 1, '¡La última ciudad humana está siendo atacado por temibles titanes y junto a tu equipo debes detenerlos!\r\n\r\n \r\n\r\nBasado en el mundialmente famoso manga y anime Attack on Titan, llega el juego Attack on Titan: Deck Building Game, en el cual junto a tus camaradas se enfrentarán a implacables Titanes para asegurar la supervivencia de la humanidad. Recoger las armas y tácticas que necesitan, a continuación, pon a prueba tu temple en la pared antes de que sea demasiado tarde.\r\nJuega como Eren, Mikasa, Armin y muchos otros personajes a medida que luchas para mantener a raya a los Titanes. Protege los muros y no pierdas la esperanza sin importar las adversidades ni quién de tu equipo cae durante el ataque. Pronto puedes dar el golpe mortal que cambiar el rumbo de la batalla.\r\n\r\nContenido del juego :\r\n\r\n188 cartas de juego\r\n8 cartas de Héroe\r\n5 secciones de pared\r\n10 Fichas de pared\r\n8 bases de personaje\r\n5 bases de juego\r\n1 marcador de turno\r\n1 Libro de Reglas', 15),
(47, 'Epic: Insurreccion', '40', '2019-06-09', 9, 'epicinsurreccionsobre.png', 99, 10, 1, ' Epic Insurrección nos entrega una expansión para el juego de cartas Epic, que forma parte de un conjunto de 4 sobres independientes, y nos trae 13 fantásticas nuevas cartas y llega para hacer tus partidas aún más emocionantes. Los fuegos de la rebelión han sido encendidos; la caída de los Tiranos está sobre nosotros! Epic Insurrección es la segunda expansión para el juego de cartas Epic. Compuesta por 4 nuevos sobres, que presenta algunos giros interesantes en algunas mecánicas de juego, con ilustraciones espectaculares e introduce la primera carta de condición de victoria alternativa de Epic. Con estos 4 sobres juegas con los enemigos más poderosos del mundo Epic: El Ansia de Zannos, Las Llamas de Scarros, La Ira de Velden y El Edicto de Kark.\r\n\r\nJugadores: 2 a 4   Tiempo de Juego: 20 minutos  Edad Recomendada: 12 años ', 7),
(48, 'Pathfinder. Dragon Rojo Gargantua', '150', '2019-06-09', 10, 'gargantuanreddragon.jpg', 49, 10, 1, 'Dungeons Deep es lo más reciente de Pathfinder Battleseries en miniaturas plásticas pre-pintadas de Wzkids y Paizo Inc. Esta fantástica miniatura es varios centímetros más grande que las demás figuras de Pathfinder Battles: Dungeon Deep Set. El Gargantuan Red Dragón es una pieza única con extraodinarios detalles que no puedes dejar pasar.\r\nLos clientes que preordenen un Case de Dungeon Deep Standard Booster tendrán la oportunidad de comprar una figura promocional de Gargantuan Red Dragon.', 7),
(49, 'Pathfinder Battles. Calabozo Profundo', '200', '2019-06-09', 10, 'pathfinderdungeondeep.jpg', 79, 10, 1, 'Dungeons Deep es el más reciente set de 51 miniaturas de la aclamada y premiada línea de productos de Pathfinder Battles. Dungeon Deep presenta a los amigos y enemigos del mundo de Golarion y de seguro congregará tanto a los jugadores nuevos como experimentados en el tablero de juego.\r\n\r\nLas miniaturas de Pathfinder Battles: Dungeon Deep vienen en 2 tipos de producto, el Booster estándar y el Booster Brick.\r\n¡Boosters de 4 figuras! Los boosters estándar de Pathfinder Battles: Dungeon Deep contienen 1 figura grande y otras 3 pequeñas.', 7),
(50, 'Pathfinder Battles. Heroes Icónicos I', '80', '2019-06-09', 10, 'pathfinderheroes1.jpg', 0, 10, 1, 'Iconic Heroes es el más reciente lanzamiento de Pathfinder Battle Series de Wizkids y Paizo Publishing.\r\n\r\nIconic Series I incluye 6 nuevas miniaturas que presentan personajes legendarios del universo de Pathfinder: Valeros, Seelah, Seoni, Sajan, Lini y amigo animal Droogami. Cada una de estas miniaturas trae un diseño espectacular con nuevas y desafiantes posturas, detalles increíbles y un pintado muy profesional. Además, cada set contiene Boon Cards exclusivas (una para cada miniatura) para que puedas usar en tu juego de Pathfinder Adventure Card Game.\r\n\r\nLos personajes que este set incluye:\r\n\r\n• Valeros, Iconic Fighter\r\n• Seelah, Iconic Paladin\r\n• Seoni, Iconic Sorcerer\r\n• Sajan, Iconic Monk\r\n• Lini, Iconic Druid\r\n• Droogami, Snow Leopard Animal Companion', 7),
(51, 'Calabozos y Dragones 5th Ed. Out of the Abyss Adventure', '200', '2019-06-09', 7, 'd&doutoftheabyss.jpg', 80, 10, 1, ' Atrévete a bajar a las profundidades de Underdark en esta aventura del Juego de Rol mas grande del mundo!\r\n\r\nDungeons & Dragons: Underdark es una tierra subterránea, un vasto laberinto donde reina el miedo, es hogar de horribles monstruos que nunca han visto la luz del día, es ahí donde el elfo oscuro Gromph Baenre, Archimago de Menzoberranzan lanza un oscuro hechizo para consumir la energía impregnada en Underdark y lugar donde lágrimas abren portales al abismo demoníaco. Las sorpresas que traen sus acciones, su camino de ahora en adelante, la locura que domina Underdark y amenaza con sacudir Forgotten Realms hasta sus inicios. ¡Deten esta locura antes que te consuma!\r\n\r\nUna aventura Dungeons & Dragons para jugadores de nivel 1 al 15.\r\n\r\nPara jugar debes tener también los libros del Jugador, Dungeons Master y Monstruos.\r\nProducto en Inglés. ', 30),
(52, 'Calabozos y Dragones: The Rise of Tiamat', '250', '2019-06-09', 7, 'trot.jpg', 20, 10, 1, ' Evita el apocalíptico regreso de Tiamat en esta aventura del juego de rol más importante del mundo.\r\n\r\nEl Cult of Dragon está liderando la carga en una impía crusada para traer a Tiamat de vuelta a los reinos. La situación empeora cada vez más y las desgracias siguen asechando a las buenas personas. La batalla toma un giro político y las alianzas comienzan a tomar gran ventaja entre los bandos. De Waterdeep hasta Thay, la guerra en contra del mal ha comenzado, triunfa o sucumbe ante la opresión dracónica. Ganes o pierdas, las cosas nunca volverán a ser como antes. ', 7),
(53, 'Calabozos y Dragones Set Inicial', '400', '2019-06-09', 7, 'DDStarterSet.jpg', 50, 10, 1, ' ¡Todo lo que los jugadores y el Dungeon Master necesitan para comenzar su aventura ya está aquí!\r\n\r\nEl Starter Set presenta la nueva edicion de Dungeons & Dragons de una manera que será muy fácil de entender para los jugadores nuevos y experimentados y que hace que el inicio de tu aventura sea mucho más rápido y ligero de sobrellevar en los clásicos parajes de los Reinos Olvidados. El Starter Set de D&D es la introducción perfecta para una aventura de 4 a 6 jugadores, y personajes de nivel 1 a 5. ', 7),
(54, 'Pokemon TCG: Mega Gengar Products', '40', '2019-06-09', 11, 'pok_megagengar.jpg', 79, 10, 1, ' Mantén tus cartas seguras -y luciendo muy bien- con los nuevos accesorios Pokémon TCG con protectores y deck boxes. Cada uno de estos accesorios de  de alta calidad está respaldado con el aspecto exclusivo de Mega Gengar, y cada uno incluye la calidad oficial Pokémon Trading Card Game.\r\n\r\nEstos accesorios OFICIALES incluyen:\r\nProtectores y deck boxes con arte de Mega Gengar\r\n¡Sello de calidad oficial de Pokémon TCG! ', 7),
(55, 'Pokemon TCG: Mega Gyarados Shiny ', '80', '2019-06-09', 11, 'pok_megagyaradosshiny.jpg', 79, 10, 1, ' Mantén tus cartas seguras -y luciendo muy bien- con los nuevos accesorios Pokémon TCG con protectores, deck boxes y tapetes de juego. Cada uno de estos accesorios de  de alta calidad está respaldado con el aspecto exclusivo de Mega Gyarados Shiny, y cada uno incluye la calidad oficial Pokémon Trading Card Game.\r\n\r\nEstos accesorios OFICIALES incluyen:\r\n\r\nProtectores, deck boxes y playmat con arte de Mega Gyarados Shiny\r\n¡Sello de calidad oficial de Pokémon TCG! ', 7),
(56, 'UltraPRO: Yellow & Orange ', '100', '2019-06-09', 11, 'carpeta.jpg', 80, 10, 1, '¡Te presentamos nuestra nueva carpeta PRO Binder en 2 nuevos colores, Orange & Yellow!\r\n\r\nEsta nueva carpeta Ultra PRO PRO Binder tiene un elegante diseño con reverso de color negro en cada una de las páginas. Está hecha para que el jugador introduzca las cartas de forma lateral lo que añade mucha más seguridad al transporte de tus cartas. Contiene además, una banda elástica de color negro que asegura el cierre de la carpeta. Está hecha de un material muy resistente y libre de elementos nocivos o tóxicos.', 7),
(57, 'Magic: The Gathering. Abaco Contador de Vidas', '200', '2019-06-09', 11, 'mtg_abacus.jpg', 79, 10, 1, 'Ahora podrás llevar la cuenta de tus puntos de 1 a 110 con el estilo del ábaco contador de vida trae para ti, ofreciendo un diseño minimalista con los 5 símbolos de maná de Magic: The Gathering. Hecho con aleación de zinc fundido troquel y acabado con un esmalte negro de alto brillo, este contador es una elegante adición a elementos imprescindibles de un jugador de Magic .', 7),
(58, 'La Llamada de Cthulhu. Edicion Primigenia', '1200', '2019-06-09', 8, 'cthulhu6.jpg', 98, 10, 1, '   Los primigenios gobernaron la Tierra eones antes de la aparición del hombre, pero aún hoy es posible encontrar restos de sus ciclópeas ciudades en remotas islas del Pacífico, bajo la arena de los vastos desiertos y en las gélidas desolaciones de los casquetes polares. En un principio vinieron a este mundo desde las estrellas, pero ahora duermen; unos, en las profundidades de la tierra, y otros, bajo el mar. Sin embargo, cuando las estrellas estén en posición, se alzarán y caminarán de nuevo sobre la Tierra.\r\nLa llamada de Cthulhu es un juego de rol basado en la obra de H. P. Lovecraft, en el que personas corrientes se enfrentan a las espantosas fuerzas de los Mitos de Cthulhu.   ', 30),
(59, 'La Llamada de Cthulhu 6. Las mascaras de Nyarlathotep', '1500', '2019-06-09', 8, 'cocmascarasdenyarlathotep.jpg', 80, 10, 1, ' Él cabalga sobre la montaña,\r\nal igual que cabalga sobre el mundo!\r\n¡Nyar shthan, Nyar gashanna!\r\nPor fin las estrellas casi están en la posición correcta. Pronto los planes de Nyarlathotep darán fruto, y el mundo cambiará de forma irrevocable... pero aún no. Un grupo de molestos investigadores humanos ha averiguado demasiadas cosas. Ahora deben sobrevivir lo suficiente como para encontrar sentido a lo que saben y actuar de forma resuelta.\r\nLas máscaras de Nyarlathotep, un clásico del rol, se compone de una serie de aventuras enlazadas, que forman una campaña larga e inolvidable. Hechos terroríficos y peligrosas hechicerías acechan a quienes intentan desentrañar el destino de la Expedición Carlyle. El carácter no lineal de la narrativa mantiene a los jugadores perplejos y en ascuas. “Acción” es la palabra clave, mientras los personajes se baten contra sectarios, magia, dementes y los terribles poderes de los dioses exteriores (o intentan esquivarlos).\r\n\r\nLa llamada de Cthulhu es un juego de rol basado en la obra de H. P. Lovecraft, en el que personas corrientes se enfrentan a las espantosas fuerzas de los Mitos de Cthulhu. Todo lo que necesitas para jugar es este manual, unos dados y a tus amigos. ', 7);
INSERT INTO `productos` (`id_producto`, `nombre_producto`, `precio`, `fecha`, `subcategoria`, `nombreFoto`, `stock`, `stockMinimo`, `estado`, `descripcion`, `garantia`) VALUES
(60, 'La Llamada de Cthulhu 6. Los harapos del rey', '1800', '2019-06-09', 8, 'harapordelrey.jpg', 110, 10, 1, 'Octubre de 1928. Londres: la capital de un imperio que abarca la cuarta parte del planeta y de la raza humana. Sus habitantes andan de aquí para allá, ocupados con asuntos de política y gobierno, finanzas e industria, trabajo y placer.\r\nPero cuán frágiles son esas cosas.\r\nQué grande su ignorancia.\r\nPues hay quienes persiguen objetivos bien distintos, personas que desean traer un poder inhumano a la Tierra, uno tan inmenso que la frenética actividad del resto del mundo parecería simplemente un último baile antes de morir.\r\nTodos sienten la llamada de las estrellas. Artistas, músicos y escritores trabajan tras la puesta de sol, sentados junto a la ventana, con las cortinas abiertas al cielo. Los afligidos recorren las calles de noche, hablando con ellos mismos, enfureciéndose con aquellos que los interrumpen. Los locos se sientan en sus celdas con la mirada perdida allí por donde las Híades no tardarán en aparecer.\r\nLas estrellas están alineadas.\r\nLa mirada de Hastur se posa brevemente sobre la Tierra.\r\nY todo cambia.', 30),
(61, 'Clank', '200', '2019-06-10', 1, 'clank.png', 80, 10, 1, 'En Clank! debes ser rápido y silencioso. Cada sonido descuidado atrae la atención del dragón, y cada artefacto robado aumenta su ira. ¡Puedes disfrutar de tu botín solo si logras salir con vida de las profundidades!\r\n\r\nClank! Es un juego de construcción de mazos. Cada jugador tiene su propio mazo, y construir el tuyo es parte del juego. Comienza cada uno de tus turnos con cinco cartas en tu mano, y las jugarás todas en el orden que elijas. La mayoría de las tarjetas generarán recursos, de los cuales hay tres tipos diferentes:\r\n\r\nHabilidad , que se usa para adquirir nuevas cartas para tu mazo.\r\nLas espadas , que se utilizan para luchar contra los monstruos que infestan la mazmorra.\r\nBotas , que se utilizan para moverse por el tablero.\r\nCada vez que adquieres una nueva carta, la pones boca arriba en tu pila de descarte. Siempre que necesites sacar una carta y tu mazo este vacío, barajas tu pila de descarte y la giras boca abajo para formar un nuevo mazo. ¡Con cada shuffle, tus nuevas cartas se convierten en parte de un mazo más grande y mejor! Cada jugador comienza con las mismas cartas en su mazo, pero adquirirá cartas diferentes durante sus turnos. Debido a que las cartas pueden hacer muchas cosas diferentes, el mazo (y la estrategia) de cada jugador serán cada vez más diferentes a medida que se desarrolle el juego.', 45),
(62, 'Diamant', '300', '2019-06-10', 2, 'ElHobbit.jpg', 80, 20, 1, 'Descripción Modificada', 35),
(63, 'Arriba y Abajo', '250', '2019-06-10', 1, 'arribayabajo.jpg', 50, 10, 1, 'Los bárbaros acaban de saquear la última aldea de tu pueblo. A duras penas has podido huir con tu hijo y tu caña de pescar favorita antes de verlo todo consumido por el fuego, en la noche huyes de la tierra en que naciste, atravesando desiertos ardientes, cordilleras heladas y mares llenos de peligro. Hasta que encuentras tu lugar, la tierra perfecta para convertirse en tu nuevo hogar. Pero, tras construir tu primera cabaña, descubres una enorme red de cavernas subterráneas repletas de tesoros, productos escasos y un sin fin de aventuras.\r\n\r\nPrepárate para organizar expediciones y empezar a construir tu aldea Arriba y Abajo.\r\n\r\nComponentes del juego\r\n1 Tablero de reputación\r\n81 Fichas de productos\r\n12 de frutas\r\n12 de setas\r\n12 de pescado\r\n10 de vasijas\r\n10 de cuerdas\r\n10 de papel\r\n8 de metales\r\n7 de amatistas\r\n7 Dados\r\n10 Fichas de sidra\r\n1 Ficha de ronda\r\n20 Fichas de poción\r\n1 Carta de primer jugador\r\nAúreos\r\n36 Monedas de 1 áureo\r\n6 Monedas de 5 áureos\r\n8 Monedas de 10 áureos\r\n4 Tableros personales\r\n1 Libro de encuentros\r\n25 Cartas de casa\r\n4 Cartas de Hogar (Casa inicial)\r\n24 Cartas de refugio\r\n9 Cartas de taller (con un icono de llave)\r\n6 Cartas de almacén (con un icono de estrella)\r\n25 Cartas de cueva\r\nFichas\r\n12 Aldeanos Iniciales\r\n18 Aldeanos\r\n6 Aldeanos especiales', 30),
(64, 'Gobblet Gobblers', '500', '2019-06-10', 1, 'gobbletgobblers.png', 200, 10, 1, 'Pon a prueba tu memoria con este fácil de aprender y rápido juego de estrategia. personajes caricaturescos de madera traen dinamismo a este juego de estilo de Tic-Tac-Toe.\r\nSólo alinea tres  en una fila para ganar, ¡pero cuidado! Estos Gobblers están hambrientos de diversión.\r\nInundado de colores alegres, Gobblet Gobblers es el mejor primer juego de estrategia para los niños hecho para desafiar a sus padres.\r\n\r\nContenido:\r\n1 cuadrícula de madera\r\n12 Gobblers de plástico\r\n1 manual ilustrado', 45),
(65, 'Plenus', '200', '2019-06-10', 1, 'plenus.png', 50, 10, 1, 'En Plenus, los dados determinan qué color y cuántos espacios puedes marcar en tu hoja de juego, pero el jugador inicial puede primero evaluar lo que están haciendo todos, luego quitar dos dados, dejando que todos los demás realicen sus elecciones con los dados que quedan …', 30),
(66, 'King of Tokyo', '200', '2019-06-10', 1, 'KingOfTokyo.jpg', 50, 10, 1, '¡Toma control de un MegaMonstruo y destruye todo lo que encuentres a tu paso!\r\nLanza los dados para conseguir la mejor combinación con la que curarte, atacar, comprar cartas especiales o ganar Puntos de Victoria. Tú decides qué táctica usar para conseguir ser King of Tokyo: acumular 20 Puntos de Victoria o ser el último Monstruo en pie. Ambas opciones te convertirán en el Monstruo vencedor.', 30),
(67, 'King of New York', '200', '2019-06-10', 1, 'KingOfNewYork.jpg', 50, 10, 1, 'King of New York es un juego independiente de su hermano mayor, diseñado para 2 a 6 jugadores que volverán a tomar el papel gigantescas criaturas que han elegido una ciudad para ver cuál de ellas es la mejor, sin importar la destrucción colateral que esta “discusión” pueda provocar.\r\n\r\nAl igual que en King of Tokyo, el objetivo del juego es conseguir 20 puntos de victoria, en su turno los jugadores lanzan seis dados hasta tres veces y llevan a cabo las acciones según los resultados. Las garras hacen daño a otros jugadores, los corazones sirven para recuperar nuestra salud y la energía nos sirve para conseguir cartas de habilidades especiales, que podremos usar para llevar a cabo efectos únicos. Sin embargo, debido a esta idiosincrasia estadounidense, los jugadores pueden intentar ganar puntos de victoria a través de la fama; convirtiendo a su monstruo en una superestrella.\r\n\r\nEl tablero de King of New York es más grande que el de King of Tokyo, cada monstruo ocupa un distrito de la ciudad mientras intentan alcanzar Manhattan para poder llevar a cabo su “Grand Stand” Los jugadores pueden usar los ataques para moverse por la ciudad, destruir edificios o huir del ejército, ya que a mayor destrucción provocada, más dura será la respuesta de las Fuerzas Armadas de Estados Unidos.\r\n\r\nY si, por si os lo estabais preguntando, se pueden usar las cartas de King of Tokyo (y viceversa) en este juego.\r\n\r\nEl juego dispone de 64 cartas y le corresponden las “fundas juegos de mesa 69x69mm”', 30),
(68, 'Terra Mystica', '300', '2019-06-10', 1, 'TerraMistyca.jpg', 50, 10, 1, 'Terra Mystica es un juego de estrategia con unas bases muy sencillas y una mínima presencia del factor suerte. Cada jugador gobierna sobre una facción y trata de transformar el terreno en su propio beneficio para así construir diversas estructuras. La proximidad de otros jugadores en el tablero limita sus posibilidades de expansión, pero a la vez puede aportar otro tipo de beneficios. El atractivo de Terra Mystica se apoya precisamente en este dilema.\r\n\r\nLas 14 facciones del juego están diseñadas con sumo cuidado. Cada una posee sus propias habilidades especiales, que en combinación con las diversas losetas de bonificación garantizan una amplia variedad de partidas y situaciones para que nunca os canséis de jugar. Terra Mystica ha contado con el inestimable apoyo de Uwe Rosenberg durante el desarrollo de las mecánicas de juego.', 30);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategorias`
--

CREATE TABLE `subcategorias` (
  `Id` int(10) UNSIGNED NOT NULL,
  `nombreSub` varchar(30) COLLATE utf8_bin NOT NULL,
  `IdCategoria` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `subcategorias`
--

INSERT INTO `subcategorias` (`Id`, `nombreSub`, `IdCategoria`) VALUES
(1, 'estrategia', 1),
(2, 'abstracto', 1),
(3, 'wargames', 1),
(4, 'yu-gi-oh!', 2),
(5, 'magic', 2),
(6, 'pokemon', 2),
(7, 'D&D', 5),
(8, 'COC', 5),
(9, 'otros', 2),
(10, 'miniatura', 3),
(11, 'accesorios', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `Id` int(10) UNSIGNED NOT NULL,
  `mail` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `IdPersona` int(10) UNSIGNED NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`Id`, `mail`, `password`, `IdPersona`, `estado`) VALUES
(2, 'eme.19.87@hotmail.com', 'MTIzNDU2Nzg=', 5, 1),
(5, 'wilbur.33@gmail.com', 'MTIzNDU2Nzg=', 10, 1),
(6, 'rupert87@gmail.com', 'MTIzNDU2Nzg=', 18, 0),
(7, 'elrich87@gmail.com', 'MTIzNDU2Nzg=', 19, 0),
(8, 'willycaballero@gmail.com', 'MTIzNDU2Nzg=', 20, 0),
(9, 'fedeola@gmail.com', 'MTIzNDU2Nzg=', 21, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria_producto`
--
ALTER TABLE `categoria_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `consultas`
--
ALTER TABLE `consultas`
  ADD PRIMARY KEY (`id_consulta`),
  ADD KEY `consulta` (`consulta`),
  ADD KEY `motivo` (`motivo`);

--
-- Indices de la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  ADD PRIMARY KEY (`id_detalle`),
  ADD KEY `idOrden` (`idOrden`),
  ADD KEY `idProducto` (`idProducto`);

--
-- Indices de la tabla `motivo_consulta`
--
ALTER TABLE `motivo_consulta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orden_pedido`
--
ALTER TABLE `orden_pedido`
  ADD PRIMARY KEY (`orden_id`),
  ADD KEY `idCliente` (`idCliente`);

--
-- Indices de la tabla `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IdPerfil` (`IdPerfil`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `subcategoria` (`subcategoria`);

--
-- Indices de la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IdCategoria` (`IdCategoria`),
  ADD KEY `IdCategoria_2` (`IdCategoria`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IdPersona` (`IdPersona`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `consultas`
--
ALTER TABLE `consultas`
  MODIFY `id_consulta` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  MODIFY `id_detalle` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT de la tabla `motivo_consulta`
--
ALTER TABLE `motivo_consulta`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `orden_pedido`
--
ALTER TABLE `orden_pedido`
  MODIFY `orden_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `perfil`
--
ALTER TABLE `perfil`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'El id que identifica a la persona', AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `consultas`
--
ALTER TABLE `consultas`
  ADD CONSTRAINT `consultas_ibfk_1` FOREIGN KEY (`motivo`) REFERENCES `motivo_consulta` (`id`);

--
-- Filtros para la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  ADD CONSTRAINT `detalle_pedido_ibfk_2` FOREIGN KEY (`idProducto`) REFERENCES `productos` (`id_producto`),
  ADD CONSTRAINT `detalle_pedido_ibfk_3` FOREIGN KEY (`idOrden`) REFERENCES `orden_pedido` (`orden_id`);

--
-- Filtros para la tabla `orden_pedido`
--
ALTER TABLE `orden_pedido`
  ADD CONSTRAINT `orden_pedido_ibfk_1` FOREIGN KEY (`idCliente`) REFERENCES `personas` (`Id`);

--
-- Filtros para la tabla `personas`
--
ALTER TABLE `personas`
  ADD CONSTRAINT `personas_ibfk_1` FOREIGN KEY (`IdPerfil`) REFERENCES `perfil` (`id`);

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`subcategoria`) REFERENCES `subcategorias` (`Id`);

--
-- Filtros para la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  ADD CONSTRAINT `subcategorias_ibfk_1` FOREIGN KEY (`IdCategoria`) REFERENCES `categoria_producto` (`id`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`IdPersona`) REFERENCES `personas` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
