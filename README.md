# Tienda Virtual

Proyecto de tienda virtual de comercio electrónico iniciado en la materia Taller De Programación I de tercer año para la carrera Licenciatura En Sistemas De Información de la Universidad Nacional Del Nordeste (Corrientes, Capital).
El proyecto está creado en el framework de php codeigniter, usando MySQL como base de datos y aplicando bootstrap para dar estilos css a las páginas html.
El proyecto se basa en una tienda online que venderá juegos de mesa, juegos de cartas, juegos de rol y demás productos relacionados al mundo del comic, manga, anime y videojuegos.
