<?php

/*Modelo que permite trabajar con la tabla de pedidos*/

class ModeloPedido extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function guardarOrdenPedido($data) {
		$this->db->insert('orden_pedido',$data);
	}

	public function guardarDetallePedido($data) {
		$this->db->insert('detalle_pedido',$data);
	}

	
	/*retorna todas las ordenes de pedido para un cliente*/
	public function getPedidoCliente($idCliente,$limite,$fila) {
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('orden_pedido');
		
		$where= "idCliente='".$idCliente."'";
		$this->db->where($where);
		$this->db->limit($limite,$fila);
		$this->db->join('personas','personas.Id=orden_pedido.idCliente');
		$this->db->join('detalle_pedido','detalle_pedido.idOrden=orden_pedido.orden_id');
		$this->db->join('productos','productos.id_producto=detalle_pedido.idProducto');
		
		$consulta=$this->db->get();
		/*realizo la consulta*/
		return $consulta->result();
	}

	/*retorna todas las compras que se han realizado*/
	public function getTotalCompras() {
		/*indico que seleccionaré todos los registros*/
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('orden_pedido');
		

		/*realizo la consulta*/
		$consulta=$this->db->get();
		/*retorno la consulta para que sea usada por el controlador*/
		return $consulta->num_rows();
	}

	public function getCantidadPedidos($idCliente) {
		/*indico que seleccionaré todos los registros*/
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('orden_pedido');
		
		$this->db->where('idCliente',$idCliente);
		

		/*realizo la consulta*/
		$consulta=$this->db->get();
		/*retorno la consulta para que sea usada por el controlador*/
		return $consulta->num_rows();
	}

	/*retorna todos los detalles de una orden de pedido de un cliente*/
	public function getDetallesPedido($idPedido) {
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('detalle_pedido');
		
		$this->db->where('idOrden',$idPedido);
		$this->db->join('productos','productos.id_producto=detalle_pedido.idProducto');
		$this->db->join('orden_pedido','orden_pedido.orden_id=detalle_pedido.idOrden');
		$this->db->join('personas','personas.Id=orden_pedido.idCliente');
		$consulta=$this->db->get();
		/*realizo la consulta*/
		return $consulta->result();
	}

		/*retorna todas las ordenes de pedido para un cliente*/
	public function getPedidoAdm() {
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('orden_pedido');
		$this->db->join('personas','personas.Id=orden_pedido.idCliente');
		$consulta=$this->db->get();
		/*realizo la consulta*/
		return $consulta->result();
	}


	public function getComprasTotales($limite,$fila) {
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('orden_pedido');
		$this->db->join('personas','personas.Id=orden_pedido.idCliente');
		$this->db->limit($limite,$fila); 
		$consulta=$this->db->get();
		/*realizo la consulta*/
		return $consulta->result();
	}
	
	/*retorna todos los detalles de una orden de pedido de un cliente*/
	public function getDetallesAdm($idPedido) {
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('detalle_pedido');
		
		$this->db->where('idOrden',$idPedido);
		$this->db->join('productos','productos.id_producto=detalle_pedido.idProducto');
		$this->db->join('orden_pedido','orden_pedido.orden_id=detalle_pedido.idOrden');
		$this->db->join('personas','personas.Id=orden_pedido.idCliente');
		$this->db->join('subcategorias','subcategorias.id=productos.subcategoria');
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.idCategoria');
		$consulta=$this->db->get();
		/*realizo la consulta*/
		return $consulta->result();
	}

	/*retorna todos los detalles de todas las compras*/
	public function getDetallesTotales() {
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('detalle_pedido');
		
		$this->db->join('productos','productos.id_producto=detalle_pedido.idProducto');
		$this->db->join('orden_pedido','orden_pedido.orden_id=detalle_pedido.idOrden');
		$this->db->join('personas','personas.Id=orden_pedido.idCliente');
		$this->db->join('subcategorias','subcategorias.id=productos.subcategoria');
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.idCategoria');
		$this->db->order_by('idProducto','ASC');
		$consulta=$this->db->get();
		/*realizo la consulta*/
		return $consulta->result();
	}

	/*permite filtrar las compras mediante fechas, retornando las compras en un rango de fechas*/
	public function filtrarPorFecha($limite,$fila,$fechaInicio,$fechaFin) {
		//echo $fechaInicio; echo $fechaFin; die;
		$where = "orden_fecha='" . $fechaInicio . " ' " .  " AND orden_fecha='" . $fechaFin . " '";
		$this->db->select('*');
		$this->db->from('orden_pedido');
		$this->db->where($where);
		$this->db->join('personas','personas.Id=orden_pedido.idCliente');
		$this->db->limit($limite,$fila);
		$consulta=$this->db->get();
		return $consulta->result();



	}

	/*permite filtrar las compras mediante fechas, retornando las compras en un rango de fechas*/
	public function getTotalEntreFechas($fechaInicio,$fechaFin) {
		//echo $fechaInicio; echo $fechaFin; die;
		$where = "orden_fecha='" . $fechaInicio . " ' " .  " AND orden_fecha='" . $fechaFin . " '";
		$this->db->select('*');
		$this->db->from('orden_pedido');
		$this->db->where($where);
		$consulta=$this->db->get();
		return $consulta->num_rows();
	}

	/*retorna la cantidad de elementos vendido para una categoria específica*/
	public function getTotalCategoria($categoria) {
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('detalle_pedido');
		
		$this->db->join('productos','productos.id_producto=detalle_pedido.idProducto');
		$this->db->join('orden_pedido','orden_pedido.orden_id=detalle_pedido.idOrden');
		$this->db->join('personas','personas.Id=orden_pedido.idCliente');
		$this->db->join('subcategorias','subcategorias.id=productos.subcategoria');
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.idCategoria');
		$this->db->where('idCategoria',$categoria);
	}

	/*busca los detalles de compra según el número de factura*/
	public function getCompraId($id) {
		$this->db->select('*');
		$this->db->from('orden_pedido');
		$this->db->join('personas','personas.Id=orden_pedido.idCliente');
		$this->db->where('orden_id',$id);
		$consulta=$this->db->get();
		return $consulta->result();

	}

	/*busca las facturas cuyos compradores tengan apellidos similares o uno específico*/
	public function getCompraApellidos($apellido) {
		$this->db->select('*');
		$this->db->from('orden_pedido');
		$this->db->join('personas','personas.Id=orden_pedido.idCliente');
		$where="Select From orden_pedido Where apellido Like%'" . $apellido . " '";
		$this->db->where($where);
		$consulta=$this->db->get();
		return $consulta->result();
	}

}