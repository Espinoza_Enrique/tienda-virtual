<?php

/*Modelo que permite trabajar con la tabla de clientes*/

class ModeloProducto extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	/*método que carga los datos de un producto en la base de datos*/
	public function guardar_producto($data) {

		$this->db->insert('productos',$data);

	}

	/*extraigo todas las categorías*/
	public function get_categoria() {
		$this->db->select('*');
		$this->db->from('categoria_producto');
		$this->db->order_by('id','ASC');
		$categorias=$this->db->get();
		return $categorias->result();
	}

	/*extraigo las subcategorias de la base de datos*/
	public function get_subcategorias() {
		$this->db->select('*');
		$this->db->from('subcategorias');
		$this->db->order_by('IdCategoria','ASC');
		$subcategorias=$this->db->get();
		return $subcategorias->result();
	}
	/*obtengo la cantidad de registros que tiene almacenado para las categorias*/
	public function get_cantidad() {
		$cantidad=$this->db->get('categoria_producto');
		return $cantidad->num_rows();

	}

	/*obtengo sólo las subcategorias relacionadas con la categoria pasada como parámetro*/
	public function get_subcategoria($categoria) {
		$this->db->select('*');
		$this->db->from('subcategorias');
		$this->db->where('IdCategoria',$categoria);
		$consulta=$this->db->get();
		return $consulta->result();

	}

	public function getCantidadProductos() {
		$cantidad=$this->db->get('productos');
		return $cantidad->num_rows();

	}

	public function getCantidadPorCat(){

	}

	public function getCantidadPorSub() {

	}
	
	/*obtengo todos los registros de productos*/
	public function get_productos($limite,$fila) {
		/*indico que seleccionaré todos los registros*/
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('productos');
		$this->db->limit($limite,$fila); 
		/*vinculo los productos con sus subcategorias*/
		$this->db->join('subcategorias','subcategorias.id=productos.subcategoria');
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.idCategoria');
		/*realizo la consulta*/
		$consulta=$this->db->get();
		/*retorno la consulta para que sea usada por el controlador*/
		return $consulta->result();
	}

	/*Obtengo los productos para una categoria especìfica.
	Es mediante esta consulta por la cual yo muestro los productos de cada categorìa sólo para los productos activos y con stock*/
	public function get_producto_categoria($idCategoria,$criterioDeOrdenamiento,$limite,$fila) {
		/*indico que seleccionaré todos los registros*/
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('productos');

		$this->db->limit($limite,$fila); 
		/*selecciona la categoria*/
		$this->db->where('subcategoria',$idCategoria);
		/*selecciono sòlo los productos de estado activo*/
		$this->db->where('estado', 1);
		/*selecciono sólo los productos para los cuales hay stock*/
		$this->db->where('stock >', 0);
		/*se establece un criterio de ordenamiento*/
		switch($criterioDeOrdenamiento) {
			case 1:
				$this->db->order_by('nombre_producto','ASC');
			break;
			case 2:
				$this->db->order_by('nombre_producto','DESC');
			break;
			case 3:
				$this->db->order_by('precio','DESC');
			break;
			case 4:
				$this->db->order_by('precio','ASC');
			break;
		};
		/*vinculo los productos con sus subcategorias*/
		$this->db->join('subcategorias','subcategorias.id=productos.subcategoria');
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.idCategoria');
		/*realizo la consulta*/
		$consulta=$this->db->get();
		/*retorno la consulta para que sea usada por el controlador*/
		return $consulta->result();
	}

	/*obtengo un producto mediante su id*/
	public function get_ProductoId($id) {
		/*indico que seleccionaré todos los registros*/
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('productos');
		$this->db->where('id_producto',$id);
		/*realizo la consulta*/
		$consulta=$this->db->get();
		/*retorno la consulta para que sea usada por el controlador*/
		return $consulta->row();
	}

	/*obtengo los productos mediante una palabra clave o similares. Sólo revisa el nombre del producto.*/
	public function getProductosPorPalabra($palabraClave,$limite,$fila) {
		$this->db->select('*');
		$this->db->from('productos');
		$where="nombre_producto like'%" . $palabraClave. "%'";
		$this->db->where($where);
		$this->db->limit($limite,$fila); 
		$consulta=$this->db->get();
		/*retorno la consulta para que sea usada por el controlador*/
		return $consulta->result();
		
		
	}

	/*obtengo los productos mediante una palabra clave o similares. Sólo revisa el nombre del producto.*/
	public function getCantidadPalabraClave($palabraClave) {
		$this->db->select('*');
		$this->db->from('productos');
		$where="nombre_producto like'%" . $palabraClave. "%'";
		$this->db->where($where);
		$consulta=$this->db->get();
		/*retorno la consulta para que sea usada por el controlador*/
		return $consulta->num_rows();
		
		
	}

	/*permite cambiar el estado de un producto*/
	public function cambiarEstado($id,$estado) {
		/*
		$this->db->set('estado',$estado);
		$this->db->where('id_producto', $id);
		$this->db->update('productos'); */
		/*creo la consulta llamando al procedimiento almacenado. La parte (?) indica que recibirá un parámetro*/
		$sql="CALL cambiarEstadoProd(?)";
		$this->db->query($sql,intval($id));
	}

	/*permite buscar la descripción, precio y cuotas de un producto para mostrar*/
	public function buscarDetalles($idProducto) {
		$this->db->select('*');
		$this->db->from('productos');
		$this->db->where('id_producto',$idProducto);
		/*vinculo los productos con sus subcategorias*/
		$this->db->join('subcategorias','subcategorias.id=productos.subcategoria');
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.idCategoria');
		$resultado=$this->db->get();
		return $resultado->row();
	}

	/*modifica la foro*/
	public function modificarFoto($id,$nombre) {
		
		$this->db->set('nombreFoto',$nombre);
		$this->db->where('id_producto',$id);
		$this->db->update('productos');

	}

	
	/*permite modificar los datos de un producto*/
	public function modificarProducto($datos,$id) {
		
		/*reviso si el usuario ha elegido una nueva foto o ha dejado la elección en blanco,
		si es así, no modifico la foto*/
				
		$this->db->where('id_producto',$id);
		$this->db->update('productos',$datos);
	}

	/*obtiene los productos destacados, siendo estos los más caros*/
	public function getProductosDestacados(){
		$this->db->select('*');
		$this->db->from('productos');
		$this->db->where('estado', 1);
		/*selecciono sólo los productos para los cuales hay stock*/
		$this->db->where('stock >', 0);
		$this->db->order_by('precio','DESC');
		/*vinculo los productos con sus subcategorias*/
		$this->db->join('subcategorias','subcategorias.id=productos.subcategoria');
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.idCategoria');
		$this->db->limit(4);
		$consulta=$this->db->get();
		return $consulta->result();
	}

	/*obtiene los productos nuevo, siendo estos los últimos ingresados a la db*/
	public function getProductosNuevos(){
		$this->db->select('*');
		$this->db->from('productos');
		$this->db->where('estado', 1);
		/*selecciono sólo los productos para los cuales hay stock*/
		$this->db->where('stock >', 0);
		$this->db->order_by('fecha','DESC');
		/*vinculo los productos con sus subcategorias*/
		$this->db->join('subcategorias','subcategorias.id=productos.subcategoria');
		$this->db->limit(4);
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.idCategoria');
		$consulta=$this->db->get();
		return $consulta->result();
	}

	public function actualizarStock($id,$stock) {
		$producto=$this->get_ProductoId($id);
		$stockActual=$producto->stock;
		$stockActual=$stockActual-$stock;
		$this->db->set('stock',$stockActual);
		$this->db->where('id_producto',$id);
		$this->db->update('productos');
	}

	public function getCantidadCategoria($id) {
		/*indico que seleccionaré todos los registros*/
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('productos');
		/*vinculo los productos con sus subcategorias*/
		$this->db->join('subcategorias','subcategorias.id=productos.subcategoria');
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.IdCategoria');
		$this->db->where('IdCategoria=',$id);
		

		/*realizo la consulta*/
		$consulta=$this->db->get();
		/*retorno la consulta para que sea usada por el controlador*/
		return $consulta->num_rows();
	}

	public function getCantidadSubcategoria($id) {
		/*indico que seleccionaré todos los registros*/
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('productos');
		/*vinculo los productos con sus subcategorias*/
		$this->db->join('subcategorias','subcategorias.id=productos.subcategoria');
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.IdCategoria');
		$this->db->where('subcategoria=',$id);
		

		/*realizo la consulta*/
		$consulta=$this->db->get();
		/*retorno la consulta para que sea usada por el controlador*/
		return $consulta->num_rows();
	}

	public function getCantidadNombre($nombre) {
		/*indico que seleccionaré todos los registros*/
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('productos');
		/*vinculo los productos con sus subcategorias*/
		$this->db->join('subcategorias','subcategorias.id=productos.subcategoria');
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.IdCategoria');
		$this->db->where('IdCategoria',$id);
		

		/*realizo la consulta*/
		$consulta=$this->db->get();
		/*retorno la consulta para que sea usada por el controlador*/
		return $consulta->num_rows();
	}

	public function getCategoriaAdm($id,$limite,$fila) {
				/*indico que seleccionaré todos los registros*/
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('productos');
		/*vinculo los productos con sus subcategorias*/
		$this->db->join('subcategorias','subcategorias.Id=productos.subcategoria');
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.IdCategoria');
		$this->db->where('IdCategoria=',$id);
		
		$this->db->limit($limite,$fila); 

		/*realizo la consulta*/
		$consulta=$this->db->get();
		
		/*retorno la consulta para que sea usada por el controlador*/
		return $consulta->result();
	}

	public function getSubcategoriaAdm($id,$limite,$fila) {
				/*indico que seleccionaré todos los registros*/
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('productos');
		/*vinculo los productos con sus subcategorias*/
		$this->db->join('subcategorias','subcategorias.Id=productos.subcategoria');
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.IdCategoria');
		$this->db->where('subcategoria=',$id);
		
		$this->db->limit($limite,$fila); 

		/*realizo la consulta*/
		$consulta=$this->db->get();
		
		/*retorno la consulta para que sea usada por el controlador*/
		return $consulta->result();
	}

	public function getNombreAdm($id,$limite,$fila) {
		
	}

	public function getDescripcionAdm($id,$limite,$fila) {
		
	}

	public function getMayorAMenorAdm($limite,$fila) {
		/*indico que seleccionaré todos los registros*/
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('productos');
		/*vinculo los productos con sus subcategorias*/
		$this->db->join('subcategorias','subcategorias.id=productos.subcategoria');
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.IdCategoria');
		$this->db->order_by('precio','DESC');
		$this->db->limit($limite,$fila); 

		/*realizo la consulta*/
		$consulta=$this->db->get();
		/*retorno la consulta para que sea usada por el controlador*/
		return $consulta->result();
	}

	public function getMenorAMayorAdm($limite,$fila) {
		/*indico que seleccionaré todos los registros*/
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('productos');
		/*vinculo los productos con sus subcategorias*/
		$this->db->join('subcategorias','subcategorias.id=productos.subcategoria');
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.IdCategoria');
		$this->db->order_by('precio','ASC');
		$this->db->limit($limite,$fila); 

		/*realizo la consulta*/
		$consulta=$this->db->get();
		/*retorno la consulta para que sea usada por el controlador*/
		return $consulta->result();
	}


	public function getFechaMenorAdm($limite,$fila) {
		/*indico que seleccionaré todos los registros*/
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('productos');
		/*vinculo los productos con sus subcategorias*/
		$this->db->join('subcategorias','subcategorias.id=productos.subcategoria');
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.IdCategoria');
		$this->db->order_by('fecha','ASC');
		$this->db->limit($limite,$fila); 

		/*realizo la consulta*/
		$consulta=$this->db->get();
		/*retorno la consulta para que sea usada por el controlador*/
		return $consulta->result();
	}

	public function getFechaMayorAdm($limite,$fila) {
		/*indico que seleccionaré todos los registros*/
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('productos');
		/*vinculo los productos con sus subcategorias*/
		$this->db->join('subcategorias','subcategorias.id=productos.subcategoria');
		/*vinculo las subcategorias con su categoria*/
		$this->db->join('categoria_producto','categoria_producto.id=subcategorias.IdCategoria');
		$this->db->order_by('fecha','DESC');
		$this->db->limit($limite,$fila); 

		/*realizo la consulta*/
		$consulta=$this->db->get();
		/*retorno la consulta para que sea usada por el controlador*/
		return $consulta->result();
	}



}