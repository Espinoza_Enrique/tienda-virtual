<?php

/*Modelo que permite trabajar con la tabla de clientes*/

class ModeloCliente extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	/*método que carga los datos del cliente en la base de datos*/
	public function guardar_cliente($data) {
		$this->db->insert('personas',$data);

	}

	/*método que almacena los datos del usuario en la base de datos*/
	public function guardar_usuario($data) {
		$this->db->insert('usuario',$data);
	}


	/*obtiene los datos de las personas*/
	public function getPersona($id) {
		$this->db->select('*');
		/*selecciona los datos del producto*/
		$this->db->from('personas');
		
		$this->db->where('Id',$id);
		$consulta=$this->db->get();
		/*realizo la consulta*/
		return $consulta->row();
	}

}