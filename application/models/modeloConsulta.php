<?php
class ModeloConsulta extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	/*obtengo los motivos de consulta*/
	public function getMotivos() {
		$this->db->select('*');
		$this->db->from('motivo_consulta');
		$query=$this->db->get();
		return $query->result();
	}
	public function getConsultas() {
		$this->db->select('*');
		$this->db->from('consultas');
		$this->db->order_by('leido','ASC');
		$this->db->join('motivo_consulta','motivo_consulta.id=consultas.motivo');
		$query=$this->db->get();
		return $query->result();
	}

	public function getConsultaId($id) {
		$this->db->select('*');
		$this->db->from('consultas');
		$this->db->where('id_consulta',$id);
		$this->db->join('motivo_consulta','motivo_consulta.id=consultas.motivo');
		$query=$this->db->get();
		return $query->row();
	}

	public function guardarConsulta($data) {
		$this->db->insert('consultas',$data);
	}

	public function marcarComoLeido($id) {
		$this->db->set('leido',1);
		$this->db->where('id_consulta',$id);
		$this->db->update('consultas');
	}
}