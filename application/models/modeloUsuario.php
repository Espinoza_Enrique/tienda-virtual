<?php
class ModeloUsuario extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function buscarUsuario($mail,$contrasenia){
		$this->db->select('*');
		$this->db->from('usuario');
		$this->db->where('mail',$mail);
		$this->db->where('password',$contrasenia);
		$this->db->where('estado',1);
		$consulta=$this->db->get();
		return $consulta->row();
	}

	function buscarPersona($personaId){
		$this->db->select('*');
		$this->db->from('personas');
		$this->db->where('Id',$personaId);
		$consulta=$this->db->get();
		return $consulta->row();
	}

	function buscarPersonaDni($dni){
		$this->db->select('*');
		$this->db->from('personas');
		$this->db->where('dni',$dni);
		$consulta=$this->db->get();
		return $consulta->row();
	}

	function existeUsuario($mail,$contra){
		$this->db->select('*');
		$this->db->from('usuario');
		$this->db->where('mail',$mail);
		$this->db->where('password',$contra);
		$this->db->where('estado',1);
		$consulta=$this->db->get();
		return $consulta->row();

	}
}