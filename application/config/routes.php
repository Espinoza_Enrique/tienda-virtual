<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['ListaProductos']="ControlProductos/mostrarDescripcion";
$route['ListaProductos/(:any)']="ControlProductos/ordenarProductos/$6/$0";
$route['ListaProductos/(:any)/(:any)']="ControlProductos/ordenarProductos/$6/$0";
$route['ListaProductos/(:any)/(:any)/(:any)']="ControlProductos/ordenarProductos/$1/$1/$0";
$route['ordenarProductos']="ControlCarrito/agregarCarrito";
$route['registrarse']= 'ControlCliente';
$route['loggin']='ControlUsuario/cargarFormularioLoggin';
$route['contactoyconsultas']='ControlConsulta/cargarFormularioContacto';
$route['quienessomos']='QuienesSomos';
$route['ayuda']='Ayuda';
$route['terminos_de_uso_y_politicas']='Ayuda/mostrarTerminosYPoliticas';
$route['cargarProductos']='ControlProductos';
$route['registrarProductos']="ControlProductos/registrarProductos";
$route['logginUsuario']='ControlUsuario/loggin_usuario';
$route['registrarCliente']='ControlCliente/registrar_cliente';
$route['mostrarProductos']='ControlProductos/mostrarProductos';
$route['mostrarProductos/(:any)']='ControlProductos/mostrarProductos/$1';
$route['detallesProductos']='ControlProductos/mostrarDescripcionProductos';
$route['cerrarSesion']='ControlUsuario/cerrarSesion';
$route['inicio']='Welcome';
$route['ordenarProducto']="ControlProductos/ordenarProductos";
$route['mostrarDescripcion/(:any)']="ControlProductos/mostrarDescripcion/$1";
$route['descripcion/(:any)']="ControlProductos/ventanaDescripcion/$1";
$route['usuarioLogueado']='Welcome/visualizarUsuarioLogueado';
$route['modificarEstado']='ControlProductos/modificarEstado';
$route['editarProducto']="ControlProductos/formularioModificar";
$route['activarProducto']="ControlProductos/activarProducto";
$route['desactivarProducto']="ControlProductos/desactivarProducto";
$route['aplicarEdicion']="ControlProductos/controlarModificacion";
$route['ingresoAdministrador']="Welcome/visualizarAdministrador";
$route['cargarConsulta']="ControlConsulta/controlarConsultas";
$route['agregarCarrito']="ControlCarrito/agregarCarrito";
$route['Carrito']="ControlCarrito/vistaCarrito";
$route['error']="ControlCliente/mostrarError";
$route['eliminar']="ControlCarrito/eliminarUnidad";
$route['productoEliminado']="ControlCarrito/eliminarProducto";
$route['destruirCarrito']="ControlCarrito/eliminarCarrito";
$route['realizarCompra']="ControlPedido/guardarPedido";
$route['descripcion/(:num)']="ControlProducto/getDescripcion";
$route['agradecimiento']="ControlCarrito/mostrarAgradecimiento";
$route['listaCompras']="ControlUsuario/mostrarAgradecimiento";
$route['mostrarCompras']="ControlPedido/mostrarCompras";
$route['mostrarCompras/(:any)']="ControlPedido/mostrarCompras/$1";
$route['comprasTotales']="ControlPedidoAdm/mostrarComprasAdm";
$route['comprasTotales/(:any)']="ControlPedidoAdm/mostrarComprasAdm/$1";
$route['detallesTotales/(:any)']="ControlPedidoAdm/mostrarDetallesAdm/$1";
$route['detallesCompraCliente']="ControlPedido/mostrarDetallesCompra";
$route['filtrarPorFecha']="ControlPedidoAdm/controlarFechas";
$route['filtrarPorFecha/(:any)/(:any)']="ControlPedidoAdm/mostrarComprasFecha/$1/$1";
$route['filtrarPorFecha/(:any)/(:any)/(:any)']="ControlPedidoAdm/mostrarComprasFecha/$1/$1/$1";
$route['fechasIncompatibles/(:any)/(:any)/(:any)']="ControlPedidoAdm/errorFecha/$1";
$route['fechasIncompatibles/(:any)']="ControlPedidoAdm/errorFecha/$1";
$route['fechasIncompatibles/(:any)/(:any)']="ControlPedidoAdm/errorFecha/$1/$1";
$route['mostrarConsultas']="ControlConsultaAdm/mostrarConsultas";
$route['detallesConsultas/(:any)']="ControlConsultaAdm/mostrarDetallesConsulta/$1";
//$route['productosCategoriaAdm']="ControlProductos/mostrarCategoria";
$route['productosCategoriaAdm/(:any)']="ControlProductos/mostrarCategoria/$1";
$route['productosCategoriaAdm/(:any)/(:any)']="ControlProductos/mostrarCategoria/$1/$1";
$route['productosSubcategoriaAdm/(:any)']="ControlProductos/mostrarSubcategoria/$1";
$route['productosSubcategoriaAdm/(:any)/(:any)']="ControlProductos/mostrarSubcategoria/$1/$1";
$route['productosMayorMenor']="ControlProductos/mostrarProductosMayor";
$route['productosMayorMenor/(:any)']="ControlProductos/mostrarProductosMayor/$1";
$route['productosMenorMayor']="ControlProductos/mostrarProductosMenor";
$route['productosMenorMayor/(:any)']="ControlProductos/mostrarProductosMenor/$1";
$route['productosFechaMayor']="ControlProductos/mostrarFechasMayor";
$route['productosFechaMenor']="ControlProductos/mostrarFechasMenor";
$route['productosFechaMayor/(:any)']="ControlProductos/mostrarFechasMayor/$1";
$route['productosFechaMenor/(:any)']="ControlProductos/mostrarFechasMenor/$1";
$route['formularioRegistro']="ControlProductos";
$route['cantidadesRecaudacion']="ControlPedidoAdm/mostrarCantidadesCat";
$route['recaudacionSubcategoria/(:any)']="ControlPedidoAdm/mostrarCantidadesSub/$1";
$route['filtroCompras']="ControlPedidoAdm/controlarId";
$route['facturasErroneas/(:any)']="ControlPedidoAdm/errorDeFiltro/$1";
$route['facturasErroneas/(:any)/(:any)']="ControlPedidoAdm/errorDeFiltro/$1/$1";
$route['marcarLeido/(:any)']="ControlConsultaAdm/marcarLeido/$1";
$route['descripcionProductoAdm/(:any)/(:any)']="ControlProductos/mostrarDescripcionAdm/$1/$1";
$route['descripcionProductoAdm/(:any)']="ControlProductos/mostrarDescripcionAdm/$1";
$route['buscarPorPalabra']="ControlProductos/buscarPalabraClave";
$route['buscarPorPalabra/(:any)']="ControlProductos/VisualizarProductosPalabraClave/$1";
$route['buscarPorPalabra/(:any)/(:any)']="ControlProductos/VisualizarProductosPalabraClave/$1/$1";

