
<section class="mt-5 container">
	<?php 
	$nombre=$producto->nombre_producto;
	$descripcion=$producto->descripcion;
	$garantia=$producto->garantia." días";
	$sub=$producto->nombreSub;
  $id=$producto->id_producto;
  $stock=$producto->stock;
  $estado=$producto->estado;
	$precio="$".$producto->precio;
	 $rutaImagen=base_url(). "uploads/imagenes/".$producto->nombreFoto;?>
   <div class="card">
    <div class="card-body bg-dark">
	<div class="row">
		<div class="col-auto mx-auto">
		  <img class="w-75" src=<?php echo $rutaImagen;?> width=450 class=" d-flex mx-auto" alt="...">
		  <h3 class="card-title text-center text-white" style= "font-size: 1rem;"><?php echo $nombre;?></h3>
		</div>
	</div>
<form action="<?=base_url('agregarCarrito')?>" method="post" novalidate> 
    <p class="d-flex justify-content-center">
      <a class="btn btn-primary mx-3 text-white" style= "font-size: 0.8rem;" data-toggle="modal" data-target="#descripcion"  role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Detalles del Producto</a>
      <button class="btn btn-primary mx-3" style= "font-size: 0.8rem;" type="button" data-toggle="modal" data-target="#precio" aria-expanded="false" aria-controls="multiCollapseExample2">Ver Precio y Garantía</button>
      <?php if($this->session->userdata('perfil')==2 and $producto->stock>0 and $producto->estado==1) {;?>
          
        <button class="btn btn-primary mx-3"  name="id" type="submit-form" value=<?php echo $producto->id_producto;?>>Agregar a Carrito</button> 
        
      <?php };?>
  </form>
</p>
</form>
<div>
</div>

<!-- Modal -->
<div class="modal fade" id="descripcion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo $nombre;?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h1 class="display-4" style= "font-size: 1rem;"><?php echo $nombre;?></h1>
        <p class="lead" style= "font-size: 1rem;"> <?php echo $descripcion;?></p>
      </div>
      <!--<div class="modal-body">
        <div class="jumbotron">
        <h1 class="display-4"><?php echo $nombre;?></h1>
        <p class="lead"> <?php echo $descripcion;?></p>
        <hr class="my-4">
      </div>-->
       
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <?php if($this->session->userdata('perfil')==2 and $stock>0 and $estado==1) {;?>
          <form action="<?=base_url('agregarCarrito')?>" method="post" novalidate> 
            <button type="form-submit" style= "font-size: 1rem;" name="id" class="btn btn-primary" value=<?php echo $id;?>>Agregar A Carrito</button>
          </form>
        <?php };?>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="precio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo $nombre;?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h3>Precio: <?php echo $precio;?></h1>
        <h3>Garantia: <?php echo $garantia;?></h1><br>
        <h5>Los días de garantías empiezan a correr desdes del 15° día después de computada la compra</h3><br>
        <h5><a href=<?php echo base_url('terminos_de_uso_y_politicas');?>>Haga clic aquí para ver las politicas de envío, pago y garantías</a></h3><br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <?php if($this->session->userdata('perfil')==2 and $stock>0 and $estado==1) {;?>
          <form action="<?=base_url('agregarCarrito')?>" method="post" novalidate> 
            <button type="form-submit" name="id" class="btn btn-primary" value=<?php echo $id;?>>Agregar A Carrito</button>
          </form>
        <?php };?>
      </div>
    </div>
  </div>
</div>

</section>

