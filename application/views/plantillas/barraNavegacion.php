 <header>
  
      <nav class="bg-primary" style= "font-size: 0.8rem;">
    
             <div id="container">
            <ul class="nav justify-content-end">
              

              <li class="nav-item ">
                 <a class="navbar-brand bg-primary" href="<?php echo base_url('loggin');?>" >
                      <img data-toggle="tooltip" data-placement="right" title="Iniciar Sesión" class="w-100 iconosNavegacion" src="<?php echo base_url();?>assets/img/Logotipos/loggin.png"/>
                  </a>
              </li>

               
               <li class="nav-item ">
                 <a class="navbar-brand bg-primary" href="<?php echo base_url('registrarse');?>" >
                      <img data-toggle="tooltip" data-placement="right" title="Crear Cuenta" class="w-100 iconosNavegacion" src="<?php echo base_url();?>assets/img/Logotipos/nuevaCuenta.png"/>
                  </a>
              </li>

              
              <li>
                 <form class="form-inline d-flex flex-nowrap" action="<?=base_url('buscarPorPalabra')?>" method="post" novalidate id="buscar">
                    <input class="form-control" type="search" name="palabra" placeholder="Buscar" aria-label="Search">
                        <button class="btn btn-outline-success  text-white" value="Buscar" type="submit">
                            <img data-toggle="tooltip" data-placement="right" title="Buscar" class="iconosNavegacion" src="<?php echo base_url();?>assets/img/Logotipos/lupa.png"/>
                         </button>
                  </form>
              </li>
              
            </ul>
                  
          </div>
        </nav>

  </header>