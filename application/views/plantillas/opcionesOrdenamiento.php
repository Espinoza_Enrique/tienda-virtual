

<section class="container w-50  d-flex justify-content-center">
   <nav class="navbar navbar-expand-lg p-0 navbar-light bg-info mx-3 mt-3" style= "font-size: 0.9rem;">
  

    <div class="bg-info" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        
        <li class="nav-item dropdown font-weight-bold" data-toggle="tooltip" data-placement="top" title="Ordenar">
          <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           <?php echo $orden;?>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item " href=<?php echo base_url('ListaProductos')."/".$mostrarOrden."/1";?>>Alfabeticamente A-Z</a>
            <a class="dropdown-item " href=<?php echo base_url('ListaProductos')."/".$mostrarOrden."/2";?>>Alfabeticamente Z-A</a>
            <a class="dropdown-item " href=<?php echo base_url('ListaProductos')."/".$mostrarOrden."/3";?>>Mayor a Menor Precio</a>
            <a class="dropdown-item " href=<?php echo base_url('ListaProductos')."/".$mostrarOrden."/4";?>>Menor a Mayor Precio</a>
          </div>
        </li>

      </ul>
    </div>
  </nav>
 </section>
