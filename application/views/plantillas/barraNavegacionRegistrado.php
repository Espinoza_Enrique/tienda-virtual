 <header>
  
      <nav class="bg-primary" style= "font-size: 0.8rem;">
    
             <div id="container">
            <ul class="nav justify-content-end">
              
              <li class="nav-item">
                <div class="d-flex flex-nowrap">
                  <p class="mt-2 text-white"><?php echo $this->session->userdata('nombre');?></p>
                   <a class="navbar-brand bg-primary btn-outline-success" href="<?php echo base_url('inicio');?>" >
                      <img data-toggle="tooltip" data-placement="right" class="w-100 iconosNavegacion" src="<?php echo base_url();?>assets/img/Logotipos/cuenta.png" title="Mi Cuenta"/>
                  </a>
                </div>
              </li>

               <li class="nav-item">
                 <a class="navbar-brand bg-primary" href="<?php echo base_url('Carrito');?>" >
                      <img data-toggle="tooltip" data-placement="right" title="Ver Carrito" class="w-100 iconosNavegacion" src="<?php echo base_url();?>assets/img/Logotipos/carrito.png"/>
                  </a>
              </li>

               <li class="nav-item">
                 <a class="navbar-brand bg-primary" href="<?php echo base_url('mostrarCompras');?>" >
                      <img data-toggle="tooltip" data-placement="right" title="Lista de Compras" class="w-100 iconosNavegacion" src="<?php echo base_url();?>assets/img/Logotipos/lista.png"/>
                  </a>
              </li>
              
              

              <li class="nav-item">
                 <a class="navbar-brand bg-primary" href="<?php echo base_url('cerrarSesion');?>" >
                      <img data-toggle="tooltip" data-placement="right" title="Cerrar Sesion" class="w-100 iconosNavegacion" src="<?php echo base_url();?>assets/img/Logotipos/cancelarsesion.png"/>
                  </a>
              </li>

              

             
              <li>
                 <form class="form-inline d-flex flex-nowrap" action="<?=base_url('buscarPorPalabra')?>" method="post" novalidate id="buscar">
                    <input class="form-control" type="search" name="palabra" placeholder="Buscar" aria-label="Search">
                        <button class="btn btn-outline-success  text-white" value="Buscar" type="submit">
                            <img data-toggle="tooltip" data-placement="right" title="Buscar" class="iconosNavegacion" src="<?php echo base_url();?>assets/img/Logotipos/lupa.png"/>
                         </button>
                  </form>
              </li>
            </ul>
                  
          </div>
        </nav>

  </header>