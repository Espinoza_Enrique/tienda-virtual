<hearder>
     
<div class="d-flex justify-content-end">
      <nav class="navbar navbar-expand-lg p-0 navbar-light bg-info mx-5 mt-3 w-40 d-flex justify-content-end" style= "font-size: 0.9rem;">
   
    <button class="navbar-toggler bg-ligth" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon "></span>
    </button>

    <div class="collapse navbar-collapse bg-info" id="navbarSupportedContent">
      <ul class="navbar-nav  d-flex justify-content-end mr-auto">
        
        <li class="nav-item dropdown dropleft font-weight-bold" data-toggle="tooltip" data-placement="top" title="Filtro por Fecha">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Filtrar Por Fecha
          </a>
          <div class="dropdown-menu bg-primary" aria-labelledby="navbarDropdown">
            <a class="bg-primary mx-3" >
                  <form class="needs-validation form-inline  bg-primary mx-2" action="<?=base_url("filtrarPorFecha")?>" method="post" novalidate>
                  <div class="row mx-2">
                    <div class="col-auto mt-2 text-white">
                      <label>Fecha Inicio</label>
                        <input type="date" id="fechaInicio" class="form-input" name="fechaInicio">
                  </div>
                    <div class="col-auto mt-2 text-white">
                        <label>Fecha Final</label>
                        <input type="date" class="form-input" id="fechaFin" name="fechaFin">
                  </div>
                    <div class="col-auto mb-2 mt-1">
                        <button type="form-submit" class="btn btn-primary border mx-5">Filtrar</button>
                  </div>
                </div>
                <span class="text-danger d-flex justify-content-center  class="textoTamaño8""><?php echo form_error('fechaInicio');
                ?></span>
              </form>
            </a>
            
          </div>
        </li>
        <li class="nav-item dropdown dropleft font-weight-bold" data-toggle="tooltip" data-placement="top" title="Filtro Por Id">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Filtrar Por Id Orden
          </a>
          <div class="dropdown-menu bg-primary" aria-labelledby="navbarDropdown">
            <a class="mx-3 bg-primary">
                   <form class="form-inline d-flex flex-nowrap mx-2 w-75" id="buscar" action="<?=base_url("filtroCompras")?>" method="post" novalidate>
                    <input class="form-control" type="search" placeholder="Ingrese el Id" aria-label="Search" name="idOrden">
                        <button class="btn btn-outline-success  text-white" value="Buscar" type="submit">
                            <img data-toggle="tooltip" data-placement="right" title="BuscarId" class="iconosNavegacion" src="<?php echo base_url();?>assets/img/Logotipos/lupa.png"/>
                         </button>
                  </form>
            </a>
           
          </div>
        </li>
         <li class="nav-item dropdown dropleft font-weight-bold" data-toggle="tooltip" data-placement="top" title="Mostrar todas las compras">
          <a class="nav-link mx-3" href="<?=base_url("comprasTotales")?>" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
            Mostrar todas las compras
          </a>
         
          </div>
        </li>
      </ul>
    </div>
  </nav>
  </div>
  </header>