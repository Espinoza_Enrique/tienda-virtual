﻿	
	<br>
	<br>
	<br>
	<br>
	<footer class="container-fluid text-white bg-primary" style="font-size: 0.8rem;">

		<div class="container-fluid">
			<div class="row">
				<div class="col-2-lg col-3-md col-2-sm col-xs-1 mt-2" >
					<ul class="nav flex-column">
						  <li class="nav-item">
							    <!--teléfono de contacto-->
							    <a class="navbar-brand bg-primary" >
							        <img data-toggle="tooltip" data-placement="right" class="w-100" src="<?php echo base_url();?>assets/img/Logotipos/telefono.png" title="..."/>
							    </a>
						          
						    	<span>3794-34678934</span><br>
						  </li>
						  <li class="nav-item">
							    <a class="navbar-brand bg-primary" >
							        <img data-toggle="tooltip" data-placement="right" class="w-100" src="<?php echo base_url();?>assets/img/Logotipos/whatsapp.png" title="..."/>
							    </a>
							    <span>3794-456789</span> <br>
						  </li>
						  <li class="nav-item">
							    <a class="navbar-brand bg-primary" >
							        <img data-toggle="tooltip" data-placement="right" class="w-100" src="<?php echo base_url();?>assets/img/Logotipos/arroba.png" title="..."/>
							    </a>

							    <span>elcalabozocontacto@gmail.com<br>
						  </li>

						  <li class="nav-item">
							    <!--domicilio-->
							     <a class="navbar-brand bg-primary " >
							             <img data-toggle="tooltip" data-placement="right" class="w-100" src="<?php echo base_url();?>assets/img/Logotipos/domicilio.png" title="..."/>
							     </a>
							     <span>Junin 1235 Corrientes, Capital<span> <br>
						  </li>
				  </ul>
					
				</div>


				<div class=" col-2-lg col-3-md col-2-sm col-xs-1 px-2 mt-2 " >
					<ul class="nav flex-column">
						  <li class="nav-item">
							    <!--teléfono de contacto-->
							    <a class="navbar-brand bg-primary" >
							        <img data-toggle="tooltip" data-placement="right" class="w-100" src="<?php echo base_url();?>assets/img/Logotipos/twitter.png" title="..."/>
							    </a>
						          
						    	<span>Twitter</span><br>
						  </li>
						  <li class="nav-item">
							    <a class="navbar-brand bg-primary" >
							        <img data-toggle="tooltip" data-placement="right" class="w-100" src="<?php echo base_url();?>assets/img/Logotipos/facebook.png" title="..."/>
							    </a>
							    <span>Fecebook</span> <br>
						  </li>
						  <li class="nav-item">
							    <a class="navbar-brand bg-primary" >
							        <img data-toggle="tooltip" data-placement="right" class="w-100" src="<?php echo base_url();?>assets/img/Logotipos/instagram.png" title="..."/>
							    </a>

							    <span>Instagram</span><br>
						  </li>

				  </ul>
					
				</div>

				<div class=" col-2-lg col-3-md col-2-sm col-xs-1 col-1-xs xs-sm-5 mx-5 mt-2">
					<!--<span class="mt-5">Políticas y Ayuda<span> <br><br>-->
					<br>
					<a href=<?php echo base_url('terminos_de_uso_y_politicas');?>>
						<span class="text-white">Ver Términos y Condiciones de Uso<span> <br>
					</a> 
					<a class="text-white" href="<?php echo base_url('ayuda');?>">
						<span>Ver Ayudas<span> <br>
					</a>

					<a class="text-white" href="<?php echo base_url('contactoyconsultas');?>"> Contacto</a><br>
					<a class="text-white" href="<?php echo base_url('quienessomos');?>"> ¿Quienes Somos?</a><br>

			
		</div>

		<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url();?>/assets/js/jquery-3.3.1.slim.js"></script>
	<script src="<?php echo base_url();?>/assets/js/popper.min.js"></script>
	<script src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>/assets/js/fetch.js"></script>

	</footer>
    

  </body>
</html>