<header>
  <div class="d-flex justify-content-end">
    <nav class="navbar navbar-expand-lg p-0 navbar-light bg-info mx-5 mt-3 w-40 justify-center" style= "font-size: 0.9rem;">
     
      <button class="navbar-toggler bg-ligth" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon "></span>
      </button>

      <div class="collapse navbar-collapse bg-info justify-content-end" id="navbarSupportedContent">
        <ul class="navbar-nav  mr-auto">
          <li class="text-center">
            <label class=" font-weight-bold">Filtrar Por</label>
          </li>
          <li class="nav-item dropdown font-weight-bold" data-toggle="tooltip" data-placement="top" title="Juegos de Mesa">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Categorìa
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <?php 
                foreach ($categorias as $row) {?>
                    <a class="dropdown-item " href=<?php echo base_url('productosCategoriaAdm/'.$row->id);?>><?php echo $row->nombreCat;?></a>
                <?php };?>
              <!--<a class="dropdown-item " href=<?php echo base_url('productosCategoriaAdm'."/1");?>>Juegos de Mesa</a>
              <a class="dropdown-item " href=<?php echo base_url('productosCategoriaAdm')."/2";?>>Cartas</a>
              <a class="dropdown-item " href=<?php echo base_url('productosCategoriaAdm')."/3";?>>Miniaturas</a>
              <a class="dropdown-item " href=<?php echo base_url('productosCategoriaAdm')."/4";?>>Accesorios</a>
              <a class="dropdown-item " href=<?php echo base_url('productosCategoriaAdm')."/5";?>>Juegos de Rol</a>-->
            </div>
          </li>
          <li class="nav-item dropdown font-weight-bold" data-toggle="tooltip" data-placement="top" title="Cartas">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Precio
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href=<?php echo base_url('productosMayorMenor');?>>Mayor a Menor</a>
              <a class="dropdown-item" href=<?php echo base_url('productosMenorMayor');?>>Menor a Mayor</a>
            </div>
          </li>

           <li class="nav-item dropdown dropleft font-weight-bold">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Fecha
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href=<?php echo base_url('productosFechaMayor');?>>Nuevos a Antiguos</a>
              <a class="dropdown-item" href=<?php echo base_url('productosFechaMenor');?>>Antiguos a Nuevos</a>
            </div>
          </li>

           <li class="nav-item dropdown dropleft font-weight-bold" data-toggle="tooltip" data-placement="top" title="Juegos de Mesa">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Subcategorias
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <?php foreach ($subcategorias as $row){?>
              <a class="dropdown-item " href=<?php echo base_url('productosSubcategoriaAdm/'.$row->Id);?>><?php echo $row->nombreSub;?></a>
              <?php };?>
              <!--<a class="dropdown-item " href=<?php echo base_url('productosSubcategoriaAdm')."/2";?>>Juegos de Mesa-Abstracto</a>
              <a class="dropdown-item " href=<?php echo base_url('productosSubcategoriaAdm')."/3";?>>Juegos de Mesa-Wargames</a>
              <a class="dropdown-item " href=<?php echo base_url('productosSubcategoriaAdm')."/4";?>>Yu-gi-oh!</a>
              <a class="dropdown-item " href=<?php echo base_url('productosSubcategoriaAdm')."/5";?>>Magic The Gathering</a>
               <a class="dropdown-item " href=<?php echo base_url('productosSubcategoriaAdm'."/6");?>>Pokemon</a>
              <a class="dropdown-item " href=<?php echo base_url('productosSubcategoriaAdm')."/7";?>>Calabozos y Dragones</a>
              <a class="dropdown-item " href=<?php echo base_url('productosSubcategoriaAdm')."/8";?>>La Llamada de Cthulhu</a>
              <a class="dropdown-item " href=<?php echo base_url('productosSubcategoriaAdm')."/9";?>>Cartas-Otros</a>
              <a class="dropdown-item " href=<?php echo base_url('productosSubcategoriaAdm')."/10";?>>Miniaturas</a>
              <a class="dropdown-item " href=<?php echo base_url('productosSubcategoriaAdm')."/11";?>>Accesorios</a>-->
            </div>
          </li>

        </ul>
      </div>
    </nav>
  </div>
</header>
<!--</section>-->