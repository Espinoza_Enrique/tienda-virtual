<!--<section class="container-fluid">-->
  <nav class="navbar navbar-expand-lg p-0 navbar-light bg-info mx-3 mt-3" style= "font-size: 0.9rem;">
    <a class="navbar-brand bg-info" href=<?php  echo base_url('inicio');?> >
        <img class="w-100" src="<?php echo base_url();?>assets/img/Logotipos/ElCalabozo.png"/>
    </a>
    <button class="navbar-toggler bg-ligth" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon "></span>
    </button>

    <div class="collapse navbar-collapse bg-info" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <?php foreach($categorias as $cat) {?>
             
              <li class="nav-item dropdown font-weight-bold" data-toggle="tooltip" data-placement="top" title=<?php echo "Juegos de " . $cat->nombreCat;?>>
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <?php echo ucwords($cat->nombreCat);?>
                  </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <?php foreach ($subcategorias as $sub) {
              if($sub->IdCategoria==$cat->id) {;?>
                  
                    <a class="dropdown-item " href=<?php echo base_url('ListaProductos')."/".$sub->Id."/0";?>> <?php echo ucwords($sub->nombreSub);?></a>
                  
                
              <?php };?>
            
          <?php };?>
          </div>
          </li>
        <?php };?>

         <li class="nav-item dropdown font-weight-bold">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Ayudas y Consultas
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="<?php echo base_url('ayuda');?>">Ayuda</a>
            <a class="dropdown-item" href="<?php echo base_url('ayuda');?>">Comercialización</a>
            <a class="dropdown-item" href="<?php echo base_url('contactoyconsultas');?>">Contacto</a>
            <a class="dropdown-item" href="<?php echo base_url('terminos_de_uso_y_politicas');?>">Políticas y Condiciones de Uso</a>
            <a class="dropdown-item" href="<?php echo base_url('quienessomos');?>">¿Quienes Somos?</a>
          </div>
        </li>

      </ul>
    </div>
  </nav>
<!--</section>-->