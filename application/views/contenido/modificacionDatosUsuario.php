<!--formulario para crear una cuenta nueva-->
<section class="container">
      <h3 class="text-center pt-5"> <span class="badge badge-info">Modifique Los Datos Que Desee</span></h3>
    	   <form class="needs-validation mt-5" action="<?=base_url("registrarCliente")?>" method="post" novalidate>
            <div class="form-row">
              <!--línea para la validación de errores-->
              <!--<?php echo validation_errors(); ?>--> 
              <div class="col-md-4 mb-3">
                <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5 for="validationTooltip01">Nombre</label>
                <input type="text" class="form-control form-input" id="nombre" placeholder="Nombre" value="<?php echo set_value($datos->nombre);?>" name="nombre" required>
                <span class="text-danger"><?php echo form_error('nombre');?></span>
            </div>
            <div class="col-md-4 mb-3">
               <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5" for="validationTooltip02">Apellido</label>
               <input type="text" class="form-control form-input" id="apellido" placeholder="Apellido"  value="<?php echo set_value('$datos->apellido');?>" name="apellido" required>
               <span class="text-danger"><?php echo form_error('apellido')?></span>
              
            </div>

             <div class="col-md-4 mb-3">
               <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5" for="validationTooltip02">Dni</label>
               <input type="number" class="form-control form-input" id="dni" placeholder="Dni"  value="<?php echo set_value('$datos->dni');?>" name="dni" required>
               <span class="text-danger"><?php echo form_error('dni')?></span>
              
            </div>

             <div class="col-md-4 mb-3">
               <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5" for="validationTooltip02">Teléfono</label>
               <input type="text" class="form-control form-input" id="telefono" placeholder="Telefono"  value="<?php echo set_value('$datos->telefono');?>" name="telefono" required>
               <span class="text-danger"><?php echo form_error('telefono')?></span>
              
            </div>

            <div class="col-md-4 mb-3">
               <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5" for="validationTooltipUsername">Mail</label>
               <div class="input-group">
              <input type="text" class="form-control form-input" id="mail" placeholder="Mail" value="<?php echo set_value('$datos->mail');?>" name="mail" aria-describedby="validationTooltipUsernamePrepend" required>
            </div>
            <span class="text-danger"><?php echo form_error('mail');?></span>
          </div>
        </div>
        <div class="form-row">
          <div class="col-sm-6 mb-3">
            <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5" for="validationTooltip03">Coloque la contraseña anterior Contraseña</label>
            <input type="password" class="form-control form-input" name="password" id="password" placeholder="Contraseña" required>
            <span class="text-danger"><?php echo form_error('passwordAntiguo');?></span>
          </div>
          
          <div class="form-row">
          <div class="col-sm-6 mb-3">
            <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5" for="validationTooltip03">Coloque la nueva contraseña</label>
            <input type="password" class="form-control form-input" name="password" id="password" placeholder="Contraseña" required>
            <span class="text-danger"><?php echo form_error('password');?></span>
          </div>

          <div class="col-sm-6 mb-3">
            <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5" for="validationTooltip04">Repita la contraseña</label>
            <input type="password" class="form-control form-input" id="passconf" name="passconf" placeholder="Contraseña" required>
            <span class="text-danger"><?php echo form_error('passconf');?></span>
          </div>
          
          
        </div>
              <span class="text-danger"><?php echo form_error('modificar');?></span>
             

        			<button type="form-submit" class="btn-primary btn-success">Modificar</button>
               <!--<?php echo form_close();?>--> 
      		  </form>
  <!--Script que controla el formulario. Fue guardado en un archivo js para que sea más legible-->
    <!--<script src="<?php echo base_url();?>assets/js/validarNuevaCuenta.js"></script>-->
</section>