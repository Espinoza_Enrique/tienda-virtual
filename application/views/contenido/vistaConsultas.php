<!--muesta la tabla con los productos-->
<section class="container mx-5">
	<!--tabla que muestra las opciones de filtrado para productos-->
	<div class="form-group mx-auto col-auto mt-5 w-50">
	    <a class="textoTamaño7 font-weight-bold" style="font-size: 1rem;">Consultas</a>
	  </div>
	<table id="tablaProductos" class="table table-borded table-striped table- hover table-responsive">
		<!--encabezados de la tabla-->
		<thead>
			<th class="textoTamaño7">Consultor</th>
			<th class="textoTamaño7">Mail</th>
			<th class="textoTamaño7">Motivo</th>
			<th class="textoTamaño7">Ver Consulta</th>
			<th class="textoTamaño7">Fecha Consulta</th>
			<th class="textoTamaño7">Leído</th>
		</thead>

	
		<tbody>
			<?php foreach ($consultas as $row) {?>
			<tr>
				<td class="textoTamaño7"> <?php echo $row->nombre; ?> </td>
				<td class="textoTamaño7"> <?php echo $row->mail; ?> </td>
				<td class="textoTamaño7"> <?php echo $row->motivo; ?> </td>
				<td class="textoTamaño7">

					<a type="button" name="detalles"  href=<?php echo base_url('detallesConsultas')."/".$row->id_consulta?>>
							<img src="<?php echo base_url();?>assets/img/Logotipos/editar.png" data-toggle="tooltip" data-placement="right" title="Detalles"  class="w-100 iconosNavegacion" />
					</a>
					<!--<form action="<?=base_url('detallesConsultas')?>" method="post" novalidate> -->
						
					<!--</form>-->
				</td>

					<td class="textoTamaño7"> <?php echo $row->fecha; ?> </td>
				</td>
					<?php
						if(($row->leido==0)) {?>
							<td>
								
								<span  class="btn btn-primary" name="estado"> <img data-toggle="tooltip" data-placement="right" title="Producto No Leído"  class="w-100 iconosNavegacion" src="<?php echo base_url();?>assets/img/Logotipos/correcto.png"/>
								</span>
									
								
							</td>

					<?php } else { ?>
							<td> 
							    <span  class="btn btn-primary" name="estado"> <img data-toggle="tooltip" data-placement="right" title="Producto Leído"  class="w-100 iconosNavegacion" src="<?php echo base_url();?>assets/img/Logotipos/cancelar.png"/>
								</span>
								
							</td>
					<?php } ?>
				<td>
			</tr>
			<?php }; ?><!--termina el foreach-->
			

		</tbody>
	</table>
	
</section>

