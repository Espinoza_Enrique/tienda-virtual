<section>
	<div class="container">
			<div class="jumbotron mt-5 textoTamaño8">
			  <h1 class="display-4">Terminos y Condiciones De Uso</h1>
			  <p class="lead ">Por favor, lea los términos y condiciones de uso antes de realizar cualquier acción en la página.</p>
			  <hr class="my-4">
			  <p>
			  		<span>Condiciones de Uso</span><br><br>
			  		La navegación por la página es sin restricciones, pero para realizar compras se debe tener una cuenta registrada. Para registrarse haga click en el siguiente enlace 
			  		<a href="<?php echo base_url('registrarse');?>">
			  			Crear Nueva Cuenta
			  	   </a>. 
			  	   Si ya cuenta con un perfil registrado, inicie sesión haciendo click en el siguiente enlace 
			  	   <a href="<?php echo base_url('loggin');?>">
			  	   		Iniciar Sesión
			  	   </a>.<br>
			  		
			  		Las imagenes de los diversos productos mostrados en la página son sólo imagenes ilustrativas y algunos productos pueden diferir en la estética sin que esto afecte la calidad o funcionalidad de los mismos.<br> 
			  		
  			</p>
  					<p class="collapse" id="politicas">
			  		Todas las imagenes son propiedad de sus respectivos dueños y sólo se emplean para dar al usuario una previa visualización de éstos. Está prohibido el uso de las imagenes de los productos con fines lucrativos, directa o indirectamente, por cualquier medio, ya sea físico, digital, pero no necesariamente limitado a ellos.<br>
			  		El logotipo de "El Calabozo del Androide" es marca registrada y se prohibe su reproducción o uso con fines lucrativos, directa o indirectamente, por cualquier medio, ya sea físico, digital, pero no necesariamente limitado a éstos.<br>
			  		Queda expresamente prohibido realizar comentarios, consultas, quejas o cualquier otro tipo de enunciado mediante escritura, audiovisual o gráfica que promueva o incite cualquier tipo de ataque religioso, xenofóbico, cultural, de sexo, de género, edad, ataques a animales, al medio ambiente en general o cualquier otro tipo de agresión que pueda ser considera despectiva, insultante y/o peligrosa para cualquier persona miembro o no de nuestra página web . Esto incluye al personal que trabaja en nuestra sucursal o a los encargados de la página de web, así como a nuestros clientes o cualquier invitado de nuestra página web o sucursal. Ante una duda suficientemente fundada sobre peligro para terceros se dispondrá a entregar todo el material pertinente a las autoridades. No se tolerarán este tipo de comentarios e inmediatamente se dará de baja a cualquier cuenta que incurra en estas actividades o cualquier otra que pueda ser catalogada como amoral e ilegal.
			  		Asimismo, el párrafo anterior se aplica a emails, cartas, llamadas telefónicas, mensajes digitales de cualquier índole, o cualquier otro medio de comunicación o correo que se emplee para contactarse con nosotros o por medio de nuestro servicio web.
			  		Se llevará un registro de las cuentas, mails, cartas, teléfonos y demás medios utilizados para realizar estas actividades y ante la repetición de actitudes de la misma calaña, se actuará acorde a lo dispuesto en párrafos anteriores.<br><br>

			  		<span>Políticas de Devolución</span><br><br>
			  		Las devoluciones de los productos sólo pueden hacerse físicamente en nuestra sucursal sito en la calle  Junin 1235 Corrientes, Capital en la República Argentina.
			  		En caso que el producto haya sido entregado a otra provincia en la república Argentina o en el interior de la provincia de Corrientes, provincia del mencionado país, puede enviarnos un mail a la siguiente dirección de correo electrónico elcalabozocontacto@gmail.com adjuntando sus datos, detalles del producto y fotos que ilustren el problema que especifica para su producto. Tenga en cuenta que cada producto consta de garantías de diferentes períodos de días, meses o años y ante cualquier pedido de cobertura por parte de la garantía, sólo podrá solicitarse dentro del período de tiempo establecido. Este período siempre estará en los detalles del producto antes de la compra del mismo.
			  		Si el producto cumple con los criterios de garantía, se le enviará un producto con las mismas características del afectado y deberá entregar el producto defectuoso al momento de recibir el nuevo.<br>
			  		Si usted se apersona en nuestra sucursal y el producto cumple las condiciones de la garantía, se podrá optar por un nuevo producto de la misma índole o del reembolso del dinero.
			  		En caso de que no se apersone a nuestra sucursaL, deberá enviar el correo electrónico según las indicaciones señaladas anteriormente. Tardaremos máximo 72 horas hábiles en responder su consulta y se le enviará una respuesta mediante el medio de comunicación que ofrezca (teléfono, mail, etc.)
			  		En caso de realizar el pedido de devolución mediante mail y el pedido cumple las condiciones de la garantía, se enviará un producto de iguales características al afectado o, si lo desea, otro producto por el mismo precio. Su decisión deberá ser confirmada mediante mail nuevamente y recibirá el producto en el mismo tiempo de entrega como si comprara un nuevo producto (entre 3 y 7 días hábiles).<br><br>

			  		<span>Politicas de Envío<span><br><br>
			  		El envío de los productos se realiza mediante Correo Argentino. Una vez realizada la compra del producto esta tardará entre 3 y 7 días hábiles para la entrega del mismo. Si no hubiera un adulto en el domicilio al momento de la llegada del paquete, el paquete será devuelto al Correo Argentino y deberá dirigirse al correo para reclamarlo. Generalmente, el correo suele retener durante 3 días el paquete antes de devolverlo a remitente.<br>
			  		Al momento de realizar la compra se le dará unos datos para que pueda rastrear la condición de su pedido.
			  		Con esos datos podrá preguntar a Correo Argentino sobre sus pedido o podrá comunicarse con nosotros para informarle sobre la condición de su pedido.<br><br>

			  		<span>Políticas de Pago</span><br><br>
			  		El pago puede realizarse en nuestra sucursal en efectivo, tarjeta de crédito o débito. Si se realiza de manera online podrá hacerlo con tarjeta de crédito, por paypal o contrareembolso.
			  		También, si usted se encuentra en Corrientes Capital, provincia de la República Argentina, podemos enviar su pedido mediante cadete de la empresa y abonar durante la entrega del producto. Esto durante el día de la compra, de 8 a 20 hs durante días hábiles y sábado de 8 a 12 y de 16 a 20hs.
			  		<br><br>

			  		<span>Políticas de Privacidad</span><br><br>
			  		Sus datos se encuentran resguardados en nuestras bases de datos y sólo se emplean para realizar un registro de sus compras, saber sus datos de contacto y dirección en dónde comunicarnos con usted.<br>
			  		De ninguna manera realizamos comercialización de terceros con sus datos, sólo sirven para llevar un registro, conteo o seguimiento de sus actividades en nuestra página web.
			  		Le solicitamos encarecidamente que sea responsable por su contraseña de ingreso ya que sus datos pueden ser vulnerados si lo brinda a terceros. Usted es ciento por ciento responsable por las actividades que se realizan en su cuenta y sólo se permitirán reclamaciones debido a solicitudes de entidades gubernamentales.<br>
			  		La empresa no se hace responsable por cualquier tipo de actividad maliciosa realizada desde su cuenta. Se asume que la persona conectada a través de la cuenta es el titular de la misma.
			  		Asimismo, ante el pedido de las autoridades gubernamentales pertinentes, previa presentación y constatación de ordenes judiciales o cualquier otro documento que sea pertinente a la situación requerida,  se suministrará los siguientes datos, sin que sean taxativos ni sólo limitados a:<br><br>

			  		*Datos de direcciones, teléfonos, domicilio y demás información similar para la localización del usuario.<br><br>

			  		*Detalles de compras, comentarios, sugerencias, mail o cualquier otro tipo de contacto realizado con nuestra empresa.<br><br>

			  		*Información derivada de sus datos como ser: registro de compras, registros de pagos, y cualquier otro registro, consulta o resultado derivado de su actividad en nuestra página web.<br>

			  </p>
					 <a class="btn btn-primary" data-toggle="collapse" href="#politicas" role="button" aria-expanded="false" aria-controls="collapseExample">
	    						Leer más..
	  				</a>
	  		</div>

	</div>
</section>