<section class="container mx-5">
        <div class="form-group mx-auto col-auto mt-5 w-50">
            <a  style="font-size: 1rem" class="font-weight-bold">Lista de Compras</a>
        </div>

        
        <div class="container d-flex justify-content-start ">
        <table id="tablaProductos" class="table table-borded table-striped table-hover table-responsive mx-5">
                <!--encabezados de la tabla-->
                <thead class="table-secondary">
                        <th class="textoTamaño7 text-center" style="font-size: 0.9rem">Número Factura</th>
                        <th class="textoTamaño7 text-center" style="font-size: 0.9rem">Nombre</th>
                        <th class="textoTamaño7 text-center" style="font-size: 0.9rem">Apellido</th>
                        <th class="textoTamaño7 text-center" style="font-size: 0.9rem">Dni</th>
                        <th class="textoTamaño7 text-center" style="font-size: 0.9rem">Domicilio</th>
                        <th class="textoTamaño7 text-center" style="font-size: 0.9rem">Altura</th>
                        <th class="textoTamaño7 text-center" style="font-size: 0.9rem">Fecha de Compra</th>
                        <th class="textoTamaño7 text-center" style="font-size: 0.9rem">Ver Detalles</th>
                </thead>
                <tbody>
                        <?php 
                        
                        foreach ($pedidos as $filas) { 
                                ?>
                <tr class="table-primary">
                        <td class="textoTamaño7 text-center" style="font-size: 0.9rem"> <?php echo $filas->orden_id;?> </td>
                        <td class="textoTamaño7 text-center" style="font-size: 0.9rem"> <?php echo $filas->nombre; ?> </td>
                        <td class="textoTamaño7 text-center" style="font-size: 0.9rem"> <?php echo $filas->apellido; ?> </td>
                        <td class="textoTamaño7 text-center" style="font-size: 0.9rem"> <?php echo $filas->dni; ?> </td>
                        <td class="textoTamaño7 text-center" style="font-size: 0.9rem"> <?php echo $filas->calle; ?> </td>
                        <td class="textoTamaño7 text-center" style="font-size: 0.9rem"> <?php echo $filas->altura; ?> </td>
                        <td class="textoTamaño7 text-center" style="font-size: 0.9rem"> <?php echo $filas->orden_fecha; ?> </td> 
                        <td>
                            <a type="button" class="mx-auto" name="detalles"  href=<?php echo base_url('detallesTotales')."/".$filas->orden_id?>>
                                <img src="<?php echo base_url();?>assets/img/Logotipos/editar.png" data-toggle="tooltip" data-placement="right" title="Ver Detalles"  class="w-100 iconosNavegacion" />
                             </a>
                       <?php };?>
                        </td>
                    
                </tr>
                </tbody>
        

    </table>
</div>
<?php echo $this->pagination->create_links();?>
</section>