<!--muestra los datelles de quienes somos en la empresa-->

<section>
	<div class="jumbotron px5-pt5 bg-light">
  		<h1 class="display-4 text-center textoTamaño8" style="font-size: 2.5rem;">¿Quienes Somos?</h1>
  		<p class="lead px-5 pt-5 textoTamaño8" style="font-size: 1rem;">
  			Un cordial saludo a todas las personas que visitan nuestra página. <br>
  			"El calabozo del Androide" es un tienda encargada de vender juegos de mesa, cartas, comics, miniaturas y demás juegos para todos los gustos.<br>
  			Nuestra sucursal se encuentra situada físicamente en Junin 1230 en la Ciudad De Corrientes, Capital en la República Argentina.<br>
        <span class="collapse lead pt-5 textoTamaño8" style="font-size: 1rem;" id="quienesSomos">
    		Tenemos desde juegos de estrategia que pondrán a prueba tu astusia y destreza para vencer a tus amigos, juegos de cartas populares como "Magic: The Gathering", hasta clásicos como el "Estanciero" o "La Oca".<br>
    			Es nuestra creencia que, mediante el juego, no sólo se pasa un buen rato entre amigos; sino que también se
    			estrechan los lazos, se arman nuevos, se comparten experiencias y se guardan recuerdos gratos, todo desde
    			un mundo de fantasía, un mundo futurista o siendo un simple estanciero tratando de ganar más dinero que otros.<br>
    			Tratamos de ofrecer productos que diviertan, estimulen la imaginación y sean lo que ustedes, nuestro público, desean para entretenerse unas horas después del trabajo, la escuela, la facultad o un fin de semana largo.<br>
    			Es nuestra visión convertirnos en sinónimo de buena calidad, que entiende los gustos de los jugadores y ser recomendados de boca a boca. No existe mejor publicidad que la de un amigo comentando a otro cómo pudo encontrar ese juego que tanto les divierte en el "Calabozo del Androide".<br>
    			Pero ante todo somos como ustedes: personas que disfrutan de una buena partida de rol con una historia bien relatada por un master no tan experto en "La llamada de Cthulhu", hasta ser el último en mandar un hechizo en la pila y frustrar todos los planes de nuestro amigo para vencernos con su planewalker.<br>
    			Disfrutamos de esta pasión, de estimular nuestra imaginación y ser cada vez más "adictos" (en el buen sentido de la palabra) a una buena historia o un buen sistema de juego mientras disfrutamos de las ilustraciones o miniaturas tan bien detallas.<br>
    			Esperamos decidas formar parte de esta familia de jugadores, estamos seguros que encontrarás lo que desees y que incluso descubrirás nuevas formas de jugar, divertirte e incluso un juego que era el que siempre soñaste, pero no sabías que existía.<br>
    			Desde ya, gracias por tu tiempo, gracias por visitarnos y gracias por darnos la oportunidad de contarte nuestra pasión.<br><br>
    			Enrique Espinoza <br>
    		    <span class="mx-5 collapse" id="quienesSomos">Creador del "Calabozo del Androide." </span>
    		</span>
      </p>
      <!--enlace para que colapse o se muestre el texto-->
       <a class="btn btn-primary mx-5" data-toggle="collapse" href="#quienesSomos" role="button" aria-expanded="false" aria-controls="collapseExample">
                  Leer más..
            </a>
	</div>

</section>