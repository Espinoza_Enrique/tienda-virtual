<section class="container mx-3 mt-3">
	<div class="text-center">
		<span class="badge badge-dark textoTamaño8 pr-4">Cantidades Vendidas y Recaudación Por Categoria</span>
	</div>
	<div class="row justify-content-center mt-3">
		<div class="col justify-content-center">
		</div>
			<div class="col justify-content-center">
				<table  id="tablaProductos" class="table m-auto mt-3 table table-borded table-striped table-hover table-responsive">
				  <thead>
				    <tr>
				      <th scope="col">Categoria</th>
				      <th scope="col">Cantidad</th>
				      <th scope="col">Recaudacion</th>
				      <th scope="col">Detalles</th>
				    </tr>
				  </thead>
				  <tbody>
				  	
						<?php foreach ($productos as $valores=>$datos) {?>
						<tr>
							<td class="textoTamaño7"> <?php echo $datos['categoria']; ?> </td>
							<td class="textoTamaño7"> <?php echo $datos['total']; ?> </td>
							<td class="textoTamaño7"> <?php echo $datos['recaudacion']; ?> </td>
							<td class="textoTamaño7">
								
									<a type="button" name="detalles"  href=<?php echo base_url('recaudacionSubcategoria')."/".$datos['id']?>>
											<img src="<?php echo base_url();?>assets/img/Logotipos/editar.png" data-toggle="tooltip" data-placement="right" title="Detalles"  class="w-100 iconosNavegacion" />
									 </a>
								
							</td>
						</tr>
						<?php } ?><!--termina el foreach-->
					<tr>
					    <td colspan="2"> </td>
	        			<td class="left"><strong>Unidades Totales vendidas</strong></td>
	        			<td class="left"><?php echo $unidadesTotales; ?>
        			</tr>

        			<tr>
	        			<td colspan="2"> </td>
	        			<td class="left"><strong>Recaudacion Total</strong></td>
	        			<td class="left">$<?php echo $recaudacionTotal; ?>
        			</tr>
				  </tbody>
				</table>
		</div>
		<div class="col justify-content-center">
		</div>
    </div>
</section>

