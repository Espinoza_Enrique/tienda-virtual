<!--muestra un formulario para la realización de consultas-->
<section class="container">
			<div>
				 <form class="mt-5 mx-5 align-items-center"  action="<?=base_url('cargarConsulta')?>" method="post" novalidate>
					  <label for="formularioConsulta">Ingrese su nombre y un mail para contactarlo</label>
					  <div class="row">
					    <div class="col">
					      <input type="text" class="form-control" placeholder="Nombre" name="nombre" required>
					    </div>
					    
					    <div class="col">
					      <input type="mail" class="form-control" placeholder="Mail" name="mail" required>
					    </div>
					  </div>

					  <div class="row">
					    <div class="col">
					      <span class="text-danger d-flex justify-content-cente"><?php echo form_error('nombre');?></span>
					    </div>
					    
					    <div class="col">
					      <span class="text-danger d-flex justify-content-cente"><?php echo form_error('mail');?></span>
					    </div>
					  </div>
					  
					  

					 <div class="form-group">
					 	<label class="mt-3" for="formularioConsulta">Elija el motivo de su consulta</label>
					    <select class="custom-select" name="motivo" required>
						      <option selected value="0">Elija un motivo de consulta</option>
						      <?php foreach ($motivos as $row){;?>
						      <option value=<?php echo $row->id;?>><?php echo $row->nombreMotivo;?></option>
						      <?php };?>
					    </select>
				    <div class="invalid-feedback mt-3">Por favor, seleccione un motivo para su consulta</div>
				    <span class="text-danger d-flex justify-content-cente"><?php echo form_error('motivo');?></span>

		  			  <div class="form-group">
						    <label class="mt-3" for="exampleFormControlTextarea1">Escriba detalladamente su consulta</label>
						    <textarea class="form-control" id="exampleFormControlTextarea1" name="consulta" rows="3" required></textarea>
					  </div>
					  <span class="text-danger d-flex justify-content-cente"><?php echo form_error('consulta');?></span>
					  <div>
					  		<button type="submit-form" class="btn btn-primary"  name="ingresarConsulta">
								Enviar Consulta
							</button>

					  </div>
				</form>

				<!--Botón que despliega los datos de la empresa-->
				<a class="btn btn-primary mt-5" data-toggle="collapse" href="#datosEmpresa" role="button" aria-expanded="false" aria-controls="collapseExample">
	    			Ver Datos de la Empresa y Datos De Contacto
	  			</a>
			</div>
			
			<!--Datos desplegados o retraídos al pulsar el botón-->
			<div class="collapse mt-5 border border-dark" id="datosEmpresa">
			      <p class="px-5 mt-5">
			      	Razon Social: "El Calabozo del Androide" es una tienda online de ventas de juegos de mesa, cartas intercambiables, miniaturas, accesorios y demás productos lúdicos.<br><br>
					
					Titular: Enrique Manuel Espinoza.<br><br>

				    Domicilio: Junin 1235 Corrientes, Capital Argentina C.P.3400.<br><br>

					Teléfono de Contacto: 3794-34678934.<br><br>

					Whatsapp: 3794-456789.<br><br>

					E-mail: elcalabozocontacto@gmail.com.<br><br>
				</p>
					    
			</div>
		<script src="<?php echo base_url();?>assets/js/validarNuevaCuenta.js"></script>
</section>