<!--Muestra textos de ayuda para moverse a través de la página-->
<section class="container mt-5" style="font-size: 1rem;">
	<div class="text-center">

		<!--Detalles sobre comercialización-->
		<p class="text-center mt-3" style="font-size: 1rem;">Datos Sobre La Comercialización De La Empresa</p>
			<div class="accordion" id="accordionExample">
			  <div class="card">
			    <div class="card-header" id="headingOne">
			      <h2 class="mb-0">
			        <button class="btn btn-link textoTamaño8" style="font-size: 1rem;" type="button" data-toggle="collapse" data-target="#formasPago" aria-expanded="false">
			          ¿Qué formas de pago se ofrecen?
			        </button>
			      </h2>
	    	</div>

		    <div id="formasPago" class="collapse show textoTamaño8"  style="font-size: 1rem;"aria-labelledby="headingOne" data-parent="#accordionExample">
		      <div class="card-body">
		       		Mediante la compra online el cliente tiene la posibilidad de pagar mediante tarjeta de crédito, paypal o por contrareembolso si el envío se realiza en el interior de la provincia o hacia otras provincias dentro del territorio Argentino.
		       		En caso de ser en la misma provincia, ofrecemos un servicio de envío directo mediante cadete con el
		       		cual se tiene la opción de abonar en efectivo al momento de recibir el paquete.
		       		En caso que realice la compra mediante tarjeta de crédito, paypal o contrareembolso, el paquete será envíado por correo argentino con un tiempo de entrega entre 3 días hábiles hasta 7 días hábiles para todo el país.
		       		Si usted se apersona en nuestra sucursal sita en la calle Junin 1235 Corrientes, Capital Argentina C.P.3400, puede pagar directamente con tarjeta de crédito, débito o en efectivo y llevarse el producto directamente en mano.
		      </div>
		    </div>
		  </div>

		  <div class="card">
		    <div class="card-header" id="headingTwo">
		      <h2 class="mb-0">
		        <button class="btn btn-link collapsed textoTamaño8" style="font-size: 1rem;" type="button" data-toggle="collapse" data-target="#tiposEnvio" aria-expanded="false" aria-controls="busqueda">
		          Una vez comprado el producto ¿cuándo y cómo recibo mi compra?
		        </button>
		      </h2>
		    </div>
		    <div id="tiposEnvio" class="collapse textoTamaño8" style="font-size: 1rem;" aria-labelledby="headingTwo" data-parent="#accordionExample">
		      <div class="card-body">
		        	Una vez realizada la compra mediante nuestro sistema de venta online, el producto será enviado mediante Correo Argentino el cual tarda entre 3 y 7 días hábiles en llegar a su domicilio.
		        	Recibirá un mail con un número de rastreo de su compra para conocer el estado del envío de la misma. En caso de no estar un adulto presente en el momento que el paquete llegue a su domicilio,
		        	el correo suele retener durante 72 horas el paquete antes de devolverlo a remitente. Es recomendable que se comunique con el Correo Argentino para conocer el estado de su pedido o puede comuniarse con nosotros mediante nuestro contacto online 
		        	(<a href="<?php echo base_url('contactoyconsultas');?>"> Consultas y Sugerencias</a>) o mediante nuestro mail: elcalabozodelandroide_contactos@gmail.com para que podamos realizar la consulta sobre el estado de su pedido e informarle mediante mail.
		      </div>
		    </div>
		  </div>

		  <div class="card" style="font-size: 1rem;">
		    <div class="card-header" id="headingThree">
		      <h2 class="mb-0">
		        <button class="btn btn-link collapsed textoTamaño8" type="button" data-toggle="collapse" data-target="#garantias" aria-expanded="false" aria-controls="contacto">
		          	¿Cuáles son las políticas de garantías y reembolsos?
		        </button>
		      </h2>
		    </div>
		    <div id="garantias" class="collapse textoTamaño8" style="font-size: 1rem;" aria-labelledby="headingThree" data-parent="#accordionExample">
		      <div class="card-body" style="font-size: 1rem;">
		        	Nuestros productos constan de garantías por 6 meses para todos nuestros productos. Estas garantías están sujetas a las politicas de cada producto en particular que deberá verse para cada uno de manera individual. Ante el cumplimiento de los requisitos de la garantía, el usuario podrá optar
					por un reembolso o por la adquisión de un nuevo producto. Los reembolsos sólo se realizarán dentro de las 48 horas después de la compra del producto y sólo podrá realizarse en nuestra sucursal sita en la calle Junin Junin 1235 Corrientes, Capital Argentina C.P.3400.
		      </div>
		    </div>
		  </div>

		    <div class="card">
		    <div class="card-header" id="headingThree">
		      <h2 class="mb-0">
		        <button class="btn btn-link collapsed textoTamaño8" type="button" data-toggle="collapse" data-target="#quejas" aria-expanded="false" aria-controls="contacto">
		          ¿Cómo puedo realizar una queja o descargo sobre un producto?
		        </button>
		      </h2>
		    </div>
		    <div id="quejas" class="collapse textoTamaño8" aria-labelledby="headingThree" data-parent="#accordionExample">
		      <div class="card-body">
		        	Para cualquier tipo de consulta o queja que desee realizar sobre nuestros productos tiene los siguientes medios de contacto: <br>
		        	<span class="font-weight-bold">Teléfono de Contacto: 3794-34678934</span>.<br>

					<span class="font-weight-bold">Whatsapp: 3794-456789</span>.<br>

					<span class="font-weight-bold">Email: elcalabozocontacto@gmail.com</span><br>

					Le solicitamos que, si se comunica mediante whatsapp deje en claro sobre qué producto quiere realizar la consulta o queja, acompañado de un mail de contacto para darle una respuesta a la brevedad.<br>
					En caso de que desee realizar la queja o consulta vía online puede hacerlo en el siguiente enlace:
					<a href="<?php echo base_url('contactoyconsultas');?>"> Consultas y Sugerencias </a>.
					Solemos responder a las consultas 72 horas después de visto el mensaje, así que le solicitamos paciencia ante la respuesta a su consulta.<br>
					Cualquier consulta o queja que desee hacernos será bienvenida.

		      </div>
		    </div>
		  </div>
		</div>

		<!--Ayuda en general-->
		<p class="text-center mt-5">Lista De OpcionesPara Navegar por la Página</p>
			<div class="accordion mt-5" id="accordionExample">
			  <div class="card">
			    <div class="card-header" id="headingOne">
			      <h2 class="mb-0">
			        <button class="btn btn-link textoTamaño8" type="button" data-toggle="collapse" data-target="#JuegosMesa" aria-expanded="true" aria-controls="JuegosMesa">
			          ¿Quiero ir ver los juegos disponibles?
			        </button>
			      </h2>
	    	</div>

		    <div id="JuegosMesa" class="collapse textoTamaño8" aria-labelledby="headingOne" data-parent="#accordionExample">
		      <div class="card-body">
		        En la barra de navegación se tiene cada uno de los siguientes productos:<br><br>
		        <span class="font-weight-bold textoTamaño8">* Juegos de Mesa</span>: Son juegos de mesa de diversas categoría. Al hacer click en "Juegos de Mesa" verá desplegado cada uno de los diversos tipos de juegos de mesa divididos en "Estrategia", "Abstractos", "Wargames".
		        Seleccione uno de los tipos para ver los productos disponibles.<br><br>

		        <span class="font-weight-bold textoTamaño8">*Cartas</span>: Son juegos de cartas coleccionables. Se tienen productos de "Yu-gi-oh!", "Magic: The Gathering", "Pokemon". La categoría "Otros" muestra juegos de cartas más clásicos como "Truco",
		        "Poker", "Uno". Elige una de las categorías para ver los productos disponibles.<br><br>

		        <span class="font-weight-bold textoTamaño8">*Miniaturas</span>: Son figuras de diversos personajes de comics, animes, videojuegos, etc. Varían en tamaño y formas. Has click en dicha opción para ver los productos disponibles de este tipo.<br><br>

		        <span class="font-weight-bold textoTamaño8">*Accesorios</span>: Llaveros, mochilas, portavasos y demás artículos relacionados con diversos temas: "El Señor de los Anillos", "Pokemon", "Magic", etc. Miralos y talvez encuentres algo que te guste.<br><br>
		      </div>
		    </div>
		  </div>

		  <div class="card">
		    <div class="card-header" id="headingTwo">
		      <h2 class="mb-0">
		        <button class="btn btn-link collapsed textoTamaño8" type="button" data-toggle="collapse" data-target="#busqueda" aria-expanded="false" aria-controls="busqueda">
		          ¿Cómo hago para realizar una búsqueda?
		        </button>
		      </h2>
		    </div>
		    <div id="busqueda" class="collapse textoTamaño8" aria-labelledby="headingTwo" data-parent="#accordionExample">
		      <div class="card-body">
		        Para realizar una búsqueda sólo escribe el nombre de un producto en la barra que tiene el nombre "Buscar" en la barra de opciones a la derecha. Si no conoce el nombre del producto, pero tiene una idea de la palabra que pueda contener sólo coloque una letra o palabra para realizar una búsqueda de todos los productos que concuerden con el criterio. Una vez introducida una palabra o frase, presione "enter" para realizar la búsqueda, se mostrarán todos los resultados que concuerden con el criterio señalado.
		        Si conoce la categoría (carta, juego de mesa, etc.) elija la categoría de la barra de opciones y luego ordenelas usando el botón "Ordenar Por" y ordene de A-Z, luego recorra los productos hasta encontrar el que desee. En caso de no encontrar el producto que busca, contacte con nosotros mediante el siguiente enlace <a href="<?php echo base_url('contactoyconsultas');?>"> Consultas y Sugerencias </a>.
		      </div>
		    </div>
		  </div>

		  <div class="card">
		    <div class="card-header" id="headingThree">
		      <h2 class="mb-0">
		        <button class="btn btn-link collapsed textoTamaño8" type="button" data-toggle="collapse" data-target="#contacto" aria-expanded="false" aria-controls="contacto">
		          Contactarse con Nosotros.
		        </button>
		      </h2>
		    </div>
		    <div id="contacto" class="collapse textoTamaño8" aria-labelledby="headingThree" data-parent="#accordionExample">
		      <div class="card-body">
		        Para contactarse con nosotros puede hacerlo en el siguiente enlace 
		        <a href="<?php echo base_url('contactoyconsultas');?>"> 
		        	Consultas y Sugerencias
		        </a>. 
		        También puede hacer click en la opción "Contacto" de la barra de opciones superior. Llene el formulario con los datos que le solicitan. Cualquier sugerencia, productos faltantes, u otra cosa que considere pertienente será bienvenida.
		      </div>
		    </div>
		  </div>

		    <div class="card">
		    <div class="card-header" id="headingThree">
		      <h2 class="mb-0">
		        <button class="btn btn-link collapsed textoTamaño8" type="button" data-toggle="collapse" data-target="#cuentas" aria-expanded="false" aria-controls="contacto">
		          Hacer Loggin y Crear una nueva cuenta
		        </button>
		      </h2>
		    </div>
		    <div id="cuentas" class="collapse textoTamaño8" aria-labelledby="headingThree" data-parent="#accordionExample">
		      <div class="card-body">
		        Si ya tiene una cuenta haga click en la opción "Loggin" ubicada en la barra de navegación superior. Complete los datos solicitados para iniciar sesión. 
		        En caso de no tener aún una cuenta, por favor haga click en la opción "Registrarse" de la barra de la navegación superior. Complete los datos solicitados para crear una nueva cuenta. Una vez finalizada la
		        registración, haga click en Loggin, complete los datos solicitados para iniciar sesión.
		        Si ya tiene cuenta, también puede hacer click en el siguiente enlace:
		        <a href="<?php echo base_url('loggin');?>">
		        	Loggin
		        </a>.
		        <br>En caso de tener una cuenta ya creada, puede hacer click en el siguiente enlace: 
		        <a href="<?php echo base_url('registrarse');?>">
		        	Iniciar sesión
		        </a>.
		      </div>
		    </div>
		  </div>
		</div>
	</div>
</section>