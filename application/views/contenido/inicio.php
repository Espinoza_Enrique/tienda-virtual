
 <section>
  		
		<!--carrusel-->
		<div class="bd-example bd-example mt-5 mb-5 d-none d-none d-md-none d-lg-none d-xl-block">
		  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
		    <ol class="carousel-indicators">
		      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
		      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
		      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
		    </ol>
		    <div class="carousel-inner">
		      <div class="carousel-item active">
		        <img src="<?php echo base_url();?>assets/img/Carusel/D&D Dragones.jpg" class="d-block w-100" alt="...">
		        <div class="carousel-caption d-none d-md-block">
		         	 <h5 class="bg-secondary">D&D Quinta Edición</h5>
				      <p class="text-white bg-secondary">
				      	Prueba la nueva edición de Dungeons and Dragons y conviertete en legenda.
				     </p>
		        </div>
		      </div>
		      <div class="carousel-item">
		        <img src="<?php echo base_url();?>assets/img/Carusel/vrains2.jpg" class="d-block w-100" alt="...">
		        <div class="carousel-caption d-none d-md-block">
		           <h5 class="text-white bg-primary">¡Nuevas Estrategias! Una forma actualizada de ver el juego</h5>
				   <p class="text-white bg-primary">
				   		Prueba los paquetes de expansión VRains de Yu-gi-oh y demuestra tus cualidades de duelo.
				   </p>
		        </div>
		      </div>
		      <div class="carousel-item">
		        <img src="<?php echo base_url();?>assets/img/Carusel/el trono de hierro.jpg" class="d-block w-100" alt="...">
		        <div class="carousel-caption d-none d-md-block">
		          	<h5 class="bg-dark">El Invierno se Aproxima para los jugadores</h5>
				     <p class="bg-dark">
				     	Prueba Catan: Juego de Tronos y demuestra tus habilidades estratégicas para sobrevivir este desafio.
				   </p>
		        </div>
		      </div>
		    </div>
		    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
		      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		      <span class="sr-only">Previous</span>
		    </a>
		    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
		      <span class="carousel-control-next-icon" aria-hidden="true"></span>
		      <span class="sr-only">Next</span>
		    </a>
		  </div>
		</div>

		<!--secciones de muestra para los productos-->
  		<article class="container mt-5">
  			<div class="row mt-5  text-center"> 
  				<h1 class="col text-center mt-5">Productos Destacados </h1>
  			</div>

			<nav class="row mx-1">
				<?php 
					foreach ($destacados as $filas) {
						
						$sub=$filas->nombreSub;
						$precio=$filas->precio;
						$rutaImagen=base_url(). "uploads/imagenes/".$filas->nombreFoto;
					?>

						<div class="card col-lg-3 col-sm-6 mt-5 bg-dark text-white" style="width: 18rem;">
						  <img src=<?php echo $rutaImagen;?> class="card-img-top mt-3 w-50" alt="...">
						  <div class="card-body">
						    <a class="card-title tamañoTexto8 bg-dark text-white" style="font-size: 1rem;"><?php echo $filas->nombre_producto;?></a>
						    <p class="card-text tamañoTexto8" style="font-size: 1rem;">
						    	<?php echo "Precio: " . $filas->precio. "\n"; echo "Garantía: ".$filas->garantia." días";?>
						    </p>
						    <div class="btn-group" role="group">
								<a class="btn bg-primary text-white" name="detalles"  href=<?php echo base_url('mostrarDescripcion')."/".$filas->id_producto?>>
											<!--<img src="<?php echo base_url();?>assets/img/Logotipos/editar.png" data-toggle="tooltip" data-placement="right" title="Detalles"  class="w-100 iconosNavegacion" />-->
											Descripción
									 </a>
									 <!--muestro el botón si hay una sesión activa-->
									 <?php if($this->session->userdata('perfil')==2) {?>
									<form  action="<?=base_url('agregarCarrito')?>" method="post" novalidate>
										  <button type="submit-form" name="id" class="btn btn-primary ml-3 border" value=<?php echo $filas->id_producto;?>>  
										  	<a>
										  		<img src="<?php echo base_url();?>assets/img/Logotipos/carrito.png"/">
										  	</a>
										  </button>
								    </form>

								    <?php }; ?>
	
							</div>
							
							<hr>
						  </div>
						</div>
				<?php };?>
			</nav>

		</article>
		
<article class="container mt-5">
  			<div class="row mt-5  text-center"> 
  				<h1 class="col text-center mt-5">Productos Nuevos </h1>
  			</div>
  		<!--Otra sección de productos, en este caso, los productos nuevos-->
		<nav class="row mx-1">
				<?php 
					foreach ($nuevos as $filas) {
						
						$sub=$filas->nombreSub;
						$precio=$filas->precio;
						$rutaImagen=base_url(). "uploads/imagenes/".$filas->nombreFoto;
						?>
						<div class="card col-lg-3 col-sm-6 mt-5 bg-dark text-white" style="width: 18rem;">
						  <img src=<?php echo $rutaImagen;?> class="card-img-top mt-3 w-50"  alt="...">
						  <div class="card-body">
						    <a class="card-title tamañoTexto8 bg-dark text-white" style="font-size: 1rem;"><?php echo $filas->nombre_producto;?></a>
						    <p class="card-text tamañoTexto8" style="font-size: 1rem;">
						    	<?php echo "Precio: " . $filas->precio. "\n"; echo "Garantía: ".$filas->garantia." días";?>
						    </p>
						    <div class="btn-group" role="group">
								 <a class="btn bg-primary text-white" name="detalles"  href=<?php echo base_url('mostrarDescripcion')."/".$filas->id_producto?>>
											<!--<img src="<?php echo base_url();?>assets/img/Logotipos/editar.png" data-toggle="tooltip" data-placement="right" title="Detalles"  class="w-100 iconosNavegacion" />-->
											Descripción
									 </a>
								
								
									 <!--muestro el botón si hay una sesión activa-->
									 <?php if($this->session->userdata('perfil')==2) {?>
									<form  action="<?=base_url('agregarCarrito')?>" method="post" novalidate>
										  <button type="submit-form" name="id" class="btn btn-primary ml-3 border" value=<?php echo $filas->id_producto;?>>  
										  	<a>
										  		<img src="<?php echo base_url();?>assets/img/Logotipos/carrito.png"/">
										  	</a>
										  </button>
								    </form>

								    <?php }; ?>
	
							</div>
							
							<hr>
						  </div>
						</div>

				<?php };?>

			</nav>

		</article>
</section>
<!--Script incompleto-->
<script>
	function mostrarDetalles(id) {
    var result=confirm('Descripcion Producto');
    window.location=<?php echo base_url('descripcion')."/"?>.id;
    if(result) {
        return false;
    }
}
</script>
