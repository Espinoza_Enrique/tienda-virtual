<section class="container mt-5 border">
	<h3 class="d-flex justify-content-center mt-2 mx-auto col-md-10 col-lg-5"><span class="badge badge-info"> Ingrese su email y contraseña por favor</h3>
	<form action="<?=base_url("logginUsuario")?>" method="post" novalidate>
		  <div class="form-group d-flex justify-content-center mt-5 mx-auto col-md-10 col-lg-5" >
			    <label class="mx-3" for="exampleInputEmail1">Email</label>
			    <input type="email" class="form-control form-input mx-5" id="mail" name="mail" aria-describedby="emailHelp" placeholder="Mail" value="<?php echo set_value('mail');?>" required>
		  </div>
		  <div class="text-center">
		  <small id="emailHelp" class="form-text text-muted">Nunca comparta su dirección de mail.</small>
		 </div>
		 <div class="text-center">
		  <span class="text-danger"><?php echo form_error('mail');?></span>
		 </div>
		  <div class="form-group  d-flex justify-content-center mt-5 mx-auto col-md-10 col-lg-5">
			    <label class="mx-3" for="exampleInputPassword1">Contraseña</label>
			    <input type="password" class="form-control form-input" name="password" id="password" placeholder="Contraseña" value="<?php echo set_value('password');?>" required>
		  </div>
		  <div class="text-center">
		  <span class="text-danger"><?php echo form_error('password');?></span>
		 </div>
		  <div class="d-flex justify-content-center mt-3">
		  <button type="form-submit" class="btn btn-primary mb-3">Loggin</button>
		</div>
	</form>
	<!--<script src="<?php echo base_url();?>assets/js/validarNuevaCuenta.js"></script>-->
</section>