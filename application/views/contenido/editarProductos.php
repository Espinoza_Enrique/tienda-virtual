<!--formulario para la carga de productos-->
<section class="align-items-center d-md-mt-3 mb-5 d-sm-mt-3 ">

	<form class="mt-5 mx-5 align-items-center border bg-info" enctype="multipart/form-data" action="<?=base_url('aplicarEdicion')?>" method="post" novalidate>
	  <div class="d-flex justify-content-center mt-5 mx-auto col-md-10 col-lg-5 ">
	  	<h3><span class="badge badge-info">Editar Producto</span></h3>
	  </div>
	  <div class="form-group mx-auto col-auto col-md-10 col-lg-5  mt-3 text-center text-white">
	  	<?php echo form_label('Nombre','nombre');?>
	  	<?php echo form_input(['name'=>'nombre','id'=>'nombre','type'=>'text','class'=>'form-control','value'=>$producto->nombre_producto]);?>
	    <!--<label for="exampleInputEmail1">Nombre</label>
	    <input type="text" value=<?php  echo str_replace(" ","_",$producto->nombre_producto)//cambio los espacios por guón bajo ya que sólo muestra los datos hasta el primer espacio en blanco;?> class="form-control form-input text-center" id="nombre" name="nombre" aria-describedby="emailHelp" placeholder="Ingrese un nombre">-->
	  </div>
	  <div class="d-flex justify-content-center text-center">
	  <div class="container bg-danger w-50 text-center"><span class="text-white text-center d-flex justify-content-cente"><?php echo form_error('nombre');?></span></div>
	</div>
	  <div class="form-group mx-auto col-auto col-md-10 col-lg-5  mt-3 text-center text-white">
	    <label for="exampleInputPassword1">Precio</label>
	    <input type="number" value=<?php echo $producto->precio?> class="form-control form-input text-center" id="precio" name="precio" placeholder="Ingrese un precio">
	  </div>
	  
	  <div class="container bg-danger w-50">
	  	<span class="text-white d-flex justify-content-center"><?php echo form_error('precio');?></span>
	  	<span class="text-white d-flex justify-content-center"><?php echo form_error('controlarNegativosYCero');?></span>
	  </div>
	
	  <?php
	  		/*codigo que permite cargar las categorías en un dropbox*/

	  		//creo un array asociativo que tendrá el valor 0 para indicar que no se ha seleccionado
			$lista['0']='Seleccione Categoría';
			
			/*creo la lista teniendo en cuenta la categoría y subcategoría de los productos*/
			foreach ($categorias as $filaCat) {
				foreach ($subcategorias as $filaSub) {
					if($filaSub->IdCategoria==$filaCat->id) {
						$lista[$filaSub->Id]=$filaCat->nombreCat . "-" . $filaSub->nombreSub;
					};
				};
			};
			
			//luego creo el dropbox
			 
	  	 echo
	  	 "<div class='form-group mx-auto col-auto col-md-10 col-lg-5  mt-3 text-white'>
		  	 <label for='exampleInputEmail1' >Categoria</label>
			  <select class='custom-select form-input text-center' name='categoria'>";
			  		//aquí asigno a cada elemento del dropbox cuyo valor es el índice de la subcategoria y 
			  		//el nombre es la concatenación entre el nombre de la categoría y de la subcategoría
				  	foreach ($lista as $indice=>$categoria) {
				  		if ($indice==$producto->subcategoria) {
				  			echo "<option selected value=". $indice.">".$categoria."</option>"; 
				  		} else {
				  			echo "<option value=". $indice. ">" . $categoria. "</option>";
				  		};
				  	};
		     echo "</select>" . "</div>"
		?>
	  <div class="container container bg-danger w-50"><span class="text-white  d-flex justify-content-center"><?php echo form_error('categoria');?></span></div>
  	  <div class="form-group mx-auto col-auto col-md-10 col-lg-5  mt-3 text-center text-white">
	    <label for="exampleInputPassword1">Stock Minimo</label>
	    <input type="number" value=<?php echo $producto->stockMinimo?> class="form-control form-input text-center" id="stockMinimo" name="stockMinimo" placeholder="Ingrese el stock mínimo">
	  </div>
	  <div class="container bg-danger w-50">
	  	<span class="text-white d-flex justify-content-center"><?php echo form_error('stockMinimo');?></span>
	  	<span class="text-white d-flex justify-content-center"><?php echo form_error('controlarNegativosYCero');?></span>
	  </div>
	  <div class="form-group mx-auto col-auto col-md-10 col-lg-5  mt-3 text-center text-white">
	    <label for="exampleInputPassword1">Stock Actual</label>
	    <input type="number" value=<?php echo $producto->stock?> class="form-control form-input text-center" id="stock" name="stock" placeholder="Ingrese el stock actual">
	  </div>
	  <div class="container bg-danger w-50">
	  	<span class="text-white d-flex justify-content-center"><?php echo form_error('stock');?></span>
	  	<span class="text-white d-flex justify-content-center"><?php echo form_error('controlarNegativosYCero');?></span>
	  </div>
	   <div class="form-group mx-auto col-auto col-md-10 col-lg-5  mt-3 text-center text-white">
	    <label for="exampleInputPassword1">Tiempo de Garantía(en días)</label>
	    <input type="number" value=<?php echo $producto->garantia?> class="form-control form-input text-center" id="garantia" name="garantia" placeholder="Ingrese los días para la garantía">
	  </div>
	  <div class="container bg-danger w-50">
		  <span class="text-white d-flex justify-content-center"><?php echo form_error('controlarNegativosYCero');?></span>
		  <span class="text-white d-flex justify-content-center"><?php echo form_error('garantia');?></span>
	  </div>
	 
	 <div class="form-group mx-auto col-auto col-md-10 col-lg-5  mt-3 text-center text-white">
	    <label for="exampleFormControlFile1">Seleccione la imagen del producto</label>
	    <input type="file" class="form-control-file form-input text-center" id="imagen" name="imagen">
	  </div >
	  <div class="container container bg-danger w-50"><span class="text-white d-flex justify-content-center"><?php echo form_error('imagen');?></span></div>

	  <div class="mx-auto  mx-auto w-75 text-center mt-3 text-white">	
	    <label for="validationTextarea">Descripción</label>
	    <textarea class="form-control form-input mx-auto col-auto" id="descripcion" name="descripcion" placeholder="Coloque la descripción del producto" required> <?php echo $producto->descripcion?> </textarea>
  	  </div>
  	  <div class="container bg-danger w-50 mt-3"><span class="text-white d-flex justify-content-center"><?php echo form_error('descripcion');?></span></div>
  	  <div class="d-flex justify-content-center">
	  	<button type="form-submit" class="btn mt-5 btn-primary mb-3" value=<?php echo $producto->id_producto?> name="id" btn-primary" data-toggle="modal" data-target="#confirmarIngreso">Modificar Producto</button>
	  </div>
	</form>

	
</section>