<section class="container mx-5 text-center">
        <div class="form-group mx-auto col-auto mt-5 w-50">
            <a class="textoTamaño7 text-dark font-weight-bold" style="font-size: 1rem;">Detalles de Compras</a>
        </div>
        

        <table id="tablaProductos" class="table table-borded table-striped table- hover table-responsive mx-5">
                <!--encabezados de la tabla-->
                <thead>
                         <th class="textoTamaño7 text-center">Comprador</th>
                        <th class="textoTamaño7 text-center">Producto</th>
                        <th class="textoTamaño7 text-center">Cantidad</th>
                        <th class="textoTamaño7 text-center">Precio</th>
                        <th class="textoTamaño7 text-center">Categoria</th>
                        <th class="textoTamaño7 text-center">Subcategoria</th>
                        <th class="textoTamaño7 text-center">Fecha venta</th>
                </thead>
                <tbody>
                        <?php 
                        $cantidad=0;
                        $recaudacion=0;
                        foreach ($compras as $detalles) { 
                                $cantidad=$cantidad+$detalles->cantidad;
                                $recaudacion=$recaudacion+($detalles->cantidad*$detalles->precio);
                                ?>
                <tr>
                        <td class="textoTamaño7 text-center"> <?php echo $detalles->nombre ." ".$detalles->apellido; ?> </td>
                        <td class="textoTamaño7 text-center"> <?php echo $detalles->nombre_producto; ?> </td>
                        <td class="textoTamaño7 text-center"> <?php echo $detalles->cantidad; ?> </td>
                        <td class="textoTamaño7 text-center"> <?php echo $detalles->precio; ?> </td>
                        <td class="textoTamaño7 text-center"> <?php echo $detalles->nombreCat; ?> </td>
                         <td class="textoTamaño7 text-center"> <?php echo $detalles->nombreSub; ?> </td>
                         <td class="textoTamaño7 text-center"> <?php echo $detalles->fecha; ?> </td>
                       
                       <?php };?>
                    
                </tr>
                    <br>
                  <tr>
                        <td colspan="2"> </td>
                        <td class="left"><strong>Unidades Totales vendidas</strong></td>
                        <td class="left"><?php echo $cantidad; ?>
                    </tr>

                    <tr>
                        <td colspan="2"> </td>
                        <td class="left"><strong>Recaudacion Total</strong></td>
                        <td class="left">$<?php echo $recaudacion; ?>
                    </tr>
                </tbody>
        

    </table>
</section>