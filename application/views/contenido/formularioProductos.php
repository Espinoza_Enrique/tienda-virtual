<!--formulario para la carga de productos-->
<section class="align-items-center d-md-mt-3 mb-5 d-sm-mt-3">
	<form class="mt-5 mx-5 align-items-center border" enctype="multipart/form-data" action="<?=base_url('registrarProductos')?>" method="post" novalidate>
	  <div class="d-flex justify-content-center mt-5 mx-auto col-md-10 col-lg-5 ">
	  	<h3><span class="badge badge-info">Cargar Productos</span></h3>
	  </div>
	  <div class="form-group mx-auto col-auto col-md-10 col-lg-5  mt-3 text-center">
	  	
	    <label class="textoTamaño8" for="exampleInputEmail1">Nombre</label>
	    <input type="text" class="form-control form-input input-sm" id="nombre" name="nombre" aria-describedby="emailHelp" 
	    placeholder="Ingrese un nombre">
	  </div>
	  <div class="d-flex justify-content-center text-center">
	  <span class="text-danger d-flex justify-content-center textoTamaño8"><?php echo form_error('nombre');?></span>
	</div>
	  <div class="form-group col-auto mx-auto col-md-10 col-lg-5  mt-3 text-center">
	    <label  class="textoTamaño8" for="exampleInputPassword1">Precio</label>
	    <input type="number" class="form-control form-input" id="precio" name="precio" placeholder="Ingrese un precio">
	    
	  </div>
	  <span class="text-danger d-flex justify-content-center"><?php echo form_error('controlarNegativosYCero');?></span>
	  <span class="text-danger d-flex justify-content-center  class="textoTamaño8""><?php echo form_error('precio');?></span>
	
	  <?php
	  		/*codigo que permite cargar las categorías en un dropbox*/

	  		//creo un array asociativo que tendrá el valor 0 para indicar que no se ha seleccionado
			$lista['0']='Seleccione Categoría';
			
			/*creo la lista teniendo en cuenta la categoría y subcategoría de los productos*/
			foreach ($categorias as $filaCat) {
				foreach ($subcategorias as $filaSub) {
					if($filaSub->IdCategoria==$filaCat->id) {
						$lista[$filaSub->Id]=$filaCat->nombreCat . "-" . $filaSub->nombreSub;
					};
				};
			};
			
			//luego creo el dropbox
			 
	  	 echo
	  	 "<div class='col-auto w-100 mx-auto mt-3 col-md-10 col-lg-5  text-center'>
		  	 <label for='exampleInputEmail1' >Categoria</label>
			  <select class='custom-select form-input textoTamaño8' name='categoria'>";
			  		//aquí asigno a cada elemento del dropbox cuyo valor es el índice de la subcategoria y 
			  		//el nombre es la concatenación entre el nombre de la categoría y de la subcategoría
				  	foreach ($lista as $indice=>$categoria) {
				  		echo "<option value=". $indice. ">" . $categoria. "</option>";
				  	};
		     echo "</select>" . "</div>"
		?>
	  <span class="text-danger d-flex justify-content-center"><?php echo form_error('categoria');?></span>
  	  <div class="form-group col-auto w-100 mt-3 mx-auto col-md-10 col-lg-5  text-center">
	    <label  class="textoTamaño8" for="exampleInputPassword1">Stock Minimo</label>
	    <input type="number" class="form-control form-input adaptarTamaño" id="stockMinimo" name="stockMinimo" placeholder="Ingrese el stock mínimo">
	  </div>
	  <span class="text-danger d-flex justify-content-center"><?php echo form_error('stockMinimo');?></span>
	  <span class="text-danger d-flex justify-content-center"><?php echo form_error('controlarNegativosYCero');?></span>
	  <div class="form-group col-auto w-100 mt-3 mx-auto col-md-10 col-lg-5  text-center">
	    <label for="exampleInputPassword1">Stock Actual</label>
	    <input type="number" class="form-control form-input" id="stock" name="stock" placeholder="Ingrese el stock actual">
	  </div>
	  <span class="text-danger d-flex justify-content-center"><?php echo form_error('stock');?></span>
	   <span class="text-danger d-flex justify-content-center"><?php echo form_error('controlarNegativosYCero');?></span>
	   <div class="form-group col-auto mt-3 w-100 mx-auto col-md-10 col-lg-5  text-center">
	    <label for="exampleInputPassword1">Tiempo de Garantía(en días)</label>
	    <input type="number" class="form-control form-input" id="garantia" name="garantia" placeholder="Ingrese los días para la garantía">
	  </div>
	  <span class="text-danger d-flex justify-content-center"><?php echo form_error('garantia');?></span>
	  <span class="text-danger d-flex justify-content-center"><?php echo form_error('controlarNegativosYCero');?></span>
	  <div class="form-group col-auto w-100 mt-3 mx-auto col-md-10 col-lg-5  text-center">
	    <label for="exampleFormControlFile1">Seleccione la imagen del producto</label>
	    <input type="file" class="form-control-file form-input w-20" id="imagen" name="imagen">
	  </div >
	  <span class="text-danger d-flex justify-content-center"><?php echo form_error('imagen');?></span>

	  <div class="w-100 text-center mt-3">	
	    <label for="validationTextarea">Descripción</label>
	    <textarea class="form-control form-input" id="descripcion" name="descripcion" placeholder="Coloque la descripción del producto" required></textarea>
  	  </div>
  	  <span class="text-danger d-flex justify-content-center"><?php echo form_error('descripcion');?></span>
  	  <div class="d-flex justify-content-center text-center">
	  	<button type="form-submit" class="btn mt-5 mb-2 btn-primary" data-toggle="modal" data-target="#confirmarIngreso">Cargar Producto</button>
	  </div>
	</form>

	
</section>