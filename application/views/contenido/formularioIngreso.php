<!--formulario para crear una cuenta nueva-->
<section class="container">
      <h3 class="text-center pt-5"> <span class="badge badge-info">Complete El Formulario</span></h3>
    	   <form class="needs-validation mt-5 text-center" action="<?=base_url("registrarCliente")?>" method="post" novalidate>
          <label class="mt-3">Datos del Usuario</label>
            <div class="form-row border bg-primary">
              <!--línea para la validación de errores-->
              <!--<?php echo validation_errors(); ?>--> 
              <div class="col-md-4 mb-3">
                <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5 text-white" for="validationTooltip01">Nombre</label>
                <input type="text" class="form-control form-input" id="nombre" placeholder="Nombre" value="<?php echo set_value('nombre');?>" name="nombre" required>
                <span class="text-white"><?php echo form_error('nombre');?></span>
            </div>
            <div class="col-md-4 mb-3">
               <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5 text-white" for="validationTooltip02">Apellido</label>
               <input type="text" class="form-control form-input" id="apellido" placeholder="Apellido"  value="<?php echo set_value('apellido');?>" name="apellido" required>
               <span class="text-white"><?php echo form_error('apellido')?></span>
              
            </div>

             <div class="col-md-4 mb-3">
               <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5 text-white" for="validationTooltip02">Dni</label>
               <input type="number" class="form-control form-input" id="dni" placeholder="Dni"  value="<?php echo set_value('dni');?>" name="dni" required>
               <span class="text-white"><?php echo form_error('dni')?></span>
              
            </div>
          </div>

          <label class="mt-3 text-center">Datos de Contacto</label>
          <div class="row border bg-primary">
             <div class="col-md-4 mb-3">
               <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5 text-white" for="validationTooltip02">Teléfono</label>
               <input type="number" class="form-control form-input" id="telefono" placeholder="Telefono"  value="<?php echo set_value('telefono');?>" name="telefono" required>
               <span class="text-white"><?php echo form_error('telefono')?></span>
              
            </div>

            <div class="col-md-4 mb-3 bg_primary">
               <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5 text-white" for="validationTooltipUsername">Mail</label>
               <div class="input-group">
              <input type="text" class="form-control form-input" id="mail" placeholder="Mail" value="<?php echo set_value('mail');?>" name="mail" aria-describedby="validationTooltipUsernamePrepend" required>
            </div>
            <span class="text-white"><?php echo form_error('mail');?></span>
          </div>
        </div>

        <label class="mt-3">Datos del Domicilio</label>
          <div class="form-row border bg-primary">
          <div class="col-sm-6 mb-3">
            <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5 text-white" for="validationTooltip03">Calle</label>
            <input type="text" class="form-control form-input" name="domicilio" id="domiclio" placeholder="Calle de su domicilio" required>
            <span class="text-white"><?php echo form_error('domicilio');?></span>
          </div>
          

          <div class="col-sm-6 mb-3">
            <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5 text-white" for="validationTooltip04">Altura</label>
            <input type="number" class="form-control form-input" id="altura" name="altura" placeholder="Altura de su calle" required>
            <span class="text-white"><?php echo form_error('altura');?></span>
          </div>
          
          
        </div>

        <label class="mt-3">Ingreso de Contraseña</label>
        <div class="form-row border bg-primary">
          <div class="col-sm-6 mb-3">
            <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5 text-white" for="validationTooltip03">Contraseña</label>
            <input type="password" class="form-control form-input" name="password" id="password" placeholder="Contraseña" required>
            <span class="text-white"><?php echo form_error('password');?></span>
          </div>
          

          <div class="col-sm-6 mb-3">
            <label class="d-flex justify-content-center mx-auto col-md-10 col-lg-5 text-white" for="validationTooltip04">Repita la contraseña</label>
            <input type="password" class="form-control form-input" id="passconf" name="passconf" placeholder="Contraseña" required>
            <span class="text-white"><?php echo form_error('passconf');?></span>
          </div>
          
          
        </div>
              <span class="text-danger"><?php echo form_error('aceptar');?></span>
              <a href="<?php echo base_url('terminos_de_uso_y_politicas');?>">Leer las Condiciones de Uso</a>
        			<div class="form-group form-check">
          			<input type="checkbox" class="form-check-input" name="aceptar" id="exampleCheck1" required>
          			<label class="form-check-label" for="exampleCheck1">Acepto las condiciones de uso</label>
        			</div>

        			<button type="form-submit" class="btn-primary btn-success">Registrarse</button>
               <!--<?php echo form_close();?>--> 
      		  </form>
  <!--Script que controla el formulario. Fue guardado en un archivo js para que sea más legible-->
    <!--<script src="<?php echo base_url();?>assets/js/validarNuevaCuenta.js"></script>-->
</section>