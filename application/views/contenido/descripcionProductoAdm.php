<section>
	<div class="jumbotron mx5 mx-3 mt-3">
		  <p class="display-4 text-center font-weight-bold" style="font-size: 1.0rem;"> Descripción del Producto </3>
		  <br>
		  <p class="display-4 text-dark font-weight-bold" style="font-size: 1rem;"><?php echo  $producto->nombre_producto;?></p>
		  <p class="lead text-dark font-weight-bold font-italic" style="font-size: 0.9rem;"><?php echo $producto->descripcion;?></p>
		  <hr class="my-4">
		   <hr class="my-4">
  		  <p style="font-size: 1rem;">Pulse el botón "Atrás" de su navegador para regresar a la pantalla anterior o pulse el botón para ver todos los productos.</p>
		
		
				<a class="btn btn-primary btn-lg textoTamaño7 " style="font-size: 1rem;" href=<?php echo base_url('mostrarProductos');?>>Volver a la lista de productos</a>

	</div>
</section>