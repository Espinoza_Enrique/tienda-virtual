<div class="modal"  id="confirmarIngreso" tabindex="-1" role="dialog">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title">Confirmación de Ingreso</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		      <?php echo $error;?>
		        
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary"  data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
</div>
<!--script para que muestre la ventana modal. Se coloca aquí porque sólo se usará para esta ventana a priori-->
<script>$('#confirmarIngreso').modal('show')</script>