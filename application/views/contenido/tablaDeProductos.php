<!--muesta la tabla con los productos-->
<section class="container mx-5">
	<!--tabla que muestra las opciones de filtrado para productos-->
	<div class="form-group mx-auto text-center col-auto mt-5 w-50">
	    <a class="textoTamaño7 font-weight-bold" style="font-size: 1rem;">Lista Productos</a>
	  </div>
	<table id="tablaProductos" class="table table-borded table-striped table-hover table-responsive">
		<!--encabezados de la tabla-->
		<thead>
			<th class="textoTamaño7">Nombre</th>
			<th class="textoTamaño7">Precio</th>
			<th class="textoTamaño7">Categoria</th>
			<th class="textoTamaño7">Subcategoria</th>
			<th class="textoTamaño7">Stock Actual</th>
			<th class="textoTamaño7">Stock Minimo</th>
			<th class="textoTamaño7">Garantia</th>
			<th class="textoTamaño7">Fecha Ingreso</th>
			<th class="textoTamaño7">Foto</th> 
			<th class="textoTamaño7">Editar</th>
			<th class="textoTamaño7">Alta/Baja</th>
			<th class="textoTamaño7">Descripcion</th>
		</thead>

	
		<tbody>
			<?php foreach ($productos as $row) {
				
				$rutaImagen=base_url(). "uploads/imagenes/".$row->nombreFoto;?>
			<tr>
				<td class="textoTamaño7"> <?php echo $row->nombre_producto; ?> </td>
				<td class="textoTamaño7"> <?php echo $row->precio; ?> </td>
				<td class="textoTamaño7"> <?php echo $row->nombreCat; ?> </td>
				<td class="textoTamaño7"> <?php echo $row->nombreSub; ?> </td>
				<td class="textoTamaño7"> <?php echo $row->stock; ?> </td>
				<td class="textoTamaño7"> <?php echo $row->stockMinimo;?> </td>
				<td class="textoTamaño7"> <?php echo $row->garantia;?> </td>
				<td class="textoTamaño7"> <?php echo $row->fecha;?> </td>
				<td class="textoTamaño7"> <img class="w-100" src= <?php echo $rutaImagen; ?> /> </td>
				<td class="textoTamaño7">
					<form action="<?=base_url('editarProducto')?>" method="post" novalidate> 
						<button type="submit-form" name="modificar"  value=<?php echo $row->id_producto?>> <img src="<?php echo base_url();?>assets/img/Logotipos/editar.png" data-toggle="tooltip" data-placement="right" title="Editar Datos"  class="w-100 iconosNavegacion" /></button>
					</form> 
				</td>
				<?php
					if(($row->estado==1)) {?>
						<td>
							<form action="<?=base_url('desactivarProducto')?>" method="post" novalidate>
								<button type="submit-form" class="btn btn-primary" name="estado" value=<?php echo $row->id_producto?>> <img data-toggle="tooltip" data-placement="right" title="Desactivar Producto"  class="w-100 iconosNavegacion" src="<?php echo base_url();?>assets/img/Logotipos/cancelar.png"/>
								</button>
							</form>
						</td>

				<?php } else { ?>
						<td> 
							<form action="<?=base_url('activarProducto')?>" method="post" novalidate>
								<button type="submit-form" class="btn btn-primary" name="estado" value=<?php echo $row->id_producto?>> <img data-toggle="tooltip" data-placement="right" title="Activar Producto"  value=<?php echo $row->estado?> class="w-100 iconosNavegacion" src="<?php echo base_url();?>assets/img/Logotipos/confirmarProducto.png"/>
								</button>
							</form>
						</td>
				<?php } ?>
			<td>

				<a  class="btn btn-primary" href=<?php echo base_url('descripcionProductoAdm')."/".$row->id_producto?>>Descripcion </a>
					
			</td>

			
			</tr>
			<?php } ?><!--termina el foreach-->

			<td>
			
		</tbody>
	</table>
	<?php echo $this->pagination->create_links();?>
</section>

