<section class="container mx-3 mt-3">
	<div class="text-center">
		<span class="badge badge-dark textoTamaño8 pr-4">Cantidades Vendidas y Recaudación Por Subcategoria</span>
	</div>
	<div class="row justify-content-center mt-3">
		<div class="col justify-content-center">
		</div>
			<div class="col justify-content-center">
				<table  id="tablaProductos" class="table m-auto mt-3 table table-borded table-striped table-hover table-responsive">
				  <thead>
				    <tr>
				      <th scope="col">Categoria</th>
				      <th scope="col">Cantidad</th>
				      <th scope="col">Recaudacion</th>
				      
				    </tr>
				  </thead>
				  <tbody>
				  	
						<?php foreach ($productos as $valores=>$datos) {?>
						<tr>
							<td class="textoTamaño7"> <?php echo $datos['subcategoria']; ?> </td>
							<td class="textoTamaño7"> <?php echo $datos['total']; ?> </td>
							<td class="textoTamaño7"> <?php echo $datos['recaudacion']; ?> </td>
							<td class="textoTamaño7">
								
									
								
							</td>
						</tr>
						<?php } ?><!--termina el foreach-->
				    <tr>
					    <td colspan="2"> </td>
	        			<td class="left"><strong>Unidades Totales vendidas</strong></td>
	        			<td class="left"><?php echo $unidadesTotales; ?>
        			</tr>

        			<tr>
	        			<td colspan="2"> </td>
	        			<td class="left"><strong>Recaudacion Total</strong></td>
	        			<td class="left">$<?php echo $recaudacionTotal; ?>
        			</tr>
				  </tbody>
				</table>
		</div>
		<div class="col justify-content-center">
		</div>
    </div>
</section>