<section>
<div class="jumbotron">
  <h1 class="display-4">¡No tiene compras realizadas!</h1>
  <p class="lead">Aún no ha tenemos registrado alguna compra suya <?php echo $cliente->nombre;?></p>
  <p class="lead">Por favor, revise nuestros productos haciendo clic en la barra de navegación con los nombres de los productos. Seguro encontrará algo de su agrado. Si no se encuentra el producto que desea o tiene alguna otra sugerencia, queja o duda puede contactarse con nosotros <a href=<?php echo base_url('contactoyconsultas');?>>haciendo clic aquí<span></a>
  

  <hr class="my-4">
  <p>Pulsa el botón para ir al inicio de la página o busca más productos.</p>
  <a class="btn btn-primary btn-lg" href=<?php echo base_url('inicio');?> role="button">Volver a la página principal</a>
</div>
</section>