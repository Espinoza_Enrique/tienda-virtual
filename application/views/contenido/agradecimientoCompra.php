<section>
<div class="jumbotron" style="font-size: 1rem;">
  <h1 class="display-4" style="font-size: 2.5rem;">¡Muchas Gracias Por Su Compra!</h1>
  <p class="lead" style="font-size: 1rem;">Agradecemos tu compra <?php echo $nombreUsuario?>. Por favor, revisa nuestros productos destacados para ver si hay algo más de tu agrado</p>
  <p class="lead" style="font-size: 1rem;">Productos Agregados</p>
  <?php foreach ($agregados as $elementos=>$producto) {;?>
  	<p><?php echo $producto['nombre'];?></p>
  <?php };?>

  <?php if(!empty($noAgregados)) {;?>
	   <p class="lead" style="font-size: 1rem;">Productos No Agregados Por Problemas de Stock</p>
	  <?php foreach ($noAgregados as $elementos=>$producto) {;?>
	  	<p><?php echo $producto['nombre'];?></p>
	  <?php };?>
  <?php };?>

  <hr class="my-4">
  <p style="font-size: 1rem;">Pulsa el botón para ir al inicio de la página o busca más productos.</p>
  <a class="btn btn-primary btn-lg" href=<?php echo base_url('inicio');?> role="button">Volver a la página principal</a>
</div>
</section>


