<section class="container mx-5">
        <div class="form-group mx-auto col-auto mt-5 w-50">
            <a class="textoTamaño7" style="font-size: 1rem;">Lista De Todas Las Compras</a>
        </div>
        
        <div class="row">
            <div class="col-auto">
            </div>
            <div class="col-auto mx-5 flex-grow-1">
                <table id="tablaProductos" class="table table-borded table-striped table- hover table-responsive mx-5">
                        <!--encabezados de la tabla-->
                        <thead>
                                <th class="textoTamaño7 text-center">Número Factura</th>
                                <th class="textoTamaño7 text-center">Nombre</th>
                                <th class="textoTamaño7 text-center">Apellido</th>
                                <th class="textoTamaño7 text-center">Fecha</th>
                                <th class="textoTamaño7 text-center">Ver Detalles</th>
                        </thead>
                        <tbody>
                                <?php 
                                ;
                                foreach ($pedidos as $filas) { 
                                       
                                        ?>
                        <tr>
                                <td class="textoTamaño7 text-center"> <?php echo $filas->orden_id;?> </td>
                                <td class="textoTamaño7 text-center"> <?php echo $filas->nombre; ?> </td>
                                <td class="textoTamaño7 text-center"> <?php echo $filas->apellido; ?> </td>
                                <td class="textoTamaño7 text-center"> <?php echo $filas->orden_fecha; ?> </td> 
                                <td class="textoTamaño7 text-center"> <form  action="<?=base_url('detallesCompraCliente')?>" method="post" novalidate>
                                                <button  type="submit-form" class="btn btn-primary border"  name="idPedido" value=<?php echo $filas->orden_id;?>>
                                                     Detalles
                                                </button>
                                        </form> </td>
                               <?php };?>
                            
                        </tr>
                        </tbody>
                

            </table>
        </div>
        <div class="col-auto">
        </div>
    </div>
    <?php echo $this->pagination->create_links();?>
</section>