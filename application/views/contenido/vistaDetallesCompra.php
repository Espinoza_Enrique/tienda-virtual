<section>
        <div class="form-group mx-auto col-auto mt-5 w-50">
            <a class="textoTamaño7" style="font-size: 1rem;">Detalles de Compras</a>
        </div>
        

        <table id="tablaProductos" class="table table-borded table-striped table- hover table-responsive mx-5">
                <!--encabezados de la tabla-->
                <thead>
                        <th class="textoTamaño7">Producto</th>
                        <th class="textoTamaño7">Precio</th>
                        <th class="textoTamaño7">Cantidad</th>
                        <th class="textoTamaño7">Fecha</th>
                        
                </thead>
                <tbody>
                        <?php 
                        
                        foreach ($datos as $pedidos) { 
                                ?>
                <tr>
                        <td class="textoTamaño7"> <?php echo $pedidos->nombre_producto; ?> </td>
                        <td class="textoTamaño7"> <?php echo $pedidos->precio; ?> </td>
                        <td class="textoTamaño7"> <?php echo $pedidos->cantidad; ?> </td>
                        <td class="textoTamaño7"> <?php echo $pedidos->orden_fecha; ?> </td>
                       
                       <?php };?>
                    
                </tr>
                </tbody>
        

    </table>
</section>