<!--muestra los productos-->
<section class="container ml-5 mt-5">
  		
			<div class="jumbotron mx5 mx-3 mt-3">
		  <p class="display-4 text-center font-weight-bold" style="font-size: 1.0rem;"> No hay producto con la frase buscada <?php echo "'".$clave."'";?></3>
		  <br>
		  
		  <hr class="my-4">
		   <hr class="my-4">
  		  <p style="font-size: 1rem;">No Existen productos que cumplen con el criterio de búsqueda. Por favor, revise nuestros productos eligiendo una categoría. Si no encuentra el producto que está buscando por favor escribanos un mensaje llenando el siguiente formulario o comuniquese mediante los medios de contacto señalados <a href=<?php echo base_url('contactoyconsultas');?>>Sugerencias y Consultas</a> .</p>
		
		
				<a class="btn btn-primary btn-lg textoTamaño7 " style="font-size: 1rem;" href=<?php echo base_url('inicio');?>>Volver al Inicio</a>

	</div>
	<br>
	<br>
	
 </section>
