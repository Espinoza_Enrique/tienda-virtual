<section>
        <div class="form-group mx-auto col-auto mt-5 w-50">
            <h1 class="textoTamaño7">Carrito De Compras</h1>
        </div>
        <?php if($vacio==" ") {;?>
                <div class="form-group mx-auto col-auto mt-5 w-50">

                 <h1 class="textoTamaño7"><?php $vacio;?></h1>
                </div>
        <?php } else {;?>

        <table id="tablaProductos" class="table table-borded table-striped table- hover table-responsive mx-3">
                <!--encabezados de la tabla-->
                <thead>
                        <th class="textoTamaño7">Item</th>
                        <th class="textoTamaño7 text-center">Nombre</th>
                        <th class="textoTamaño7">Precio</th>
                        <th class="textoTamaño7">Cantidad</th>
                        <th class="textoTamaño7">Subtotal</th>
                        <th class="textoTamaño7">Eliminar Unidad</th>
                        <th class="textoTamaño7">Eliminar Todo</th>
                </thead>
                <tbody>
                        <?php 
                        $elemento=0;
                        foreach ($this->cart->contents() as $items) {
                                $elemento++;?>
                <tr>
                        <td class="textoTamaño7 text-center"> <?php echo $elemento; ?> </td>
                        <td class="textoTamaño7 text-center"> <?php echo $items['name']; ?> </td>
                        <td class="textoTamaño7 text-center"> <?php echo $items['price']; ?> </td>
                        <td class="textoTamaño7 text-center"> <?php echo $items['qty']; ?> </td>
                        <td class="textoTamaño7 text-center"> <?php echo ($items['price'] * $items['qty']); ?> </td>

                        
                        <td>
                                <form  action="<?=base_url('eliminar')?>" method="post" novalidate>
                                       <button data-toggle="tooltip"  title="Descontar una unidad" type="submit-form" name="id" class="btn btn-primary ml-3 border thumbnail" value=<?php echo $items['rowid'];?>>  
                                                <a>
                                                        <img src="<?php echo base_url();?>assets/img/Logotipos/flechaRojaBajo.png"/">
                                                </a>
                                        </button>
                                 </form>
                        </td>
                         <td>
                                <form  action="<?=base_url('productoEliminado')?>" method="post" novalidate>
                                       <button type="submit-form" data-toggle="tooltip"  title="Quitar del Carrito"name="id" class="btn btn-primary ml-3 border" value=<?php echo $items['rowid'];?>>  
                                                <a>
                                                        <img src="<?php echo base_url();?>assets/img/Logotipos/eliminarItem.png"/">
                                                </a>
                                        </button>
                                 </form>
                        </td>
                       <?php };?>
                </tr>
                </tbody>
        <tr>
        <td colspan="2"> </td>
        <td class="right"><strong>Acciones</strong></td>
        <?php if (!empty($this->cart->contents())) {?>
        <td class="right">
             <button data-toggle="tooltip" title="Vaciar Carrito" type="submit-form" name="id" class="btn btn-primary ml-3 border" onclick="limpiar_carrito()">  
                    <a>
                        <img src="<?php echo base_url();?>assets/img/Logotipos/tachoBasura.png"/">
                    </a>
            </button>
                            
        </td>
        
        <td class="right">
             <button data-toggle="tooltip"  title="Efectuar Pago" type="submit-form" name="pago" value="3" id="compra" data-carrito=<?php echo "hola";?> class="btn btn-primary ml-3 border" onclick="efectuar_pago()">  
                    <a>
                        <img src="<?php echo base_url();?>assets/img/Logotipos/signoPesos.png"/">
                    </a>
            </button>
                            
        </td>
        <?php };?>
        </tr>
        <td colspan="2"> </td>
        <td class="right"><strong>Total</strong></td>
        <td class="right">$<?php echo $this->cart->format_number($this->cart->total()); ?>
        </tr>
        </table>
        <?php };?>
</section>

<script>
function limpiar_carrito() {
    var result=confirm('Desea vaciar el carrito?');
    if(result) {
        window.location="<?php echo base_url('destruirCarrito');?>";
    }else {
        return false;
    }

}

 function efectuar_pago() {
 

    var result=confirm('¿Desea comprar los productos del carrito?');
    if(result) {
        window.location="<?php echo base_url('realizarCompra');?>";
    }else {
        return false;
    }

}

</script>

