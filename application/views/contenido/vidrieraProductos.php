
<!--muestra los productos-->
<section class="container ml-5">
  		
			<nav class="row  ml-5">
				
				<?php 
					foreach ($productos as $filas) {
						
						$precio=$filas->precio;
						$rutaImagen=base_url(). "uploads/imagenes/".$filas->nombreFoto;
						?>
						<div class="card col-lg-3 col-xl-3 col-md-4 col-sm-6 mt-5 bg-dark text-white w-75" style="width: 18rem;">
						  <img src=<?php echo $rutaImagen;?> class="card-img-top img-fluid w-75 mt-3 ml-5" alt="...">
						  <div class="card-body">
						    <p class="card-title tamañoTexto8 text-center" style="font-size: 1rem;"><?php echo $filas->nombre_producto;?></p>
						    <p class="card-text tamañoTexto8 text-center" style="font-size: 1rem;">
						    	<?php echo "Precio: " . $filas->precio. "\n"; echo "Garantía: ".$filas->garantia." días";?>
						    </p>
						    <div class="btn-group d-flex justify-content-center" role="group">
								<!--<form  action="<?= base_url('mostrarDescripcion')?>" method="post" novalidate>
									 	<button  type="form-submit" class="btn btn-primary border"  name="detalles" value=<?php echo $filas->id_producto;?>>
										 	 Detalles
										</button>
								</form>-->
								
								 <a class="btn bg-primary text-white" name="detalles"  href=<?php echo base_url('mostrarDescripcion')."/".$filas->id_producto?>>
											<!--<img src="<?php echo base_url();?>assets/img/Logotipos/editar.png" data-toggle="tooltip" data-placement="right" title="Detalles"  class="w-100 iconosNavegacion" />-->
											Descripción
									 </a>
								
									 <!--muestro el botón si hay una sesión activa-->
									 <?php if($this->session->userdata('perfil')==2 and $filas->stock>0 and $filas->estado==1) {?>
									<form  action="<?=base_url('agregarCarrito')?>" method="post" novalidate>
										  <button type="form-submit" name="id" class="btn btn-primary ml-3 border" value=<?php echo $filas->id_producto;?>>  
										  	<a>
										  		<img src="<?php echo base_url();?>assets/img/Logotipos/carrito.png"/">
										  	</a>
										  </button>
								    </form>

								    <?php }; ?>
	
							</div>
							
							<hr>
						  </div>
						</div>
				<?php };?>
			
			</nav>
	<br>
	<br>
	<?php echo $this->pagination->create_links();?>
 </section>


