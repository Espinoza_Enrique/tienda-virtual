<!--muestra los detalles de las consultas-->
<section class="container mt-5 text-center">
			<div class="text-center text-white bg-primary mb-4">
				 
					  <label class="text-center mt-3 font-weight-bold" style="font-size: 1rem;">Detalles de Consulta</label>
					  <div class="row mt-3 mx-3">
					    <div class="col mx-2 border mb-3">
					      <label>Nombre del Consultor</label>
					      <input type="text" class="form-control text-center mb-3" placeholder="Nombre" name="nombre" value=<?php echo $detalle->nombre;?> required>
					    </div>
					   
					    <div class="col mx-2 border mb-3">
					    	<label>Mail</label>
					      <input type="mail" class="form-control text-center mb-3" placeholder="Mail" name="mail"  value=<?php echo $detalle->mail;?>  required>
					    </div>
					  </div>

					 
					  
					  

					 <div class="form-group w-50 text-center mx-3 border mt-2">
					 	<label class="mt-2 mx-2" for="formularioConsulta">Motivo</label>
					    <select class="custom-select text-center mb-2 w-75 mt-2" name="motivo" required>
						      <option selected><?php echo $detalle->nombreMotivo;?> </option>
				
					    </select>
					 </div>
					 <div class="container mb-3">
			  			  <div class="form-group mx-2 mb-4 mt-2 border d-flex- justify-content-center">
							    <label class="mt-3" for="exampleFormControlTextarea1">Detalles De La Consulta</label>
							    <textarea class="form-control mb-4 w-75 mx-5" id="exampleFormControlTextarea1" name="consulta" rows="3" required> <?php echo $detalle->consulta;?> </textarea>
						  </div>
					  </div>	
	
	</div>
	<div class="mt-5">
			<?php
				if(($leido==0)) {?>
						<a type="button" name="detalles"  class="bg-success text-white border mb-3"href=<?php echo base_url('marcarLeido')."/".$idConsulta?>>
				Marcar Como Leido
			</a>
			<?php }?>
	</div>
</section>
			