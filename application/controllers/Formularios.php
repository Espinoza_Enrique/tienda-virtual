
<?php
/*Permite manejar todo lo referentes a los formularios*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Formularios extends CI_Controller {

	function __construct() {
		parent::__construct();
		//$this->load->model('ModeloProducto');
	}

	/**
	 *Muestra el formulario para el ingreso de un nuevo socio desde la página
	 */
	public function index()
	{
		$categorias['categorias']=$this->ModeloProducto->get_categoria();
		$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
		$titulo = array('titulo' => 'Registro');
		$this->load->view('plantillas/encabezado',$titulo);
		$this->load->view('plantillas/barraNavegacion');
		$this->load->view('plantillas/barraOpciones',$categorias);
		$this->load->view('contenido/formularioIngreso');
		$this->load->view('plantillas/pie');
		
	}

		/**
	 *Muestra el formulario para realizar loggin
	 */
	public function cargarFormularioLoggin()
	{
		$categorias['categorias']=$this->ModeloProducto->get_categoria();
		$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
		$titulo = array('titulo' => 'Loggin');
		$this->load->view('plantillas/encabezado',$titulo);
		$this->load->view('plantillas/barraNavegacion');
		$this->load->view('plantillas/barraOpciones',$categorias);
		$this->load->view('contenido/formularioLoggin');
		$this->load->view('plantillas/pie');
		
		
		
	}


		/**
	 *Muestra el formulario para contactos y consultas
	 */
	public function cargarFormularioContacto()
	{
		$categorias['categorias']=$this->ModeloProducto->get_categoria();
		$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
		$titulo = array('titulo' => 'Contacto y Consultas');
		$this->load->view('plantillas/encabezado',$titulo);
		$this->load->view('plantillas/barraNavegacion');
		$this->load->view('plantillas/barraOpciones',$categorias);
		$this->load->view('contenido/contacto');
		$this->load->view('plantillas/pie');
		
		
		
	}

	/*Permite validar los datos del formulario de ingreso*/
	public function registrar_cliente()
	{
		/*muestra los criterios de control para cada campo*/
		$this->form_validation->set_rules('apellido','Apellido del usuario', 'required');
		$this->form_validation->set_rules('nombre', 'Nombre del usuario', 'required');            
		$this->form_validation->set_rules('mail', 'Email', 'required|valid_email|is_unique[personas.mail]');       
 		$this->form_validation->set_rules('password', 'Contraseña', 'trim|required|min_length[8]');      
 		$this->form_validation->set_rules('passconf', 'Confirmar password', 'trim|required|matches[password]');

 		/*muestra los mensajes de error para los campos inválidos*/
 		$this->form_validation->set_message('valid_email', 'El campo %s debe ser un mail válido'); 
 
		$this->form_validation->set_message('integer', 'El campo %s debe poseer solo numeros enteros'); 
 
		$this->form_validation->set_message('required', 'El campo %s es obligatorio'); 
 
		$this->form_validation->set_message('min_length', 'El campo %s debe contener como mínimo %d caracteres'); 

		$this->form_validation->set_message('matches', 'contraseñas no coinciden');

 		$this->form_validation->set_message('passconf', 'Las contraseñas no coinciden');

 		$this->form_validation->set_message('is_unique', 'El mail ya pertenece a otro usuario');


 		/*muestra lo que sucede si hay o no errores*/

 		 if ($this->form_validation->run() == FALSE) { 
 			//en caso de ser falso, vuelve a cargar la página
            $this->index(); 
 
        } else {       
        	
        	$this->insertar_cliente();
        }; 
	}

	/*Permite validar los datos requeridos para que el cliente inicie sesión*/
	public function loggin_cliente()
	{
		/*$this->form_validation->set_rules('apellido','Apellido del usuario', 'required');
		$this->form_validation->set_rules('nombre', 'Nombre del usuario', 'required');*/            
		$this->form_validation->set_rules('mail', 'Email', 'required|valid_email');      
		//$this->form_validation->set_rules('telefono', 'Telefono del usuario', 'required|integer'); 
 		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');      
 		/*$this->form_validation->set_rules('passconf', 'Confirmar password', 'trim|required|matches[password]');*/

 		$this->form_validation->set_message('valid_email', 'El campo %s debe ser un mail válido'); 
 
		//$this->form_validation->set_message('integer', 'El campo %s debe poseer solo numeros enteros'); 
 
		$this->form_validation->set_message('required', 'El campo %s es obligatorio'); 
 
		/*$this->form_validation->set_message('min_length', 'El campo %s debe contener como mínimo %d caracteres'); 

 		$this->form_validation->set_message('matches', 'contraseñas no coinciden');*/

 		 if ($this->form_validation->run() == FALSE) { 
 
                $this->cargarFormularioLoggin(); 
 
        } else {            
        	echo "datos correctos";
        }; 
	}

	/*Permite validar los datos requeridos para el formulario de consulta*/
	public function consultas_cliente()
	{
		//$this->form_validation->set_rules('apellido','Apellido del usuario', 'required');
		$this->form_validation->set_rules('nombre', 'Nombre del usuario', 'required');           
		$this->form_validation->set_rules('mail', 'Email', 'required|valid_email');      
		$this->form_validation->set_rules('motivo', 'Motivo consulta', 'required'); 
 		$this->form_validation->set_rules('consulta', 'Consulta', 'required');      
 		/*$this->form_validation->set_rules('passconf', 'Confirmar password', 'trim|required|matches[password]');*/

 		$this->form_validation->set_message('valid_email', 'El campo %s debe ser un mail válido'); 
 
		//$this->form_validation->set_message('integer', 'El campo %s debe poseer solo numeros enteros'); 
 
		$this->form_validation->set_message('required', 'El campo %s es obligatorio'); 
 
		/*$this->form_validation->set_message('min_length', 'El campo %s debe contener como mínimo %d caracteres'); 

 		$this->form_validation->set_message('matches', 'contraseñas no coinciden');*/

 		 if ($this->form_validation->run() == FALSE) { 
 
                $this->cargarFormularioConsulta(); 
 
        } else {            
        	echo "datos correctos";
        }; 
	}

	/*permite insertar un cliente a la base de datos mediante los datos del formulario de para crear una nueva cuenta*/
	public function insertar_cliente() {
		/*capturamos los datos del formulario en un arreglo*/
		$cliente = array (
			/*Almaceno en los campos de la tabla los datos en los controles. En el método post coloco el name del control.*/
			'apellido'=>$this->input->post('apellido'),
			'nombre'=>$this->input->post('nombre'),
			'mail'=>$this->input->post('mail'),
			'IdPerfil'=>2//indica que es un cliente
		);

		/*Se carga los datos del cliente y los datos del usuario. Para completar el campo IdPersona de la tabla usuario se debe capturar el último id que hace referencia al último registro de la tabla que tiene los datos del cliente*/
		$this->load->model('ModeloCliente');//cargo el Modelo
		$this->ModeloCliente->guardar_cliente($cliente);//llamo al método guardar_cliente del Modelo ModeloCliente
		$idPersona=$this->db->insert_id();//obtengo el id del último registro ingresado
		/*almaceno los datos que se usaran para los registros de la tabla usuario*/
		$usuario=array (
			'mail'=>$this->input->post('mail'),
			'contraseña'=>base64_encode($this->input->post('password')),//encripto el password
			'IdPersona'=>$idPersona
		);

		/*cargo los datos en la tabla de clientes*/
		$this->ModeloCliente->guardar_usuario($usuario);
		redirect('loggin');
	}
	
}