<?php 
class ControlCarrito extends CI_Controller {

	function __construct() {
		parent::__construct();
		//cargo el Modelo pertinente, en este caso el Modelo de producto
		//$this->load->model('ModeloProducto');
		//contrrola que haya una sesión iniciada y que el perfil sea 1 que corresponde a un usuario no administrador
		if(!$this->session->userdata('login') or ($this->session->userdata('perfil')==1) ) {
				redirect('inicio');
			}
	}

	public function vistaCarrito(){
		$contenido=$this->cart->total_items();
		$categorias['categorias']=$this->ModeloProducto->get_categoria();
		$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
		$texto=null;
		if($contenido==0) {
			$texto['vacio']="";
			$titulo = array('titulo' => 'Carrito');
			$this->load->view('plantillas/encabezado',$titulo);
			$this->load->view('plantillas/barraNavegacionRegistrado');
			$this->load->view('plantillas/barraOpciones',$categorias);
			$this->load->view('contenido/carritoVacio');
			$this->load->view('plantillas/pie');
		}else {
			$texto['vacio']="Carrito Vacio";
			$titulo = array('titulo' => 'Carrito');
			$this->load->view('plantillas/encabezado',$titulo);
			$this->load->view('plantillas/barraNavegacionRegistrado');
			$this->load->view('plantillas/barraOpciones',$categorias);
			$this->load->view('contenido/vistaCarrito2',$texto);
			$this->load->view('plantillas/pie');
		}
		
	}

	public function agregarCarrito(){
		$id=$this->input->post('id');
		$producto=$this->ModeloProducto->get_ProductoId($id);
		$stock=$producto->stock;
		$precio=$producto->precio;
		$nombre=$producto->nombre_producto;
		$permitirAgregar=true;
		foreach($this->cart->contents() as $item) {
			//controla que el stock del producto no sea menor a la cantidad en el carrito
			if($item['id']==$id and $item['qty']>=$stock) {
				$permitirAgregar=false;
				//echo "problemas con la entrada";die;
				break;
			};
		}
		if($permitirAgregar) {

			//echo "cargar";die;
			//echo "Id:" . $id;echo "<br>"; echo "Nombre: ".$nombre;echo "<br>"; echo "Precio: ".$precio;die;
			$data=array('id'=>$id,'qty'=>1,'price'=>$precio,'name'=>$nombre);
			//print_r($data);die;
			$this->cart->insert($data);
			//$contenido=$this->cart->contents();
			//print_r($contenido);die;
		}
		redirect('Carrito');
	} 

	public function eliminarUnidad() {
		$id=$this->input->post('id');
		$producto=$this->cart->get_item($id);
		$cantidad=$producto['qty']-1;
		$data = array(
        'rowid' => $id,
        'qty' => $cantidad
		);

		$this->cart->update($data);
		
		redirect('Carrito');
	}

	public function eliminarProducto() {
		$id=$this->input->post('id');
		$producto=$this->cart->get_item($id);
		$data = array(
        'rowid' => $id,
        'qty' => 0
		);

		$this->cart->update($data);
		
		redirect('Carrito');
	}

	public function eliminarCarrito() {
		$this->cart->destroy();
		
		redirect('Carrito');
	}

	/*muestra un mensaje de agradecimiento tras la compra*/
	public function mostrarAgradecimiento() {
		//$this->load->model('ModeloCliente');
		$usuario=$this->ModeloCliente->getPersona($this->session->userdata('idUsuario'));
		$datos['nombreUsuario']=$usuario->nombre;
		$titulo = array('titulo' => '¡Muchas Gracias!');
		$this->load->view('plantillas/encabezado',$titulo);
		$this->load->view('plantillas/barraNavegacionRegistrado');
		$this->load->view('contenido/agradecimientoCompra',$datos);//paso dos arrays como datos a la vista
		$this->load->view('plantillas/pie');
	}
}