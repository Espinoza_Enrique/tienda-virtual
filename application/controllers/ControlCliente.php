
<?php
/*Permite manejar todo lo referentes a los clientes*/
defined('BASEPATH') OR exit('No direct script access allowed');

class ControlCliente extends CI_Controller {

	function __construct() {
		parent::__construct();
		/*$this->load->model('ModeloCliente');
		$this->load->model('ModeloProducto');*/
		if($this->session->userdata('perfil')==2){
			redirect('inicio');
		}
	}

	/*controlador encargado de mostrar el formulario para el registro del cliente*/
	public function index()
	{
		$categorias['categorias']=$this->ModeloProducto->get_categoria();
		$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
		$titulo = array('titulo' => 'Registro');
		$this->load->view('plantillas/encabezado',$titulo);
		$this->load->view('plantillas/barraNavegacion');
		$this->load->view('plantillas/barraOpciones',$categorias);
		$this->load->view('contenido/formularioIngreso');
		$this->load->view('plantillas/pie');
		
	}

	public function mostrarError() {
		$error['error']=true;
		$this->load->view('plantillas/encabezado',$titulo);
		$this->load->view('plantillas/pie',$error);
		$this->load->view('contenido/ventanaModalError',$error);
	}
		/**
	 *Muestra el formulario para contactos y consultas
	 */
	public function cargarFormularioContacto()
	{
		$categorias['categorias']=$this->ModeloProducto->get_categoria();
		$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
		$titulo = array('titulo' => 'Contacto y Consultas');
		$this->load->view('plantillas/encabezado',$titulo);
		$this->load->view('plantillas/barraNavegacion');
		$this->load->view('plantillas/barraOpciones',$categorias);
		$this->load->view('contenido/contacto');
		$this->load->view('plantillas/pie');
		
		
		
	}


	/*Permite validar los datos del formulario de ingreso para un nuevo cliente*/
	public function registrar_cliente()
	{
		/*muestra los criterios de control para cada campo*/
		$this->form_validation->set_rules('apellido','Apellido del usuario', 'required');
		$this->form_validation->set_rules('nombre', 'Nombre del usuario', 'required');            
		$this->form_validation->set_rules('mail', 'Email', 'required|valid_email|is_unique[personas.mail]|max_length[30]');   
		$this->form_validation->set_rules('dni', 'Dni', 'required|integer|is_unique[personas.dni]|max_length[30]'); 
 		$this->form_validation->set_rules('password', 'Contraseña', 'trim|required|min_length[8]|max_length[10]'); 
 		$this->form_validation->set_rules('telefono','Teléfono','required|integer'); 
 		$this->form_validation->set_rules('altura','Altura','required|integer|min_length[2]'); 
 		$this->form_validation->set_rules('domicilio','Calle','required|max_length[30]');      
 		$this->form_validation->set_rules('passconf', 'Confirmar password', 'trim|required|matches[password]');
 		$this->form_validation->set_rules('aceptar', 'Aceptar Condiciones', 'trim|required');

 		/*muestra los mensajes de error para los campos inválidos*/
 		$this->form_validation->set_message('valid_email', 'El campo %s debe ser un mail válido'); 
 
		$this->form_validation->set_message('integer', 'El campo %s debe poseer solo numeros enteros'); 
 
		$this->form_validation->set_message('required', 'El campo %s es obligatorio'); 

 		
 		$this->form_validation->set_message('controlarTelefono', 'El telefono debe ser sólo numérico'); 

		$this->form_validation->set_message('min_length', 'El campo %s debe contener como mínimo %d caracteres'); 

		$this->form_validation->set_message('max_length', 'El campo %s debe contener como máximo %d caracteres');

		$this->form_validation->set_message('matches', 'contraseñas no coinciden');

 		$this->form_validation->set_message('passconf', 'Las contraseñas no coinciden');

 		$this->form_validation->set_message('is_unique', 'El mail ya pertenece a otro usuario');


 		/*muestra lo que sucede si hay o no errores*/

 		 if ($this->form_validation->run() == FALSE) { 
 			//en caso de ser falso, vuelve a cargar la página
            $this->index(); 
 
        } else {       
        	
        	$this->insertar_cliente();
        }; 
	}

	/*Permite validar los datos requeridos para el formulario de consulta*/
	public function controlarConsultas()
	{
		//$this->form_validation->set_rules('apellido','Apellido del usuario', 'required');
		$this->form_validation->set_rules('nombre', 'Nombre del usuario', 'required');           
		$this->form_validation->set_rules('mail', 'Email', 'required|valid_email');      
		$this->form_validation->set_rules('motivo', 'Motivo consulta', 'required|callback_validar_motivo'); 
 		$this->form_validation->set_rules('consulta', 'Consulta', 'required');      
 		/*$this->form_validation->set_rules('passconf', 'Confirmar password', 'trim|required|matches[password]');*/

 		$this->form_validation->set_message('valid_email', 'El campo %s debe ser un mail válido'); 
 
		//$this->form_validation->set_message('integer', 'El campo %s debe poseer solo numeros enteros'); 
 
		$this->form_validation->set_message('required', 'El campo %s es obligatorio'); 
 
		/*$this->form_validation->set_message('min_length', 'El campo %s debe contener como mínimo %d caracteres'); 

 		$this->form_validation->set_message('matches', 'contraseñas no coinciden');*/

 		 if ($this->form_validation->run() == FALSE) { 
 
              $this->cargarFormularioContacto(); 
 
        } else {            
        	$this->insertarConsulta();
        }; 
	}

	/*inserta la consulta en la tabla de consultas*/
	public function insertarConsulta() {
		/*capturamos los datos del formulario en un arreglo*/
		$consulta=array (
			/*Almaceno en los campos de la tabla los datos en los controles. En el método post coloco el name del control.*/
			'nombre'=>$this->input->post('nombre'),
			'mail'=>$this->input->post('mail'),
			'fecha'=>date("Y-m-d"),
			'motivo'=>$this->input->post('motivo'),
			'consulta'=>$this->input->post('consulta'),
			'leido'=>0//indica que es un cliente
		);
		$this->ModeloCliente->guardarConsulta($consulta);
		/*Se carga los datos del cliente y los datos del usuario. Para completar el campo IdPersona de la tabla usuario se debe capturar el último id que hace referencia al último registro de la tabla que tiene los datos del cliente*/
		
		redirect('contactoyconsultas');
	}


	/*permite insertar un cliente a la base de datos mediante los datos del formulario de para crear una nueva cuenta*/
	public function insertar_cliente() {
		/*capturamos los datos del formulario en un arreglo*/
		$cliente=array (
			/*Almaceno en los campos de la tabla los datos en los controles. En el método post coloco el name del control.*/
			'apellido'=>$this->input->post('apellido'),
			'nombre'=>$this->input->post('nombre'),
			'mail'=>$this->input->post('mail'),
			'telefono'=>$this->input->post('telefono'),
			'dni'=>$this->input->post('dni'),
			'calle'=>$this->input->post('domicilio'),
			'altura'=>$this->input->post('altura'),
			'IdPerfil'=>2//indica que es un cliente
		);

		/*Se carga los datos del cliente y los datos del usuario. Para completar el campo IdPersona de la tabla usuario se debe capturar el último id que hace referencia al último registro de la tabla que tiene los datos del cliente*/
		//cargo el Modelo
		$this->ModeloCliente->guardar_cliente($cliente);//llamo al método guardar_cliente del Modelo ModeloCliente
		$idPersona=$this->db->insert_id();//obtengo el id del último registro ingresado
		/*almaceno los datos que se usaran para los registros de la tabla usuario*/
		$usuario=array (
			'mail'=>$this->input->post('mail'),
			'password'=>base64_encode($this->input->post('password')),//encripto el password
			'IdPersona'=>$idPersona,
			'estado'=>1
		);

		/*cargo los datos en la tabla de clientes*/
		$this->ModeloCliente->guardar_usuario($usuario);
		redirect('loggin');
	}
	
	/*valida que una categoria haya sido seleccionada*/
	 function validar_motivo($motivo) {
	 	
			if ($motivo==0) {
				/*genero el mensaje*/
				$this->form_validation->set_message ('validar_motivo','Seleccione un motivo para su consulta');
				return false;/*retorno falso para que vuelva a mostrar el formulario para cargar los datos correctos*/
			} 
			else {
				return true;/*indica que se seleccionó una categoría*/
			}
		
	}

	/*valida que una categoria haya sido seleccionada*/
	 function controlarTelefono() {
	 	
	 	     $telefono=$this->input->post('telefono');
	 	     $telefono=trim($telefono);
	 	     if (is_integer($telefono)) {
				/*genero el mensaje*/
				return true;/*retorno falso para que vuelva a mostrar el formulario para cargar los datos correctos*/
			} 
			else {
				return false;/*indica que se seleccionó una categoría*/
			}
		
	}

	public function CambiarDatos()
	{
		/*muestra los criterios de control para cada campo*/
		$this->form_validation->set_rules('apellido','Apellido del usuario', 'required');
		$this->form_validation->set_rules('nombre', 'Nombre del usuario', 'required');            
		$this->form_validation->set_rules('mail', 'Email', 'required|valid_email|callback_controlMail|max_length[30]');   
		$this->form_validation->set_rules('dni', 'Dni', 'required|integer|is_unique[personas.dni]|max_length[30]'); 
 		$this->form_validation->set_rules('password', 'Contraseña','callback_controlarSimilitudes');
 		$this->form_validation->set_rules('passwordAntiguo', 'Contraseña', 'trim|callback_controlarPassword'); 
 		$this->form_validation->set_rules('telefono','Teléfono','required|integer');      
 		$this->form_validation->set_rules('passconf', 'Confirmar password',);


 		/*muestra los mensajes de error para los campos inválidos*/
 		$this->form_validation->set_message('valid_email', 'El campo %s debe ser un mail válido');

 		$this->form_validation->set_message('controlMail', 'Ya existe un usuario con el mail indicado'); 

 		$this->form_validation->set_message('controlarPassword', 'El password colocado no pertenece a un usuario');  

 		$this->form_validation->set_message('controlarSimilitudes', 'Los password colocados no coinciden o tienen menos de 8 caracteres'); 
 
		$this->form_validation->set_message('integer', 'El campo %s debe poseer solo numeros enteros'); 
 
		$this->form_validation->set_message('required', 'El campo %s es obligatorio'); 
 		
 		$this->form_validation->set_message('controlarTelefono', 'El telefono debe ser sólo numérico'); 

		$this->form_validation->set_message('min_length', 'El campo %s debe contener como mínimo %d caracteres'); 

		$this->form_validation->set_message('max_length', 'El campo %s debe contener como máximo %d caracteres');

		$this->form_validation->set_message('matches', 'contraseñas no coinciden');

 		$this->form_validation->set_message('passconf', 'Las contraseñas no coinciden');

 		$this->form_validation->set_message('is_unique', 'El mail ya pertenece a otro usuario');


 		/*muestra lo que sucede si hay o no errores*/

 		 if ($this->form_validation->run() == FALSE) { 
 			//en caso de ser falso, vuelve a cargar la página
            $this->index(); 
 
        } else {       
        	
        	$this->insertar_cliente();
        }; 
	}
}