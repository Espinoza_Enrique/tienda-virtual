
<?php
/*Permite manejar todo lo referentes a los clientes*/
defined('BASEPATH') OR exit('No direct script access allowed');

class ControlProductos extends CI_Controller {

	function __construct() {
		parent::__construct();
		//$this->load->model('ModeloProducto');
		//$this->load->library('pagination');
		/*restringe el acceso sólo a los usuarios registrados
		Debo agregar la lìnea if($this->session->userdata('login') en el cuerpo de cada función que debe realizarse sólo para usuarios registrados*/
		/*if(!$this->session->userdata('login')) {
			$this->index();
		};*/
	}

	/**/
	/*Muestra la vista inicial para ingresar el producto.*/
	public function index()
	{

		if($this->session->userdata('perfil')==1) {
			/*guardo las categorias de los productos*/
			$datos['categorias']=$this->ModeloProducto->get_categoria();
			$datos['subcategorias']=$this->ModeloProducto->get_subcategorias();
			$titulo = array('titulo' => 'Ingreso de Productos');
			$this->load->view('plantillas/encabezado',$titulo);
			$this->load->view('plantillas/barraNavegacionAdm');
			$this->load->view('contenido/formularioProductos',$datos);//paso dos arrays como datos a la vista
			$this->load->view('plantillas/pie');
		}else {
			redirect('inicio');
		};
	}

	/*muestra el formulario para modificar los productos*/
	public function formularioModificar() {
		if($this->session->userdata('perfil')==1) {
				$id=$this->input->post("modificar");
				$producto=$this->ModeloProducto->get_ProductoId($id);
				$datos['producto']=$producto;
				$datos['categorias']=$this->ModeloProducto->get_categoria();
				$datos['subcategorias']=$this->ModeloProducto->get_subcategorias();
				$titulo = array('titulo' => 'Modificar Productos');
				$this->load->view('plantillas/encabezado',$titulo);
				$this->load->view('plantillas/barraNavegacionAdm');
				$this->load->view('contenido/editarProductos',$datos);//paso dos arrays como datos a la vista
				$this->load->view('plantillas/pie');
			}else {
				redirect('inicio');
			};
	}

	/*Es la vista que muestra los errores que se han producido una vez presionado el botón de ingreso.
	En caso de no haber errores, mostrará que el producto se ingresó correctamente
	Posteriormente, se muestra la vista con los mensajes de error en caso de haber ocurrido alguno*/
	public function confirmarIngreso($error,$formulario,$id)
	{

		if($this->session->userdata('perfil')==1) {
			//$errores['error']=$error;
			$datos['categorias']=$this->ModeloProducto->get_categoria();
			$datos['subcategorias']=$this->ModeloProducto->get_subcategorias();
			/*guardo las categorias de los productos*/
			if($formulario==true) {
				$titulo = array('titulo' => 'Ingreso Productos');
				$this->load->view('plantillas/encabezado',$titulo);
				$this->load->view('plantillas/barraNavegacionAdm');
				//$this->load->view('contenido/ventanaModalError',$errores);
				$this->load->view('contenido/formularioProductos',$datos);
				$this->load->view('plantillas/pie');
				
			}else {
				$producto=$this->ModeloProducto->get_ProductoId($id);
				$datos['producto']=$producto;
				$datos['categorias']=$this->ModeloProducto->get_categoria();
				$datos['subcategorias']=$this->ModeloProducto->get_subcategorias();
				$titulo = array('titulo' => 'Modificar Productos');
				$this->load->view('plantillas/encabezado',$titulo);
				$this->load->view('plantillas/barraNavegacionAdm');
				//$this->load->view('contenido/ventanaModalError',$errores);
				$this->load->view('contenido/editarProductos',$datos);//paso dos arrays como datos a la vista
				$this->load->view('plantillas/pie');
				
			};
		
		} else {
			redirect('inicio');
		}
		
	}

	/*Muestra los datos de los productos al seleccionar sus catagorias en la barra de opciones.
	Dependiendo dónde se haga clic, se mostrará uno u otro producto.*/
	public function visualizarProductos($idCategoria,$criterioOrdenamiento) {
		$titulo="";
		$ordenadoPor=null;
		$datos['mostrarOrden']=null;
		$datos['categoria']=$idCategoria;
		$datos['criterio']=$criterioOrdenamiento;
		$categorias['categorias']=$this->ModeloProducto->get_categoria();
		$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
			$pagina=$this->uri->segment(1);
			$config['base_url']=base_url().'ListaProductos'."/".$idCategoria."/".$criterioOrdenamiento;
			$config['first_link']='Primero';
			$config['prev_link']='Anterior';
			$config['last_link']='Último';
			$config['next_link']='Siguiente';
			$config['full_tag_open']='<ul class="pagination justify-content-center">';
			$config['full_tag_close']='</ul>';
			$config['num_tag_open']='<li class="page-item page-link">';
			$config['num_tag_close']='</li>';
			$config['cur_tag_open']='<li class="active"><span class="page-link bg-primary text-white" >';
			$config['cur_tag_close']='<span ></span></span></li>';
			$config['prev_tag_open']='<li class="page-item page-link">';
			$config['prev_tag_close']='</li>';
			$config['next_tag_open']='<li class="page-item page-link">';
			$config['next_tag_close']='</li>';
			$config['last_tag_open']='<li class="page-item page-link">';
			$config['last_tag_close']='</li>';
			$config['first_tag_open']='<li class="page-item page-link">';
			$config['first_tag_close']='</li>';
			//echo $this->ModeloProducto->get_cantidad();die;
			$config['per_page']=8;
			$config['num_links']=2;
			$config['uri_segment']=4;
			
		switch($criterioOrdenamiento) {
			case 1:
				$ordenadoPor="Ordenado A-Z";
			break;
			case 2:
				$ordenadoPor="Ordenado Z-A";
			break;
			case 3:
				$ordenadoPor="Mayor A Menor";
			break;
			case 4:
				$ordenadoPor="Menor A Mayor";
			break;
			default:
				$ordenadoPor="Ordenar Por";
			break;
		}

		/*busco el título para que se muestre en la pantalla*/
		if ($this->session->userdata('perfil')==2 or (!$this->session->userdata('login'))) {
			foreach ($categorias['categorias'] as $cat) {
				foreach ($categorias['subcategorias'] as $sub) {
					if($sub->Id==$idCategoria and $sub->IdCategoria==$cat->id) {
						$titulo=array('titulo'=>$cat->nombreCat."-".$sub->nombreSub);
						break;
					}
				}
			}
			
			$datos['mostrarOrden']=$idCategoria;
			$config['total_rows']=$this->ModeloProducto->getCantidadSubcategoria($idCategoria);
			$this->pagination->initialize($config);
			$page=$this->uri->segment(4);//define cuántos elementos mostrará según el valor del segmento 2
			$datos['orden']=$ordenadoPor;
			$datos['productos']=$this->ModeloProducto->get_producto_categoria($idCategoria,$criterioOrdenamiento,8,$page);
			//$titulo = array('titulo' => 'Listado de Productos');
			$this->load->view('plantillas/encabezado',$titulo);
			/*cargo la vista inicial dependiendo el usuario*/
			switch($this->session->userdata('perfil')){
				case 1:
					$this->load->view('plantillas/barraNavegacionAdm');
				break;

				case 2:

					$this->load->view('plantillas/barraNavegacionRegistrado');
					$this->load->view('plantillas/barraOpciones',$categorias);
				break;
				default:
					$this->load->view('plantillas/barraNavegacion');
					$this->load->view('plantillas/barraOpciones',$categorias);
				break;

			};
			
			$this->load->view('plantillas/opcionesOrdenamiento',$datos);
			$this->load->view('contenido/vidrieraProductos',$datos);//paso dos arrays como datos a la vista
			$this->load->view('plantillas/pie');
		} else {
			redirect('inicio');
		}

	}

	/*Muestra los datos de los productos al seleccionar sus catagorias en la barra de opciones.
	Dependiendo dónde se haga clic, se mostrará uno u otro producto.*/
	public function visualizarProductosPalabraClave($palabraClave) {
		$titulo="";
		$ordenadoPor=null;
		$datos['mostrarOrden']=null;
		$cantidadResultados=$this->ModeloProducto->getCantidadPalabraClave($palabraClave);
		$categorias['categorias']=$this->ModeloProducto->get_categoria();
		$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
			$pagina=$this->uri->segment(1);
			$config['base_url']=base_url().'buscarPorPalabra'."/".$palabraClave;
			$config['first_link']='Primero';
			$config['prev_link']='Anterior';
			$config['last_link']='Último';
			$config['next_link']='Siguiente';
			$config['full_tag_open']='<ul class="pagination justify-content-center">';
			$config['full_tag_close']='</ul>';
			$config['num_tag_open']='<li class="page-item page-link">';
			$config['num_tag_close']='</li>';
			$config['cur_tag_open']='<li class="active"><span class="page-link bg-primary text-white" >';
			$config['cur_tag_close']='<span ></span></span></li>';
			$config['prev_tag_open']='<li class="page-item page-link">';
			$config['prev_tag_close']='</li>';
			$config['next_tag_open']='<li class="page-item page-link">';
			$config['next_tag_close']='</li>';
			$config['last_tag_open']='<li class="page-item page-link">';
			$config['last_tag_close']='</li>';
			$config['first_tag_open']='<li class="page-item page-link">';
			$config['first_tag_close']='</li>';
			//echo $this->ModeloProducto->get_cantidad();die;
			$config['per_page']=8;
			$config['num_links']=2;
			$config['uri_segment']=3;
			
		
			if ($this->session->userdata('perfil')==2 or (!$this->session->userdata('login'))) {
			$titulo=array("titulo"=>"Filtrar Productos");
			
			$config['total_rows']=$cantidadResultados;
			$this->pagination->initialize($config);
			$page=$this->uri->segment(3);//define cuántos elementos mostrará según el valor del segmento 2
			$datos['orden']=$ordenadoPor;
			$datos['productos']=$this->ModeloProducto->getProductosPorPalabra($palabraClave,8,$page);
			
			//$titulo = array('titulo' => 'Listado de Productos');
			$this->load->view('plantillas/encabezado',$titulo);
			/*cargo la vista inicial dependiendo el usuario*/
			switch($this->session->userdata('perfil')){
				case 1:
					$this->load->view('plantillas/barraNavegacionAdm');
				break;

				case 2:

					$this->load->view('plantillas/barraNavegacionRegistrado');
					$this->load->view('plantillas/barraOpciones',$categorias);
				break;
				default:
					$this->load->view('plantillas/barraNavegacion');
					$this->load->view('plantillas/barraOpciones',$categorias);
				break;

			};
			
			if ($cantidadResultados>0) {
				//$this->load->view('plantillas/opcionesOrdenamiento',$datos);
				$this->load->view('contenido/vidrieraProductos',$datos);//paso dos arrays como datos a la vista
			} else {
				$this->load->view('contenido/vistaNoHayProductos',array('clave'=>$palabraClave));//paso dos arrays como datos a la vista
			};
			$this->load->view('plantillas/pie');
		} else {
			redirect('inicio');
		}

	}

	public function buscarPalabraClave() {
		$palabra=$this->input->post('palabra');
		if(!empty($palabra)) {
			redirect('buscarPorPalabra'."/".$palabra);
		}else {
			redirect('inicio');
		}
	}
	

	/*permite mostrar los datos de los productos en una tabla*/
	public function mostrarProductos() {
		if($this->session->userdata('perfil')==1) {
			$categorias['categorias']=$this->ModeloProducto->get_categoria();
			$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
			//print_r($categorias);die;
			$pagina=$this->uri->segment(1);
			$config['base_url']=base_url().'mostrarProductos';
			$config['first_link']='Primero';
			$config['prev_link']='Anterior';
			$config['last_link']='Último';
			$config['next_link']='Siguiente';
			$config['full_tag_open']='<ul class="pagination justify-content-center">';
			$config['full_tag_close']='</ul>';
			$config['num_tag_open']='<li class="page-item page-link">';
			$config['num_tag_close']='</li>';
			$config['cur_tag_open']='<li class="active"><span class="page-link bg-primary text-white" >';
			$config['cur_tag_close']='<span ></span></span></li>';
			$config['prev_tag_open']='<li class="page-item page-link">';
			$config['prev_tag_close']='</li>';
			$config['next_tag_open']='<li class="page-item page-link">';
			$config['next_tag_close']='</li>';
			$config['last_tag_open']='<li class="page-item page-link">';
			$config['last_tag_close']='</li>';
			$config['first_tag_open']='<li class="page-item page-link">';
			$config['first_tag_close']='</li>';
			//echo $this->ModeloProducto->get_cantidad();die;
			$config['total_rows']=$this->ModeloProducto->getCantidadProductos();
			$config['per_page']=5;
			$config['num_links']=3;
			$config['uri_segment']=2;
			$this->pagination->initialize($config);
			$page=$this->uri->segment(2);//define cuántos elementos mostrará según el valor del segmento 2

			$datos['productos']=$this->ModeloProducto->get_productos(5,$page);
			$datos['paginacion']=$page;
			$datos['pagina']=$pagina;
			$titulo = array('titulo' => 'Listado de Productos');
			$this->load->view('plantillas/encabezado',$titulo);
			$this->load->view('plantillas/barraNavegacionAdm');
			$this->load->view('plantillas/filtroProductosAdm',$categorias);
			$this->load->view('contenido/tablaDeProductos',$datos);//paso dos arrays como datos a la vista
			$this->load->view('plantillas/pie');
		} else {
			redirect('inicio');
		}

	}

	/*muestra los productos segùn una categorìa*/
	public function mostrarCategoria() {
		if($this->session->userdata('perfil')==1) {
			$categorias['categorias']=$this->ModeloProducto->get_categoria();
			$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
			$categoria=$this->uri->segment(2);
			$cantidadProductosCat=$this->ModeloProducto->getCantidadCategoria($categoria);
			$config['base_url']=base_url().'productosCategoriaAdm'."/".$categoria;
			$config['first_link']='Primero';
			$config['prev_link']='Anterior';
			$config['last_link']='Último';
			$config['next_link']='Siguiente';
			$config['full_tag_open']='<ul class="pagination justify-content-center">';
			$config['full_tag_close']='</ul>';
			$config['num_tag_open']='<li class="page-item page-link">';
			$config['num_tag_close']='</li>';
			$config['cur_tag_open']='<li class="active"><span class="page-link bg-primary text-white" >';
			$config['cur_tag_close']='<span ></span></span></li>';
			$config['prev_tag_open']='<li class="page-item page-link">';
			$config['prev_tag_close']='</li>';
			$config['next_tag_open']='<li class="page-item page-link">';
			$config['next_tag_close']='</li>';
			$config['last_tag_open']='<li class="page-item page-link">';
			$config['last_tag_close']='</li>';
			$config['first_tag_open']='<li class="page-item page-link">';
			$config['first_tag_close']='</li>';
			$config['total_rows']=$cantidadProductosCat;
			$config['per_page']=5;
			$config['num_links']=2;
			$config['uri_segment']=3;
			$this->pagination->initialize($config);
			$page=$this->uri->segment(3);//define cuántos elementos mostrará según el valor del segmento 2
			$datos['productos']=$this->ModeloProducto->getCategoriaAdm($categoria,5,$page);
			$datos['paginacion']=$page;
			
			$titulo = array('titulo' => 'Listado de Productos');
			$this->load->view('plantillas/encabezado',$titulo);
			$this->load->view('plantillas/barraNavegacionAdm');
			$this->load->view('plantillas/filtroProductosAdm',$categorias);
			$this->load->view('contenido/tablaDeProductos',$datos);//paso dos arrays como datos a la vista
			$this->load->view('plantillas/pie');
		} else {
			redirect('inicio');
		}

	}

	/*muestra los productos segùn una subcategoria*/
	public function mostrarSubcategoria() {
		if($this->session->userdata('perfil')==1) {
			$categorias['categorias']=$this->ModeloProducto->get_categoria();
			$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
			$subcategoria=$this->uri->segment(2);
			$cantidadProductosCat=$this->ModeloProducto->getCantidadSubcategoria($subcategoria);
			$config['base_url']=base_url().'productosSubcategoriaAdm'."/".$subcategoria;
			$config['first_link']='Primero';
			$config['prev_link']='Anterior';
			$config['last_link']='Último';
			$config['next_link']='Siguiente';
			$config['full_tag_open']='<ul class="pagination justify-content-center">';
			$config['full_tag_close']='</ul>';
			$config['num_tag_open']='<li class="page-item page-link">';
			$config['num_tag_close']='</li>';
			$config['cur_tag_open']='<li class="active"><span class="page-link bg-primary text-white" >';
			$config['cur_tag_close']='<span ></span></span></li>';
			$config['prev_tag_open']='<li class="page-item page-link">';
			$config['prev_tag_close']='</li>';
			$config['next_tag_open']='<li class="page-item page-link">';
			$config['next_tag_close']='</li>';
			$config['last_tag_open']='<li class="page-item page-link">';
			$config['last_tag_close']='</li>';
			$config['first_tag_open']='<li class="page-item page-link">';
			$config['first_tag_close']='</li>';
			$config['total_rows']=$cantidadProductosCat;
			$config['per_page']=5;
			$config['num_links']=2;
			$config['uri_segment']=3;
			$this->pagination->initialize($config);
			$page=$this->uri->segment(3);//define cuántos elementos mostrará según el valor del segmento 2
			$datos['productos']=$this->ModeloProducto->getSubcategoriaAdm($subcategoria,5,$page);
			$datos['paginacion']=$page;
		
			$titulo = array('titulo' => 'Listado de Productos');
			$this->load->view('plantillas/encabezado',$titulo);
			$this->load->view('plantillas/barraNavegacionAdm');
			$this->load->view('plantillas/filtroProductosAdm',$categorias);
			$this->load->view('contenido/tablaDeProductos',$datos);//paso dos arrays como datos a la vista
			$this->load->view('plantillas/pie');
		} else {
			redirect('inicio');
		}

	}

		public function mostrarProductosMayor() {
		if($this->session->userdata('perfil')==1) {
			$categorias['categorias']=$this->ModeloProducto->get_categoria();
			$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
			$pagina=$this->uri->segment(1);
			$config['base_url']=base_url().'productosMayorMenor';
			$config['first_link']='Primero';
			$config['prev_link']='Anterior';
			$config['last_link']='Último';
			$config['next_link']='Siguiente';
			$config['full_tag_open']='<ul class="pagination justify-content-center">';
			$config['full_tag_close']='</ul>';
			$config['num_tag_open']='<li class="page-item page-link">';
			$config['num_tag_close']='</li>';
			$config['cur_tag_open']='<li class="active"><span class="page-link bg-primary text-white" >';
			$config['cur_tag_close']='<span ></span></span></li>';
			$config['prev_tag_open']='<li class="page-item page-link">';
			$config['prev_tag_close']='</li>';
			$config['next_tag_open']='<li class="page-item page-link">';
			$config['next_tag_close']='</li>';
			$config['last_tag_open']='<li class="page-item page-link">';
			$config['last_tag_close']='</li>';
			$config['first_tag_open']='<li class="page-item page-link">';
			$config['first_tag_close']='</li>';
			//echo $this->ModeloProducto->get_cantidad();die;
			$config['total_rows']=$this->ModeloProducto->getCantidadProductos();
			$config['per_page']=5;
			$config['num_links']=3;
			$config['uri_segment']=2;
			$this->pagination->initialize($config);
			$page=$this->uri->segment(2);//define cuántos elementos mostrará según el valor del segmento 2

			$datos['productos']=$this->ModeloProducto->getMayorAMenorAdm(5,$page);
			$datos['paginacion']=$page;
			$datos['pagina']=$page;
			$titulo = array('titulo' => 'Listado de Productos');
			$this->load->view('plantillas/encabezado',$titulo);
			$this->load->view('plantillas/barraNavegacionAdm');
			$this->load->view('plantillas/filtroProductosAdm',$categorias);
			$this->load->view('contenido/tablaDeProductos',$datos);//paso dos arrays como datos a la vista
			$this->load->view('plantillas/pie');
		} else {
			redirect('inicio');
		}

	}

		public function mostrarProductosMenor() {
		if($this->session->userdata('perfil')==1) {
			$categorias['categorias']=$this->ModeloProducto->get_categoria();
			$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
			$config['base_url']=base_url().'productosMenorMayor';
			$config['firts_link']='Primero';
			$config['prev_link']='Anterior';
			$config['last_link']='Último';
			$config['next_link']='Siguiente';
			$config['full_tag_open']='<ul class="pagination justify-content-center">';
			$config['full_tag_close']='</ul>';
			$config['num_tag_open']='<li class="page-item page-link">';
			$config['num_tag_close']='</li>';
			$config['cur_tag_open']='<li class="active"><span class="page-link bg-primary text-white" >';
			$config['cur_tag_close']='<span ></span></span></li>';
			$config['prev_tag_open']='<li class="page-item page-link">';
			$config['prev_tag_close']='</li>';
			$config['next_tag_open']='<li class="page-item page-link">';
			$config['next_tag_close']='</li>';
			$config['last_tag_open']='<li class="page-item page-link">';
			$config['last_tag_close']='</li>';
			$config['first_tag_open']='<li class="page-item page-link">';
			$config['first_tag_close']='</li>';
			//echo $this->ModeloProducto->get_cantidad();die;
			$config['total_rows']=$this->ModeloProducto->getCantidadProductos();
			$config['per_page']=5;
			$config['num_links']=3;
			$config['uri_segment']=2;
			$this->pagination->initialize($config);
			$page=$this->uri->segment(2);//define cuántos elementos mostrará según el valor del segmento 2

			$datos['productos']=$this->ModeloProducto->getMenorAMayorAdm(5,$page);
			$datos['paginacion']=$page;
			$titulo = array('titulo' => 'Listado de Productos');
			$this->load->view('plantillas/encabezado',$titulo);
			$this->load->view('plantillas/barraNavegacionAdm');
			$this->load->view('plantillas/filtroProductosAdm',$categorias);
			$this->load->view('contenido/tablaDeProductos',$datos);//paso dos arrays como datos a la vista
			$this->load->view('plantillas/pie');
		} else {
			redirect('inicio');
		}

	}

	public function mostrarFechasMenor() {
		if($this->session->userdata('perfil')==1) {
			$categorias['categorias']=$this->ModeloProducto->get_categoria();
			$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
			$config['base_url']=base_url().'productosFechaMenor';
			$config['firts_link']='Primero';
			$config['prev_link']='Anterior';
			$config['last_link']='Último';
			$config['next_link']='Siguiente';
			$config['full_tag_open']='<ul class="pagination justify-content-center">';
			$config['full_tag_close']='</ul>';
			$config['num_tag_open']='<li class="page-item page-link">';
			$config['num_tag_close']='</li>';
			$config['cur_tag_open']='<li class="active"><span class="page-link bg-primary text-white" >';
			$config['cur_tag_close']='<span ></span></span></li>';
			$config['prev_tag_open']='<li class="page-item page-link">';
			$config['prev_tag_close']='</li>';
			$config['next_tag_open']='<li class="page-item page-link">';
			$config['next_tag_close']='</li>';
			$config['last_tag_open']='<li class="page-item page-link">';
			$config['last_tag_close']='</li>';
			$config['first_tag_open']='<li class="page-item page-link">';
			$config['first_tag_close']='</li>';
			//echo $this->ModeloProducto->get_cantidad();die;
			$config['total_rows']=$this->ModeloProducto->getCantidadProductos();
			$config['per_page']=5;
			$config['num_links']=3;
			$config['uri_segment']=2;
			$this->pagination->initialize($config);
			$page=$this->uri->segment(2);//define cuántos elementos mostrará según el valor del segmento 2

			$datos['productos']=$this->ModeloProducto->getFechaMenorAdm(5,$page);
			$datos['paginacion']=$page;
			$titulo = array('titulo' => 'Listado de Productos');
			$this->load->view('plantillas/encabezado',$titulo);
			$this->load->view('plantillas/barraNavegacionAdm');
			$this->load->view('plantillas/filtroProductosAdm',$categorias);
			$this->load->view('contenido/tablaDeProductos',$datos);//paso dos arrays como datos a la vista
			$this->load->view('plantillas/pie');
		} else {
			redirect('inicio');
		}

	}

	public function mostrarFechasMayor() {
		if($this->session->userdata('perfil')==1) {
			$categorias['categorias']=$this->ModeloProducto->get_categoria();
			$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
			$config['base_url']=base_url().'productosFechaMayor';
			$config['firts_link']='Primero';
			$config['prev_link']='Anterior';
			$config['last_link']='Último';
			$config['next_link']='Siguiente';
			$config['full_tag_open']='<ul class="pagination justify-content-center">';
			$config['full_tag_close']='</ul>';
			$config['num_tag_open']='<li class="page-item page-link">';
			$config['num_tag_close']='</li>';
			$config['cur_tag_open']='<li class="active"><span class="page-link bg-primary text-white" >';
			$config['cur_tag_close']='<span ></span></span></li>';
			$config['prev_tag_open']='<li class="page-item page-link">';
			$config['prev_tag_close']='</li>';
			$config['next_tag_open']='<li class="page-item page-link">';
			$config['next_tag_close']='</li>';
			$config['last_tag_open']='<li class="page-item page-link">';
			$config['last_tag_close']='</li>';
			$config['first_tag_open']='<li class="page-item page-link">';
			$config['first_tag_close']='</li>';
			//echo $this->ModeloProducto->get_cantidad();die;
			$config['total_rows']=$this->ModeloProducto->getCantidadProductos();
			$config['per_page']=5;
			$config['num_links']=3;
			$config['uri_segment']=2;
			$this->pagination->initialize($config);
			$page=$this->uri->segment(2);//define cuántos elementos mostrará según el valor del segmento 2

			$datos['productos']=$this->ModeloProducto->getFechaMayorAdm(5,$page);
			$datos['paginacion']=$page;
			$titulo = array('titulo' => 'Listado de Productos');
			$this->load->view('plantillas/encabezado',$titulo);
			$this->load->view('plantillas/barraNavegacionAdm');
			$this->load->view('plantillas/filtroProductosAdm',$categorias);
			$this->load->view('contenido/tablaDeProductos',$datos);//paso dos arrays como datos a la vista
			$this->load->view('plantillas/pie');
		} else {
			redirect('inicio');
		}

	}


	/*valida que una categoria haya sido seleccionada*/
	 function validar_categoria($categoria) {
	 	if($this->session->userdata('perfil')==1) {
			if ($categoria==0) {
				/*genero el mensaje*/
				$this->form_validation->set_message ('validar_categoria','Seleccione una categoria');
				return false;/*retorno falso para que vuelva a mostrar el formulario para cargar los datos correctos*/
			} 
			else {
				return true;/*indica que se seleccionó una categoría*/
			}
		} else {
			redirect('inicio');
		}
	}

	/*valida que una imagen haya sido seleccionada*/
	 function validar_imagen() {
	 	$tipoArchivo=$_FILES['imagen']['type'];
		$tamañoArchivo=$_FILES['imagen']['size'];
	 	
	 	if($this->session->userdata('perfil')==1) {
			$nombre_imagen=$_FILES['imagen']['name'];//elijo el nombre de la imagen
			if(empty($nombre_imagen) or (strstr($tipoArchivo,"jpeg")!="jpeg" and strstr($tipoArchivo,"png")!="png") or $tamañoArchivo>5000000) {
				return false;
			} 
			else {
				return true;
			}
		}else {
			redirect('inicio');
		}
	}

	/*valida que una imagen haya sido seleccionada*/
	 function validarImagenEditada() {
	 	$tipoArchivo=$_FILES['imagen']['type'];
		$tamañoArchivo=$_FILES['imagen']['size'];
		$nombre_imagen=$_FILES['imagen']['name'];//elijo el nombre de la imagen

	 	if($this->session->userdata('perfil')==1) {
			$producto=$this->ModeloProducto->get_ProductoId($this->input->post('id'));
			//reviso que el archivo sea una foto válida y que no se elija la misma imagen existente
			//caso contrario, si está vacía, no se cambia
			if(empty($nombre_imagen) or ((strstr($tipoArchivo,"jpeg")=="jpeg" or strstr($tipoArchivo,"png")=="png") and $tamañoArchivo<=5000000)) {
				
				return true;
			} 
			else {
				$this->form_validation->set_message('validarImagenEditada', 'La foto debe ser jpg o png de hasta 5MB'); 
				return false;
			}
		}else {
			redirect('inicio');
		}
	}

	/*calida que una imagen haya sido seleccionada*/
	 function validarImagenVacia() {
		$nombre_imagen=$_FILES['imagen']['name'];//elijo el nombre de la imagen

	 	if($this->session->userdata('perfil')==1) {
			$producto=$this->ModeloProducto->get_ProductoId($this->input->post('id'));
			//reviso que el archivo sea una foto válida y que no se elija la misma imagen existente
			//caso contrario, si está vacía, no se cambia
			if(!empty($nombre_imagen) and ($producto->nombreFoto==$nombre_imagen)){
				$this->form_validation->set_message('validarImagenVacia', 'No elija la misma foto del producto. Deje este campo en blanco para no modificarla.');
				return false;
			} 
			else {
				return true;
			};
		}else {
			redirect('inicio');
		}
	}

	/*Permite validar los datos del formulario de ingreso para un nuevo producto*/
	public function registrarProductos()
	{
		if($this->session->userdata('perfil')==1) {
			/*muestra los criterios de control para cada campo*/
			$this->form_validation->set_rules('nombre', 'Nombre del producto', 'required|is_unique[productos.nombre_producto]');        
			$this->form_validation->set_rules('precio', 'Precio del producto', 'required|callback_controlarNegativosYCero');       
	 		$this->form_validation->set_rules('descripcion', 'Descripcion del producto', 'required');      
	 		$this->form_validation->set_rules('stockMinimo', 'Stock Minimo','required|integer|callback_controlarNegativosYCero');
	 		$this->form_validation->set_rules('stock', 'Stock','required|integer|callback_controlarNegativosYCero');
	 		$this->form_validation->set_rules('garantia', 'Garantia','required|integer|callback_controlarNegativosYCero');
	 		$this->form_validation->set_rules('categoria','Seleccionar una categoria','required|callback_validar_categoria|callback_controlarNegativosYCero');
			$this->form_validation->set_rules('imagen','Seleccionar Imagen', 'callback_validar_imagen');

	 
			$this->form_validation->set_message('integer', 'El campo %s debe poseer solo numeros enteros'); 

			$this->form_validation->set_message('validar_categoria', 'Elija una categoria por favor.');

			$this->form_validation->set_message('validar_imagen', 'Error. La foto debe ser png o jpg de hasta 5MB.');

			$this->form_validation->set_message('decimal', 'El campo %s debe poseer solo numeros'); 
	 
			$this->form_validation->set_message('required', 'El campo %s es obligatorio'); 

			$this->form_validation->set_message('controlarNegativosYCero', 'El campo %s debe ser mayor a cero');
	 
			$this->form_validation->set_message('max_length', 'El campo %s debe contener como máximo %d caracteres'); 

	 		$this->form_validation->set_message('is_unique', 'El nombre del producto ya existe');


	 		/*muestra lo que sucede si hay o no errores*/

	 		 if ($this->form_validation->run() == FALSE) { 
	 			//en caso de ser falso, vuelve a cargar la página
	 		
	 			$this->confirmarIngreso("Error al ingresar los datos. Por favor, revise los mensajes que aparecen dejabo de cada campo solicitado.",true,0);
	 			
	        } else {       
	        	
	        	$this->insertar_producto();
	        }; 
	    } else {
	    	redirect('inicio');
	    }
	}

	

	
	/*permite insertar un producto a la base de datos mediante los datos del formulario*/
	public function insertar_producto() {
		if($this->session->userdata('perfil')==1) {
			
			
			/*$config['upload_path']='./uploads/';//dónde se moverá la imagen
			$config['allowed_type']='jpg|png';//tipos de imagenes permitidos
			$config['remove_spaces']=TRUE;//quita los espacios en blanco
			$config['max_size']='500000';//el tamaño máximo de la imagen
			$this->load->library('upload',$config);//carga la configuración*/

			//permite la carga del producto
			$nombre = $_FILES['imagen']['name'];
			$nombre = strtolower($nombre);
			$cd=$_FILES['imagen']['tmp_name'];
			$ruta = "uploads/"."imagenes" ."/". $nombre;
			$resultado = @move_uploaded_file($_FILES["imagen"]["tmp_name"], $ruta);
			$tipoArchivo=$_FILES['imagen']['type'];
			$tamañoArchivo=$_FILES['imagen']['size'];
			//pregunta si se copió, es de tipo jpg o png y si pesa menos o igual a 100MB
			if (empty($resultado) or (strstr($tipoArchivo,"jpeg")!="jpeg" and strstr($tipoArchivo,"png")!="png") or $tamañoArchivo>5000000) {
				
				$this->form_validation->set_message('validar_imagen', 'Seleccionar una imagen con formato JPG o PNG');
				//aquí llamo nuevamente a la función que muestra la vista para cargar el producto
				$this->index();
			} else {
					$producto=array (
					/*Almaceno en los campos de la tabla los datos en los controles. En el método post coloco el name del control.*/
					/*reemplazo los guiones bajos por espacios*/
					'nombre_producto'=>str_replace("_"," ", $this->input->post('nombre')),
					'precio'=>$this->input->post('precio'),
					'fecha'=>date("Y-m-d"),
					'subcategoria'=>$this->input->post('categoria'),
					'nombreFoto'=>$_FILES['imagen']['name'],
					'stock'=>$this->input->post('stock'),
					'stockMinimo'=>$this->input->post('stockMinimo'),
					'estado'=>1,
					'descripcion'=>$this->input->post('descripcion'),
					'garantia'=>$this->input->post('garantia')
					);

				$this->ModeloProducto->guardar_producto($producto);
				$this->confirmarIngreso("Datos Ingresados correctamente. Volviendo a la página para dar de alta los productos",true,0);
			}
		}else {
			redirect('inicio');
		};//termina el if
	}

	

	/*muestra la descripcion de los productos*/
	public function mostrarDescripcion() {
		$id=$this->uri->segment(2);
		//$id=$this->input->post("detalles");
		$datos['producto']=$this->ModeloProducto->buscarDetalles($id);
		$categorias['categorias']=$this->ModeloProducto->get_categoria();
		$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
		if($this->session->userdata('perfil')==2 ){
				$titulo = array('titulo' => 'Detalles Producto');
				$this->load->view('plantillas/encabezado',$titulo);
				$this->load->view('plantillas/barraNavegacionRegistrado');
				$this->load->view('plantillas/barraOpciones',$categorias);
				$this->load->view('plantillas/ventanaModalDetalles',$datos);
				$this->load->view('plantillas/pie');
		}
		elseif(!$this->session->userdata('login')) {
				$titulo = array('titulo' => 'Detalles Producto');
				$this->load->view('plantillas/encabezado',$titulo);
				$this->load->view('plantillas/barraNavegacion');
				$this->load->view('plantillas/barraOpciones',$categorias);
				$this->load->view('plantillas/ventanaModalDetalles',$datos);
				$this->load->view('plantillas/pie');
	
		}elseif($this->session->userdata('perfil')==1) {
			redirect('inicio');
		}
	
	}

	


	
	/**/
	public function activarProducto() {
		if ($this->session->userdata('perfil')==1) {
			$id=$this->input->post('estado');
			$this->ModeloProducto->cambiarEstado($id,1);
			redirect('mostrarProductos');
		}else {
			redirect('inicio');
		}
	}

	/**/
	public function desactivarProducto() {
		if($this->session->userdata('perfil')==1) {
			$id=$this->input->post('estado');
			$this->ModeloProducto->cambiarEstado($id,0);
			redirect('mostrarProductos');
		}else {
			redirect('inicio');
		}
	}

	/*controla que no se hayan colocado valores negativos o cero*/
	public function controlarNegativosYCero($dato) {
		if($dato<=0) {
			return false;
		} else {
			return true;//no es negativo o cero, es un dato válido
		}
	}

	/*Permite validar los datos del formulario de ingreso para un nuevo producto*/
	public function controlarModificacion()
	{
		if($this->session->userdata('perfil')==1) {

			$id=$this->input->post('id');
			/*muestra los criterios de control para cada campo*/
			$this->form_validation->set_rules('nombre', 'Nombre del producto', 'required');        
			$this->form_validation->set_rules('precio', 'Precio del producto', 'required|callback_controlarNegativosYCero');       
	 		$this->form_validation->set_rules('descripcion', 'Descripcion del producto', 'required');      
	 		$this->form_validation->set_rules('stockMinimo', 'Stock Minimo','required|integer|callback_controlarNegativosYCero');
	 		$this->form_validation->set_rules('stock', 'Stock','required|integer|callback_controlarNegativosYCero');
	 		$this->form_validation->set_rules('garantia', 'Garantia','required|integer|callback_controlarNegativosYCero');
	 		$this->form_validation->set_rules('imagen','Modificar Imagen', 'callback_validarImagenEditada|callback_validarImagenVacia');
	 		$this->form_validation->set_rules('categoria','Seleccionar una categoria','required|callback_validar_categoria');

			$this->form_validation->set_message('integer', 'El campo %s debe poseer solo numeros enteros'); 
			
			$this->form_validation->set_message('validar_categoria', 'Elija una categoria por favor'); 
			$this->form_validation->set_message('imagen',"La foto ya existe");
			$this->form_validation->set_message('decimal', 'El campo %s debe poseer solo numeros'); 
	 		$this->form_validation->set_message('controlarNegativosYCero', 'El campo %s debe ser mayor a cero');
			$this->form_validation->set_message('required', 'El campo %s es obligatorio'); 
	 
			$this->form_validation->set_message('max_length', 'El campo %s debe contener como máximo %d caracteres'); 

	 		$this->form_validation->set_message('is_unique', 'El nombre del producto ya existe');


	 		/*muestra lo que sucede si hay o no errores*/

	 		 if ($this->form_validation->run() == FALSE) { 
	 			//en caso de ser falso, vuelve a cargar la página
	 			
	 			$this->confirmarIngreso(true,false,$id);
	 
	        } else {       
	        	
	        	$this->modificarProducto($id);
	        }; 
	    }else {
	    	redirect('inicio');
	    }
	}

	

	/*ingresa efectivamente la foto modificada*/
	public function modificarFoto($id) {
		if($this->session->userdata('perfil')==1) {
			
			//permite la carga del producto
			$nombre = $_FILES['imagen']['name'];
			$nombrer = strtolower($nombre);
			$cd=$_FILES['imagen']['tmp_name'];
			$ruta  = "uploads/"."imagenes" ."/". $nombre;;
			$resultado = @move_uploaded_file($_FILES["imagen"]["tmp_name"], $ruta);
			$archivoJpeg=strstr($_FILES['imagen']['type'],"jpeg");
			$archivoPng=strstr($_FILES['imagen']['type'],"png");
			$tamañoArchivo=$_FILES['imagen']['size'];
			
			//pregunta si se copió, es de tipo jpg o png y si pesa menos o igual a 100MB
			if (!empty($resultado) and ($archivoJpeg=="jpeg" or $archivoPng=="png") and $tamañoArchivo<=500000) {
				
				$this->ModeloProducto->modificarFoto($id,$_FILES['imagen']['name']);
				
			} else {
				
				//aquí llamo nuevamente a la función que muestra la vista para cargar el producto
				$this->confirmarIngreso(true,false,$id);
				
			}
		} else {
			redirect('inicio');
		}
	}
	/*permite insertar un producto a la base de datos mediante los datos del formulario*/
	public function modificarDatos($id) {
		/*permito que sólo el administrador use este método*/
		$idProducto=$id;
		if($this->session->userdata('perfil')==1) {

					$producto=array (
					/*Almaceno en los campos de la tabla los datos en los controles. En el método post coloco el name del control.*/
					'nombre_producto'=>$this->input->post('nombre'),
					'precio'=>$this->input->post('precio'),
					'subcategoria'=>$this->input->post('categoria'),
					'stock'=>$this->input->post('stock'),
					'stockMinimo'=>$this->input->post('stockMinimo'),
					'descripcion'=>$this->input->post('descripcion'),
					'garantia'=>$this->input->post('garantia')
					);

				$this->ModeloProducto->modificarProducto($producto,$idProducto);
				
			
		}else {
			redirect('inicio');
		};//termina el if
	}

	public function modificarProducto($id) {
		$nombre = $_FILES['imagen']['name'];
		
		if($this->session->userdata('perfil')==1) {
			if(empty($nombre)) {
				$this->modificarDatos($id);

			}else {
				$this->modificarDatos($id);
				$this->modificarFoto($id);
			};
			redirect('mostrarProductos');
		}else {
			redirect('inicio');
		}
	} 

	/*muestra la descripcion del producto*/
	public function getDescripcion($id){
		$producto=$this->ModeloProducto->get_ProductoId($id);
		return $producto->descripcion;
	}

	/*mostar lista de compras*/
	public function mostrarCompras() {
		if($this->sesssion->userdata('perfil')==2) {

		}else {
			redirect('inicio');
		}
	}

	/*ordena los productos según para que se muestren alfabéticamente o por precio*/
	public function ordenarProductos() {
		$categoria=null;
		$criterioDeOrden=null;
		$criterioDeOrden=$this->uri->segment(3);
		$categoria=$this->uri->segment(2);
		
		
		$this->visualizarProductos($categoria,$criterioDeOrden);
	
	}

	/*muestra la descripcion del producto para el administrador*/
	public function mostrarDescripcionAdm() {
		$id=$this->uri->segment(2);
		$paginacion=$this->uri->segment(3);
		$pagina=$this->uri->segment(4);

		$producto['producto']=$this->ModeloProducto->get_ProductoId($id);
		$producto['paginacion']=$paginacion;
		$producto['pagina']=$pagina;
		//echo $producto['pagina'];die;
		$titulo = array('titulo' => 'Descripcion de Producto');
		$this->load->view('plantillas/encabezado',$titulo);
		$this->load->view('plantillas/barraNavegacionAdm');
		$this->load->view('contenido/descripcionProductoAdm',$producto);//paso dos arrays como datos a la vista
		$this->load->view('plantillas/pie');
	}

}