<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ControlConsultaAdm extends CI_Controller {

	function __construct() {
		parent::__construct();
		/*$this->load->model('ModeloConsulta');*/
		if(!$this->session->userdata('login') or ($this->session->userdata('perfil')==2) ) {
				redirect('inicio');
			}
	}

	public function mostrarConsultas() {
		$data['consultas']=$this->ModeloConsulta->getConsultas();
		$titulo = array('titulo' => 'Lista Consultas');
		$this->load->view('plantillas/encabezado',$titulo);
		$this->load->view('plantillas/barraNavegacionAdm');
		$this->load->view('contenido/vistaConsultas',$data);
		$this->load->view('plantillas/pieAdm');
	}

	public function mostrarDetallesConsulta() {
		$id=$this->uri->segment(2);
		//$id=$this->input->post('idConsulta');
		$consulta=$this->ModeloConsulta->getConsultaId($id);
		$datos['detalle']=$consulta;
		$datos['idConsulta']=$id;
		$datos['leido']=$consulta->leido;
		$titulo = array('titulo' => 'Detalles Consulta');
		$this->load->view('plantillas/encabezado',$titulo);
		$this->load->view('plantillas/barraNavegacionAdm');
		$this->load->view('contenido/vistaDetallesConsultas',$datos);
		$this->load->view('plantillas/pieAdm');
	}

	public function marcarLeido($id) {
		$this->ModeloConsulta->marcarComoLeido($id);
		$this->mostrarConsultas();
	}

}
