<?php 
class ControlConsulta extends CI_Controller {

	function __construct() {
		parent::__construct();
		/*$this->load->model('ModeloConsulta');
		$this->load->model('ModeloProducto');*/
		if($this->session->userdata('perfil')==1) {
				redirect('inicio');
			}
	}

	/*
	 *Muestra el formulario para contactos y consultas
	 */
	public function cargarFormularioContacto()
	{
		$data['motivos']=$this->ModeloConsulta->getMotivos();
		$titulo = array('titulo' => 'Contacto y Consultas');
		$categorias['categorias']=$this->ModeloProducto->get_categoria();
		$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
		if($this->session->userdata('perfil')==2){
			$this->load->view('plantillas/encabezado',$titulo);
			$this->load->view('plantillas/barraNavegacionRegistrado');
			$this->load->view('plantillas/barraOpciones',$categorias);
			$this->load->view('contenido/contacto',$data);
			$this->load->view('plantillas/pie');
		}else {
			$this->load->view('plantillas/encabezado',$titulo);
			$this->load->view('plantillas/barraNavegacion');
			$this->load->view('plantillas/barraOpciones',$categorias);
			$this->load->view('contenido/contacto',$data);
			$this->load->view('plantillas/pie');
		};
		
		
		
	}



	/*Permite validar los datos requeridos para el formulario de consulta*/
	public function controlarConsultas()
	{
		//$this->form_validation->set_rules('apellido','Apellido del usuario', 'required');
		$this->form_validation->set_rules('nombre', 'Nombre del usuario', 'required');           
		$this->form_validation->set_rules('mail', 'Email', 'required|valid_email');      
		$this->form_validation->set_rules('motivo', 'Motivo consulta', 'required|callback_validar_motivo'); 
 		$this->form_validation->set_rules('consulta', 'Consulta', 'required');      
 		/*$this->form_validation->set_rules('passconf', 'Confirmar password', 'trim|required|matches[password]');*/

 		$this->form_validation->set_message('valid_email', 'El campo %s debe ser un mail válido'); 
 
		//$this->form_validation->set_message('integer', 'El campo %s debe poseer solo numeros enteros'); 
 
		$this->form_validation->set_message('required', 'El campo %s es obligatorio'); 
 
		/*$this->form_validation->set_message('min_length', 'El campo %s debe contener como mínimo %d caracteres'); 

 		$this->form_validation->set_message('matches', 'contraseñas no coinciden');*/

 		 if ($this->form_validation->run() == FALSE) { 
 
              $this->cargarFormularioContacto(); 
 
        } else {            
        	$this->insertarConsulta();
        }; 
	}

	/*inserta la consulta en la tabla de consultas*/
	public function insertarConsulta() {
		/*capturamos los datos del formulario en un arreglo*/
		$consulta=array (
			/*Almaceno en los campos de la tabla los datos en los controles. En el método post coloco el name del control.*/
			'nombre'=>$this->input->post('nombre'),
			'mail'=>$this->input->post('mail'),
			'fecha'=>date("Y-m-d"),
			'motivo'=>$this->input->post('motivo'),
			'consulta'=>$this->input->post('consulta'),
			'leido'=>0//indica que es un cliente
		);
		$this->ModeloConsulta->guardarConsulta($consulta);
		/*Se carga los datos del cliente y los datos del usuario. Para completar el campo IdPersona de la tabla usuario se debe capturar el último id que hace referencia al último registro de la tabla que tiene los datos del cliente*/
		
		redirect('contactoyconsultas');
	}


	
	
	/*valida que una categoria haya sido seleccionada*/
	 function validar_motivo($motivo) {
	 	
			if ($motivo==0) {
				/*genero el mensaje*/
				$this->form_validation->set_message ('validar_motivo','Seleccione un motivo para su consulta');
				return false;/*retorno falso para que vuelva a mostrar el formulario para cargar los datos correctos*/
			} 
			else {
				return true;/*indica que se seleccionó una categoría*/
			}
		
	}

	
}

	
