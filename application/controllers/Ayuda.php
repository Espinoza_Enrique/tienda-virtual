<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ayuda extends CI_Controller {


	function __construct() {
		parent::__construct();
		/*$this->load->model('ModeloProducto');*/
		if($this->session->userdata('perfil')==1 ) {
				redirect('inicio');
			}
	}
	/**
	 *Controlador para acceder a la vista con los juegos de mesa categoría abstracto
	 */
	public function index()
	{
		$data = array('titulo' => 'Ayuda');
		$categorias['categorias']=$this->ModeloProducto->get_categoria();
		$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
		if($this->session->userdata('perfil')==2) {
			$this->load->view('plantillas/encabezado',$data);
			$this->load->view('plantillas/barraNavegacionRegistrado');
			$this->load->view('plantillas/barraOpciones',$categorias);
			$this->load->view('contenido/paginaAyuda');
			$this->load->view('plantillas/pie');
		}else {
			$this->load->view('plantillas/encabezado',$data);
			$this->load->view('plantillas/barraNavegacion');
			$this->load->view('plantillas/barraOpciones',$categorias);
			$this->load->view('contenido/paginaAyuda');
			$this->load->view('plantillas/pie');
		}
		
	}
	
	/*muestra las diversas políticas de la empresa y términos*/
	public function mostrarTerminosYPoliticas()
	{
		$categorias['categorias']=$this->ModeloProducto->get_categoria();
		$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
		$data = array('titulo' => "Terminos de Uso");
		if($this->session->userdata('perfil')==2) {
			$this->load->view('plantillas/encabezado',$data);
			$this->load->view('plantillas/barraNavegacionRegistrado');
			$this->load->view('plantillas/barraOpciones',$categorias);
			$this->load->view('contenido/terminosYUso');
			$this->load->view('plantillas/pie');
		}else {
			$this->load->view('plantillas/encabezado',$data);
			$this->load->view('plantillas/barraNavegacion');
			$this->load->view('plantillas/barraOpciones',$categorias);
			$this->load->view('contenido/terminosYUso');
			$this->load->view('plantillas/pie');

		}
		
	}
}