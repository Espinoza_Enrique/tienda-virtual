<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
		function __construct() {
		parent::__construct();
		//$this->load->model('ModeloProducto');
		/*restringe el acceso sólo a los usuarios registrados
		Debo agregar la lìnea if($this->session->userdata('login') en el cuerpo de cada función que debe realizarse sólo para usuarios registrados*/

		
	}

	public function index()
	{
		$productos['destacados']=$this->ModeloProducto->getProductosDestacados();
		$productos['nuevos']=$this->ModeloProducto->getProductosNuevos();
		$categorias['categorias']=$this->ModeloProducto->get_categoria();
		$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
		if(!$this->session->userdata('login')) {
			
			
			//print_r($categorias);die;
			$data = array('titulo' => 'El Calabozo Del Androide');
			$this->load->view('plantillas/encabezado',$data);
			$this->load->view('plantillas/barraNavegacion');
			$this->load->view('plantillas/barraOpciones',$categorias);
			$this->load->view('contenido/inicio',$productos);
			$this->load->view('plantillas/pie');
		
		}else {
			$logueo=$this->session->userdata('perfil');
			//echo $logueo;die;
			switch ($logueo) {
				case 1:
					$this->visualizarAdministrador();
					break;
				case 2:
					$this->visualizarUsuarioLogueado();
					break;
			}
		}
		
	}

	 function visualizarUsuarioLogueado() {
		$productos['destacados']=$this->ModeloProducto->getProductosDestacados();
		$productos['nuevos']=$this->ModeloProducto->getProductosNuevos();
		$categorias['categorias']=$this->ModeloProducto->get_categoria();
		$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
		$data = array('titulo' => 'El Calabozo Del Androide');
		$this->load->view('plantillas/encabezado',$data);
		$this->load->view('plantillas/barraNavegacionRegistrado');
		$this->load->view('plantillas/barraOpciones',$categorias);
		$this->load->view('contenido/inicio',$productos);
		$this->load->view('plantillas/pie');
	}

	 function visualizarAdministrador() {
		redirect('mostrarProductos');
		
	}

	

}
