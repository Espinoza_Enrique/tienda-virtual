<?php
/*Permite manejar todo lo referentes a los formularios*/
defined('BASEPATH') OR exit('No direct script access allowed');

class ControlUsuario extends CI_Controller {

	function __construct() {
		parent::__construct();
		/*
		$this->load->model('ModeloUsuario');
		$this->load->model('ModeloProducto');*/
	}
	/**
	 *Muestra el formulario para realizar loggin
	 */
	public function cargarFormularioLoggin()
	{
			$logueo=$this->session->userdata('perfil');
			
			$titulo = array('titulo' => 'Loggin');
			$this->load->view('plantillas/encabezado',$titulo);
			$categorias['categorias']=$this->ModeloProducto->get_categoria();
			$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
			
			if(empty($logueo)){
				$this->load->view('plantillas/barraNavegacion');
				$this->load->view('plantillas/barraOpciones',$categorias);
				$this->load->view('contenido/formularioLoggin');
				$this->load->view('plantillas/pie');
			} else{
				switch($logueo){
					case 1:
						redirect('mostrarProductos');
					break;

					case 2:
						redirect('usuarioLogueado');
					break;
				

				};
			};
			
			
		
	}



	
	/*Permite validar los datos requeridos para que el usuario inicie sesión*/
	public function loggin_usuario()
	{
		/*$this->form_validation->set_rules('apellido','Apellido del usuario', 'required');
		$this->form_validation->set_rules('nombre', 'Nombre del usuario', 'required');*/            
		$this->form_validation->set_rules('mail', 'Email', 'required|valid_email');      
		//$this->form_validation->set_rules('telefono', 'Telefono del usuario', 'required|integer'); 
 		$this->form_validation->set_rules('password', 'Password', 'required|callback_verificarPassword');      
 		/*$this->form_validation->set_rules('passconf', 'Confirmar password', 'trim|required|matches[password]');*/

 		$this->form_validation->set_message('valid_email', 'El campo %s debe ser un mail válido'); 
 		
		//$this->form_validation->set_message('integer', 'El campo %s debe poseer solo numeros enteros'); 
 
		$this->form_validation->set_message('required', 'El campo %s es obligatorio'); 
 
		$this->form_validation->set_message('min_length', 'El campo %s debe contener como mínimo %d caracteres'); 

 		/*$this->form_validation->set_message('matches', 'contraseñas no coinciden');*/

 		 if ($this->form_validation->run() == FALSE) { 
 			
            $this->cargarFormularioLoggin(); 
 
        } else {
        	           
        	$this->usuarioLogueado();
        }; 
	}

	/*permite verificar el password*/
	function verificarPassword() {
		$usuario=$this->input->post('mail');
		$pass=$this->input->post('password');
		$contrasenia=base64_encode($pass);
		$usuario=$this->ModeloUsuario->buscarUsuario($usuario,$contrasenia);
		if($usuario) {
			$personaId=$usuario->IdPersona;
			$persona=$this->ModeloUsuario->buscarPersona($personaId);
			$datosUsuario= array('idUsuario'=>$persona->Id,'nombre'=>$persona->nombre,'apellido'=>$persona->apellido,'perfil'=>$persona->IdPerfil,'login'=>true);
			$this->session->set_userdata($datosUsuario);
			

		} else {
			$this->form_validation->set_message('verificarPassword','Usuario o contraseña inválido');
			return false;
		};
	}

	function usuarioLogueado() {
		/*verifico que haya un usuario logueado*/
		if ($this->session->userdata('login')) {
			/*verifico cual sesión está activa*/
			switch($this->session->userdata('perfil')) {
				case 1:
					redirect('ingresoAdministrador');
				break;

				case 2:
					redirect('usuarioLogueado');
				break;
			}
		}
	}


	/*cierra una sesión activa*/
	public function cerrarSesion()

	{
		$this->session->sess_destroy();
		redirect('inicio');
	}

	/*muestra un mensaje de agradecimiento tras la compra*/
	public function mostrarAgradecimiento() {
		$this->load->model('ModeloCliente');
		$categorias['categorias']=$this->ModeloProducto->get_categoria();
		$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
		$usuario=$this->ModeloCliente->getPersona($this->session->userdata('idUsuario'));
		$datos['nombreUsuario']=$usuario->nombre;
		$titulo = array('titulo' => '¡Muchas Gracias!');
		$this->load->view('plantillas/encabezado',$titulo);
		$this->load->view('plantillas/barraNavegacionRegistrado');
		$this->load->view('plantillas/barraOpciones',$categorias);
		$this->load->view('contenido/agradecimientoCompra',$datos);//paso dos arrays como datos a la vista
		$this->load->view('plantillas/pie');
	}

	public function CambiarDatos()
	{
		/*muestra los criterios de control para cada campo*/
		$this->form_validation->set_rules('apellido','Apellido del usuario', 'required');
		$this->form_validation->set_rules('nombre', 'Nombre del usuario', 'required');            
		$this->form_validation->set_rules('mail', 'Email', 'required|valid_email|callback_controlMail|max_length[30]');   
		$this->form_validation->set_rules('dni', 'Dni', 'required|integer|is_unique[personas.dni]|max_length[30]'); 
 		$this->form_validation->set_rules('password', 'Contraseña','callback_controlarSimilitudes');
 		$this->form_validation->set_rules('passwordAntiguo', 'Contraseña', 'trim|callback_controlarPassword'); 
 		$this->form_validation->set_rules('telefono','Teléfono','required|integer');      
 		$this->form_validation->set_rules('passconf', 'Confirmar password');


 		/*muestra los mensajes de error para los campos inválidos*/
 		$this->form_validation->set_message('valid_email', 'El campo %s debe ser un mail válido');

 		$this->form_validation->set_message('controlMail', 'Ya existe un usuario con el mail indicado'); 

 		$this->form_validation->set_message('controlarPassword', 'El password colocado no pertenece a un usuario');  

 		$this->form_validation->set_message('controlarSimilitudes', 'Los password colocados no coinciden o tienen menos de 8 caracteres'); 
 
		$this->form_validation->set_message('integer', 'El campo %s debe poseer solo numeros enteros'); 
 
		$this->form_validation->set_message('required', 'El campo %s es obligatorio'); 
 		
 		$this->form_validation->set_message('controlarTelefono', 'El telefono debe ser sólo numérico'); 

		$this->form_validation->set_message('min_length', 'El campo %s debe contener como mínimo %d caracteres'); 

		$this->form_validation->set_message('max_length', 'El campo %s debe contener como máximo %d caracteres');

		$this->form_validation->set_message('matches', 'contraseñas no coinciden');

 		$this->form_validation->set_message('passconf', 'Las contraseñas no coinciden');

 		$this->form_validation->set_message('is_unique', 'El mail ya pertenece a otro usuario');


 		/*muestra lo que sucede si hay o no errores*/

 		 if ($this->form_validation->run() == FALSE) { 
 			//en caso de ser falso, vuelve a cargar la página
            $this->index(); 
 
        } else {       
        	
        	$this->insertar_cliente();
        }; 
	}


}