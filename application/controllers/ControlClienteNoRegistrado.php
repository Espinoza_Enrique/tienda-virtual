<?php
/*Permite manejar todo lo referentes a los clientes*/
defined('BASEPATH') OR exit('No direct script access allowed');

class ControlClienteNoRegistrado extends CI_Controller {

	function __construct() {
		parent::__construct();
		/*$this->load->model('ModeloProducto');*/
		/*restringe el acceso sólo a los usuarios registrados
		Debo agregar la lìnea if($this->session->userdata('login') en el cuerpo de cada función que debe realizarse sólo para usuarios registrados*/
		/*if(!$this->session->userdata('login')) {
			$this->index();
		};*/
	}
	/*Dependiendo dónde se haga clic, se mostrará uno u otro producto.*/
	public function visualizarProductos($idCategoria,$criterioOrdenamiento) {
		$titulo="";
		$datos['mostrarOrden']="";
		$datos['categoria']=$idCategoria;
		$datos['criterio']=$criterioOrdenamiento;
		switch($idCategoria) {
			case 1:
				$titulo=array('titulo'=>'Juegos de Mesa-Estrategia');
				break;
			case 2:
				$titulo=array('titulo'=>'Juegos de Mesa-Abstracto');
				break;
			case 3:
				$titulo=array('titulo'=>'Juegos de Mesa-Wargames');
				break;
			case 4:
				$titulo=array('titulo'=>'Yu-gi-oh!');
				$datos['mostrarOrden']="4";
				break;
			case 5:
				$titulo=array('titulo'=>'Magic: The Gathering');
				break;
			case 6:
				$titulo=array('titulo'=>'TCG: Pokemon');
				$datos['mostrarOrden']="6";
				break;
			case 7:
				$titulo=array('titulo'=>'Calabozos y Dragones');
				break;
			case 8:
				$titulo=array('titulo'=>'La llamada de Cthulhu');
				break;
			case 9:
				$titulo=array('titulo'=>'Juegos de Cartas Varios');
				break;
		}
		$datos['productos']=$this->ModeloProducto->get_producto_categoria($idCategoria,$criterioOrdenamiento);
		//$titulo = array('titulo' => 'Listado de Productos');
		$this->load->view('plantillas/encabezado',$titulo);
		/*cargo la vista inicial dependiendo el usuario*/
		switch($this->session->userdata('perfil')){
			case 1:
				$this->load->view('plantillas/barraNavegacionAdm');
			break;

			case 2:

				$this->load->view('plantillas/barraNavegacionRegistrado');
				$this->load->view('plantillas/barraOpciones');
			break;
			default:
				$this->load->view('plantillas/barraNavegacion');
				$this->load->view('plantillas/barraOpciones');
			break;

		};
		
		$this->load->view('plantillas/opcionesOrdenamiento',$datos);
		$this->load->view('contenido/vidrieraProductos');//paso dos arrays como datos a la vista
		$this->load->view('plantillas/pie');

	}

	/*ordena los productos según para que se muestren alfabéticamente o por precio*/
	public function ordenarProductos() {
		$criterioDeOrden=$this->input->post('seleccion');
		$categoria=$this->input->post('ordenar');
		$this->visualizarProductos($categoria,$criterioDeOrden);
	
	}

		/*muestra la descripcion de los productos*/
	public function mostrarDescripcion() {
		$id=$this->input->post("detalles");
		$datos['producto']=$this->ModeloProducto->buscarDetalles($id);
		if($this->session->userdata('perfil')==1){
				$titulo = array('titulo' => 'Detalles Producto');
				$this->load->view('plantillas/encabezado',$titulo);
				$this->load->view('plantillas/barraNavegacionRegistrado');
				$this->load->view('plantillas/barraOpciones');
				$this->load->view('plantillas/ventanaModalDetalles',$datos);
				$this->load->view('plantillas/pie');
		}
		else {
				$titulo = array('titulo' => 'Detalles Producto');
				$this->load->view('plantillas/encabezado',$titulo);
				$this->load->view('plantillas/barraNavegacion');
				$this->load->view('plantillas/barraOpciones');
				$this->load->view('plantillas/ventanaModalDetalles',$datos);
				$this->load->view('plantillas/pie');
			
		}

		
		
	}

}