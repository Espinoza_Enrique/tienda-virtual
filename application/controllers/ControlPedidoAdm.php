<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ControlPedidoAdm extends CI_Controller {
	function __construct() {
			parent::__construct();
			/*$this->load->library('pagination');
			$this->load->model('ModeloPedido');
			$this->load->model('ModeloProducto');*/
			if($this->session->userdata('perfil')!=1) {
				redirect('inicio');
			};
		}
	 

	/*muestra las compras totales del cliente*/
	public function mostrarComprasAdm() {
			$config['base_url']=base_url().'comprasTotales';
			$config['first_link']='Primero';
			$config['prev_link']='Anterior';
			$config['last_link']='Último';
			$config['next_link']='Siguiente';
			$config['full_tag_open']='<ul class="pagination justify-content-center">';
			$config['full_tag_close']='</ul>';
			$config['num_tag_open']='<li class="page-item page-link">';
			$config['num_tag_close']='</li>';
			$config['cur_tag_open']='<li class="active"><span class="page-link bg-primary text-white" >';
			$config['cur_tag_close']='<span ></span></span></li>';
			$config['prev_tag_open']='<li class="page-item page-link">';
			$config['prev_tag_close']='</li>';
			$config['next_tag_open']='<li class="page-item page-link">';
			$config['next_tag_close']='</li>';
			$config['last_tag_open']='<li class="page-item page-link">';
			$config['last_tag_close']='</li>';
			$config['first_tag_open']='<li class="page-item page-link">';
			$config['first_tag_close']='</li>';
			//echo $this->ModeloProducto->get_cantidad();die;
			$config['total_rows']=$this->ModeloPedido->getTotalCompras();
			$config['per_page']=8;
			$config['num_links']=8;
			$config['uri_segment']=2;
			$this->pagination->initialize($config);
			$page=$this->uri->segment(2);//define cuántos elementos mostrará según el valor del segmento 2

			$pedidos['pedidos']=$this->ModeloPedido->getComprasTotales(8,$page);
			$data = array('titulo' => 'Lista de Compras');
			$this->load->view('plantillas/encabezado',$data);
			$this->load->view('plantillas/barraNavegacionAdm');
			$this->load->view('plantillas/barraDeFiltroCompras');
			$this->load->view('contenido/vistaComprasAdm',$pedidos);
			$this->load->view('plantillas/pieAdm');
		
	}

	public function mostrarDetallesAdm() {
		$idPedido=$this->uri->segment(2);
		//$idPedido=$this->input->post('idPedido');
		$datos['compras']=$this->ModeloPedido->getDetallesAdm($idPedido);
		$data = array('titulo' => 'Detalles de Compras');
		$this->load->view('plantillas/encabezado',$data);
		$this->load->view('plantillas/barraNavegacionAdm');
		$this->load->view('plantillas/barraDeFiltroCompras');
		$this->load->view('contenido/vistaDetallesAdm',$datos);
		$this->load->view('plantillas/pieAdm');
	}

	/*permite controlar que las fechas existan*/
	public function controlarFechas() {
		/*muestra los criterios de control para cada campo*/
			$fechaInicio=$this->input->post('fechaInicio');
			$fechaFin=$this->input->post('fechaFin');
			$this->form_validation->set_rules('fechaInicio', 'Fecha inicio', 'required|callback_mostrarErrores');        
			$this->form_validation->set_rules('fechaFin', 'Fecha Fin', 'required');       
			$this->form_validation->set_message('required', 'El campo %s es obligatorio'); 
	 

	 		/*muestra lo que sucede si hay o no errores*/

	 		 if ($this->form_validation->run() == FALSE) { 
	 			//en caso de ser falso, vuelve a cargar la página
	 			redirect('comprasTotales');
	 			
	        } else {       
	        	
	        	$this->mostrarComprasFecha($fechaInicio,$fechaFin);
	        };
	}

	public function controlarId() {
		/*muestra los criterios de control para cada campo*/
			$id=$this->input->post('idOrden');
			$this->form_validation->set_rules('idOrden', 'Id Orden', 'required|callback_mostrarErroresId');              
			$this->form_validation->set_message('required', 'El campo %s es obligatorio'); 
	 

	 		/*muestra lo que sucede si hay o no errores*/

	 		 if ($this->form_validation->run() == FALSE) { 
	 			//en caso de ser falso, vuelve a cargar la página
	 			redirect('comprasTotales');
	 			
	        } else {       
	        	
	        	$this->mostrarComprasIdAdm($id);
	        };
	}

	/*permite validar las fechas*/
	public function validarFechas() {
		$fechaInicio=$this->input->post('fechaInicio');
		$fechaFin=$this->input->post('fechaFin');
		if($fechaInicio>$fechaFin) {
			$this->form_validation->set_message('fechaInicio', 'Fecha inicio','La fecha de inicio debe ser menor o igual que la fecha final'); 
			return false;
		}else {
			return true;
		}
	}

    /*permite ver uno de los posibles errores*/
    public function mostrarErrores() {
    	$datos=null;
    	$fechaInicio=$this->input->post('fechaInicio');
		$fechaFin=$this->input->post('fechaFin');
		if(empty($fechaInicio) or empty($fechaFin)) {
			redirect('fechasIncompatibles'. "/2"."/".$fechaInicio."/".$fechaFin) ;
			return false;
		};
		if($fechaInicio>$fechaFin) {
			
			redirect('fechasIncompatibles'. "/1"."/".$fechaInicio."/".$fechaFin) ;
			return false;
		}

		return true;
    }

      /*permite ver uno de los posibles errores*/
    public function mostrarErroresId() {
    	$datos=null;
    	$id=$this->input->post('idOrden');
	
		if(empty($id)) {
			redirect('facturasErroneas'. "/2") ;
			return false;
		};
		if(!is_numeric($id)) {
			
			redirect('facturasErroneas'. "/1") ;
			return false;
		};
		if(!$this->ModeloPedido->getCompraId($id)) {
			redirect('facturasErroneas'. "/3"."/".$id) ;
		}
		return true;
    }

	public function mostrarComprasFecha($fechaInicio,$fechaFin) {
		$registrosTotales=$this->ModeloPedido->getTotalEntreFechas($fechaInicio,$fechaFin);
		if($registrosTotales>0) {
			$config['base_url']=base_url().'filtrarPorFecha'."/".$fechaInicio."/".$fechaFin;
			$config['first_link']='Primero';
			$config['prev_link']='Anterior';
			$config['last_link']='Último';
			$config['next_link']='Siguiente';
			$config['full_tag_open']='<ul class="pagination justify-content-center">';
			$config['full_tag_close']='</ul>';
			$config['num_tag_open']='<li class="page-item page-link">';
			$config['num_tag_close']='</li>';
			$config['cur_tag_open']='<li class="active"><span class="page-link bg-primary text-white" >';
			$config['cur_tag_close']='<span ></span></span></li>';
			$config['prev_tag_open']='<li class="page-item page-link">';
			$config['prev_tag_close']='</li>';
			$config['next_tag_open']='<li class="page-item page-link">';
			$config['next_tag_close']='</li>';
			$config['last_tag_open']='<li class="page-item page-link">';
			$config['last_tag_close']='</li>';
			$config['first_tag_open']='<li class="page-item page-link">';
			$config['first_tag_close']='</li>';
			//echo $this->ModeloProducto->get_cantidad();die;
			$config['total_rows']=$registrosTotales;
			$config['per_page']=5;
			$config['num_links']=8;
			$config['uri_segment']=4;
			$this->pagination->initialize($config);
			$page=$this->uri->segment(4);//define cuántos elementos mostrará según el valor del segmento 2

			$pedidos['pedidos']=$this->ModeloPedido->filtrarPorFecha(5,$page,$fechaInicio,$fechaFin);
			$pedidos['fechaCompraInicio']=$fechaInicio;
			$pedidos['fechaCompraFin']=$fechaFin;
			$textoError['error']="Compras desde ".$fechaInicio." hasta ".$fechaFin;
			$data = array('titulo' => 'Lista de Compras');
			$this->load->view('plantillas/encabezado',$data);
			$this->load->view('plantillas/barraNavegacionAdm');
			$this->load->view('plantillas/barraDeFiltroCompras');
			$this->load->view('contenido/erroresDeFecha',$textoError);
			$this->load->view('contenido/vistaComprasAdm',$pedidos);
			$this->load->view('plantillas/pieAdm');
		}else {
			redirect('fechasIncompatibles'."/3"."/".$fechaInicio."/".$fechaFin);
		};
	}

		public function mostrarComprasId($Id) {
		
		if(!empty($this->ModeloPedido->getCompraId($id))) {
			$pedidos['pedidos']=$this->ModeloPedido->getCompraId($id);
			$data = array('titulo' => 'Lista de Compras');
			$this->load->view('plantillas/encabezado',$data);
			$this->load->view('plantillas/barraNavegacionAdm');
			$this->load->view('plantillas/barraDeFiltroCompras');
			$this->load->view('contenido/vistaComprasAdm',$pedidos);
			$this->load->view('plantillas/pieAdm');
		}else {
			redirect('facturasErroneas'."/3".$id);
		};
	}

	public function errorFecha($error) {
		$fechaInicio= $this->uri->segment(3);
		$fechaFin= $this->uri->segment(4);
		$textoError=null;
		$pedidos['pedidos']=$this->ModeloPedido->getPedidoAdm();
		$data = array('titulo' => 'Lista de Compras');
		switch($error) {
			case 1:
				$textoError['error']="Fecha Inicio=". $fechaInicio. "-". "Fecha Fin=" . $fechaFin. ".La fecha de inicio debe ser menor o igual a la fecha fin";
				break;
			case 2:
				$textoError['error']="Por favor, seleccione ambas fechas antes de filtrar";
				break;
			case 3:
				$textoError['error']="No hay compras entre las fechas: " . $fechaInicio. " y ". $fechaFin;
		}
		
		if($error==3) {
			$this->load->view('plantillas/encabezado',$data);
			$this->load->view('plantillas/barraNavegacionAdm');
			$this->load->view('plantillas/barraDeFiltroCompras');
			$this->load->view('contenido/erroresDeFecha',$textoError);
			$this->load->view('contenido/vistaComprasAdm',$pedidos);
			$this->load->view('plantillas/pieAdm');
		}else {
			$this->load->view('plantillas/encabezado',$data);
			$this->load->view('plantillas/barraNavegacionAdm');
			$this->load->view('plantillas/barraDeFiltroCompras');
			$this->load->view('contenido/erroresDeFecha',$textoError);
			$this->load->view('contenido/vistaComprasAdm',$pedidos);
			$this->load->view('plantillas/pieAdm');
		}
		
	}

	/*permite mostrar los datos de los productos por categoria en cuanto a la cantidades vendidas y el dinero recaudado*/
	public function mostrarCantidadesCat(){
		//$categorias['categoria']=$this->ModeloProducto->get_categoria();
		$data = array('titulo' => 'Cantidades y Recaudación');
		$producto['productos']=$this->calcularCantRecaudacion();
		$producto['unidadesTotales']=$this->calcularUnidadesTotales();
		$producto['recaudacionTotal']=$this->calcularRecaudacionTotal();
		//print_r($producto);die;
		$this->load->view('plantillas/encabezado',$data);
		$this->load->view('plantillas/barraNavegacionAdm');
		$this->load->view('contenido/vistaCantidadesRecaudacion',$producto);
		$this->load->view('plantillas/pieAdm');
		
		
	}

	/*permite mostrar los datos de los productos por categoria en cuanto a la cantidades vendidas y el dinero recaudado*/
	public function mostrarCantidadesSub(){
		$categoria=$this->uri->segment(2);
		//$categorias['categoria']=$this->ModeloProducto->get_categoria();
		$data = array('titulo' => 'Cantidades y Recaudación');
		$producto['productos']=$this->calcularSubRecaudacion($categoria);
		$producto['unidadesTotales']=$this->calcularSubUnidadesTotal($categoria);
		$producto['recaudacionTotal']=$this->calcularSubRecaudacionTotal($categoria);
		//print_r($producto);die;
		$this->load->view('plantillas/encabezado',$data);
		$this->load->view('plantillas/barraNavegacionAdm');
		$this->load->view('contenido/vistaSubRecaudacion',$producto);
		$this->load->view('plantillas/pieAdm');
		
		
	}

	/*obtengo la cantidad vendida y recaudación de los productos por categoria*/
	public function calcularCantRecaudacion(){
		/*la variable que almacenará los datos que serán pasados a la vista*/
		$datos=null;
		$resultado=array();
		/*busco primero todas las categorias*/
		$categorias=$this->ModeloProducto->get_categoria();
		/*obtengo los detalles de todas las compras*/
		$compras=$this->ModeloPedido->getDetallesTotales();
		/*dos acumuladores para las categorias y el precio*/
		
		$totalCat=0;
		$recaudado=0;
		$recaudacionTotal=0;
		$unidadesTotales=0;
		/*realizo la suma total de productos y datos*/
		foreach ($categorias as $elemento) {
			
			foreach($compras as $datos) {

				if($datos->IdCategoria==$elemento->id) {

					$totalCat=$totalCat+$datos->cantidad;
					$recaudado=$recaudado+$datos->precio;
					$unidadesTotales=$unidadesTotales+1;
					$recaudacionTotal=$recaudacionTotal+$datos->precio;
				}
			};
			$datos=array('id'=>$elemento->id,'categoria'=>$elemento->nombreCat,'total'=>$totalCat,'recaudacion'=>$recaudado);
			$totalCat=0;
			$recaudado=0;
			array_push($resultado, $datos);
			
		};
		return $resultado;

	}

	public function calcularRecaudacionTotal(){
		/*la variable que almacenará los datos que serán pasados a la vista*/
		
		/*busco primero todas las categorias*/
		$categorias=$this->ModeloProducto->get_categoria();
		/*obtengo los detalles de todas las compras*/
		$compras=$this->ModeloPedido->getDetallesTotales();
		/*dos acumuladores para las categorias y el precio*/
		
		$recaudacionTotal=0;
		
		/*realizo la suma total de productos y datos*/
		foreach ($categorias as $elemento) {
			
			foreach($compras as $datos) {

				if($datos->IdCategoria==$elemento->id) {
					$recaudacionTotal=$recaudacionTotal+$datos->precio;
				}
			};
			
			
		};
		return $recaudacionTotal;

	}

	public function calcularUnidadesTotales(){
		/*la variable que almacenará los datos que serán pasados a la vista*/
		
		/*busco primero todas las categorias*/
		$categorias=$this->ModeloProducto->get_categoria();
		/*obtengo los detalles de todas las compras*/
		$compras=$this->ModeloPedido->getDetallesTotales();
		/*dos acumuladores para las categorias y el precio*/
		
		$unidadesTotales=0;
		
		/*realizo la suma total de productos y datos*/
		foreach ($categorias as $elemento) {
			
			foreach($compras as $datos) {

				if($datos->IdCategoria==$elemento->id) {
					$unidadesTotales=$unidadesTotales+$datos->cantidad;
				}
			};
			
			
		};
		return $unidadesTotales;

	}

	/*obtengo la cantidad vendida y recaudación de los productos por subcategoria*/
	
	public function calcularSubRecaudacion($categoria){
		
		/*la variable que almacenará los datos que serán pasados a la vista*/
		$datos=null;
		$resultado=array();
		/*busco primero todas las categorias*/
		$subcategorias=$this->ModeloProducto->get_subcategoria($categoria);
		/*obtengo los detalles de todas las compras*/
		$compras=$this->ModeloPedido->getDetallesTotales();
		/*dos acumuladores para las categorias y el precio*/
		$totalCat=0;
		$recaudado=0;
		/*realizo la suma total de productos y datos*/
		foreach ($subcategorias as $elemento) {
			
			foreach($compras as $datos) {

				if($datos->subcategoria==$elemento->Id) {

					$totalCat=$totalCat+$datos->cantidad;
					$recaudado=$recaudado+$datos->precio;
				}
			};
			$datos=array('id'=>$elemento->Id,'subcategoria'=>$elemento->nombreSub,'total'=>$totalCat,'recaudacion'=>$recaudado);
			$totalCat=0;
			$recaudado=0;
			array_push($resultado, $datos);
			
		};
		return $resultado;

	}

		public function calcularSubUnidadesTotal($categoria){
		
		/*la variable que almacenará los datos que serán pasados a la vista*/
		/*busco primero todas las categorias*/
		$subcategorias=$this->ModeloProducto->get_subcategoria($categoria);
		/*obtengo los detalles de todas las compras*/
		$compras=$this->ModeloPedido->getDetallesTotales();
		/*dos acumuladores para las categorias y el precio*/
		$totalCat=0;
		/*realizo la suma total de productos y datos*/
		foreach ($subcategorias as $elemento) {
			
			foreach($compras as $datos) {

				if($datos->subcategoria==$elemento->Id) {

					$totalCat=$totalCat+$datos->cantidad;
					//$recaudado=$recaudado+$datos->precio;
				}
			};
			
			
		};
		return $totalCat;

	}

		public function calcularSubRecaudacionTotal($categoria){
		
		/*la variable que almacenará los datos que serán pasados a la vista*/
		/*busco primero todas las categorias*/
		$subcategorias=$this->ModeloProducto->get_subcategoria($categoria);
		/*obtengo los detalles de todas las compras*/
		$compras=$this->ModeloPedido->getDetallesTotales();
		/*dos acumuladores para las categorias y el precio*/
		$recaudado=0;
		/*realizo la suma total de productos y datos*/
		foreach ($subcategorias as $elemento) {
			
			foreach($compras as $datos) {

				if($datos->subcategoria==$elemento->Id) {

					$recaudado=$recaudado+$datos->precio;
					//$recaudado=$recaudado+$datos->precio;
				}
			};
			
			
		};
		return $recaudado;

	}

	/*permite mostrar la compra para un id específico*/
	public function mostrarComprasIdAdm($id){
		$factura['pedidos']=$this->ModeloPedido->getCompraId($id);
		$data = array('titulo' => 'Lista de Compras');
		$this->load->view('plantillas/encabezado',$data);
		$this->load->view('plantillas/barraNavegacionAdm');
		$this->load->view('plantillas/barraDeFiltroCompras');
		$this->load->view('contenido/vistaComprasAdm',$factura);
		$this->load->view('plantillas/pieAdm');
	}

	public function mostrarOrdenIdAdm(){
		$id=$this->input->post('idOrden');
		$this->mostrarComprasIdAdm($id);
	}
	
	public function errorDeFiltro($error) {
		$id= $this->uri->segment(3);
		$textoError=null;
		$pedidos['pedidos']=$this->ModeloPedido->getPedidoAdm();
		$data = array('titulo' => 'Filtrado por Id');
		switch($error) {
			case 1:
				$textoError['error']="Coloque por favor un valor entero positivo";
				break;
			case 2:
				$textoError['error']="Por favor coloque un valor de Id de factura para buscar";
				break;
			case 3:
				$textoError['error']="No existe una factura con el id " . $id;
				break;
		}
		
		
			$this->load->view('plantillas/encabezado',$data);
			$this->load->view('plantillas/barraNavegacionAdm');
			$this->load->view('plantillas/barraDeFiltroCompras');
			$this->load->view('contenido/erroresDeFecha',$textoError);
			$this->load->view('contenido/vistaComprasAdm',$pedidos);
			$this->load->view('plantillas/pieAdm');
		
	}
}