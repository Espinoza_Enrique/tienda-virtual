<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ControlPedido extends CI_Controller {
	function __construct() {
			parent::__construct();
			/*$this->load->model('ModeloPedido');
			$this->load->library('pagination');
			$this->load->model('ModeloProducto');*/
			if($this->session->userdata('perfil')!=2) {
				redirect('inicio');
			};
		}
	/*permite guardar el pedido de compra*/
	public function guardarPedido(){
		
		$vista=null;
		//$this->load->model('ModeloProducto');
		//creo un arreglo para almacenar los productos que se agregaron y los que no
		$productos=Array();
		$noAgregados=Array();
		$agregado=null;
		$datos=null;
		//creo la orden del pedido, es decir, el registro que se guardará en la cabecera de compra
		$orden_pedido=array('idCliente'=>$this->session->userdata('idUsuario'), 'orden_fecha'=>date('Y-m-d'));
		//lo guardo en la base de datos mediante el Modelo
		$this->ModeloPedido->guardarOrdenPedido($orden_pedido);
		//recupero el id de ese nuevo registro ingresado
		$id_orden=$this->db->insert_id();
		//recorro el contenido del carrito
		foreach($this->cart->contents() as $item) {
			//recupero los datos actuales de la base de datos para el producto
			$producto=$this->ModeloProducto->get_productoId($item['id']);
			//creo los datos para almacenar en la tabla de detalles de compra
			$detalle_pedido=array('idOrden'=>$id_orden,
					'idProducto'=>$item['id'],
					'cantidad'=>$item['qty'],
					'detalle_precio'=>$item['price']);
			//controlo que, para el producto analizado, la cantidad en el carrito que se va a comprar sea menor o igual al stock actual en 
			//la vase de datos
			if($producto->stock>=$item['qty']) {
					//echo "inserto el detalle";die;
				    //si es así, actualizo el stock del producto y guardo el detalles del producto en la base de datos
					//eso significa que dicho producto ha sido comprado
					$this->ModeloProducto->actualizarStock($item['id'],$item['qty']);
					$this->ModeloPedido->guardarDetallePedido($detalle_pedido);
					//cargo en el array creado qué producto fue comprado
					$agregado=array('nombre'=>$producto->nombre_producto);
					array_push($productos,$agregado);
				} else {
					//si el stock que se quiere comprar supera al stock disponible, no se guarda la información y se guarda ese producto
					//en un array aparte para informar posteriormente al usuario.
					$agregado=array('nombre'=>$producto->nombre_producto);
					array_push($noAgregados,$agregado);
				}
				
		}
		   //guardo los datos de los productos agregados y no agregados
			$datos['agregados']=$productos;
			$datos['noAgregados']=$noAgregados;
			//destruyo el carrito porque ya se realízó la compra
			$this->cart->destroy();
			//$this->load->model('ModeloCliente');
			//recupero los datos de la persona para mostrar su nombre y agradecerle la compra
			$usuario=$this->ModeloCliente->getPersona($this->session->userdata('idUsuario'));
			$datos['nombreUsuario']=$usuario->nombre;
			$titulo = array('titulo' => '¡Muchas Gracias!');
			$this->load->view('plantillas/encabezado',$titulo);
			$this->load->view('plantillas/barraNavegacionRegistrado');
			$this->load->view('contenido/agradecimientoCompra',$datos);//paso dos arrays como datos a la vista
			$this->load->view('plantillas/pie');
	}
	

	/*muestra los detalles de las una compra del cliente*/
	public function mostrarDetallesCompra() {
		$categorias['categorias']=$this->ModeloProducto->get_categoria();
		$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
		$datos=null;
		$id=$this->input->post('idPedido');
		$pedidos=$this->ModeloPedido->getDetallesPedido($id);
		$datos['datos']=$pedidos;
		$data = array('titulo' => 'Lista de Compras');
		$this->load->view('plantillas/encabezado',$data);
		$this->load->view('plantillas/barraNavegacionRegistrado');
		$this->load->view('plantillas/barraOpciones',$categorias);
		$this->load->view('contenido/vistaDetallesCompra',$datos);
		$this->load->view('plantillas/pie');
		
	}

	/*muestra las compras del cliente*/
	public function mostrarCompras() {
			//recupero los datos del cliente que tiene la sesión iniciada
			$id=$this->session->userdata('idUsuario');
			$categorias['categorias']=$this->ModeloProducto->get_categoria();
			$categorias['subcategorias']=$this->ModeloProducto->get_subcategorias();
			$config['base_url']=base_url().'mostrarCompras';
			$config['first_link']='Primero';
			$config['prev_link']='Anterior';
			$config['last_link']='Último';
			$config['next_link']='Siguiente';
			$config['full_tag_open']='<ul class="pagination justify-content-center">';
			$config['full_tag_close']='</ul>';
			$config['num_tag_open']='<li class="page-item page-link">';
			$config['num_tag_close']='</li>';
			$config['cur_tag_open']='<li class="active"><span class="page-link bg-primary text-white" >';
			$config['cur_tag_close']='<span ></span></span></li>';
			$config['prev_tag_open']='<li class="page-item page-link">';
			$config['prev_tag_close']='</li>';
			$config['next_tag_open']='<li class="page-item page-link">';
			$config['next_tag_close']='</li>';
			$config['last_tag_open']='<li class="page-item page-link">';
			$config['last_tag_close']='</li>';
			$config['first_tag_open']='<li class="page-item page-link">';
			$config['first_tag_close']='</li>';
			//echo $this->ModeloProducto->get_cantidad();die;
			//recupero la cantidad de compras realizadas por el usuario logueado
			$config['total_rows']=$this->ModeloPedido->getCantidadPedidos($id);
			//especifico las configuraciones de la paginación
			$config['per_page']=5;
			$config['num_links']=2;
			$config['uri_segment']=2;
			$this->pagination->initialize($config);
			$page=$this->uri->segment(2);//define cuántos elementos mostrará según el valor del segmento 2
			//recupera las compras según el id del usuario y la cantidad de páginas a mostrar por paginación
			$datos['pedidos']=$this->ModeloPedido->getPedidoCliente($id,5,$page);

		/*foreach($pedidos as $compras) {
			$productosComprados=$productosComprados+$compras->cantidad;
			$cantidadGastada=$cantidadGastada+($compras->cantidad*$compras->precio);
		}*/
		//$datos['unidades']=$productosComprados;
		//$datos['gastado']=$cantidadGastada;
		//print_r($datos);die;
		if(empty($datos['pedidos'])) {
			$this->load->model('ModeloCliente');
			$cliente['cliente']=$this->ModeloCliente->getPersona($this->session->userdata('idUsuario'));
			$data = array('titulo' => 'Sin Compras Aún');
			$this->load->view('plantillas/encabezado',$data);
			$this->load->view('plantillas/barraNavegacionRegistrado');
			$this->load->view('plantillas/barraOpciones',$categorias);
			$this->load->view('contenido/vistaClienteSinCompra',$cliente);
			$this->load->view('plantillas/pie');
		} else {
			$data = array('titulo' => 'Lista de Compras');
			$this->load->view('plantillas/encabezado',$data);
			$this->load->view('plantillas/barraNavegacionRegistrado');
			$this->load->view('plantillas/barraOpciones',$categorias);
			$this->load->view('contenido/vistaComprasCliente',$datos);
			$this->load->view('plantillas/pie');
		}
		
	}

	/*calculo la cantidad vendida y el precio total por categoria*/ 
	public function mostrarCantidadVendida(){
		
	}
	
}