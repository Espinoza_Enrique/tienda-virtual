/*fetch es una función que nos permite trabajar a través de promesas para hacer peticiones
donde podemos extraer un determinado código y mostrarlo. Este necesita ejecutarse sobre algún servidor o 
algún elemento ya que no puede ejecutarse directamente en consola. Este puede usarse bien en una browser
ya que estos tienen el motor de javascript. La función fetch() recibe como parámetro la url donde va hacer
la petición. La función fetch trabaja con promesas. Esto quiere decir que una vez realizada la operación
podemos capturar en la siguiente promesa la respuesta: La función llama a la API, llega al servidor y
recibe la respuesta a la solicitud. La respuesta se alamacena en un tipo de dato json con lo cual será
más fácil leer y manipular. La segunda promesa captura la respuesta y se le informa que muestre el
resultado en la consola.*/

/*fetch('https://jsonplaceholder.typicode.com/todos/1')
  .then(response => response.json())
  .then(data => console.log(data));*/

const obtenerUsuario=async()=>{
	try{
		const respuesta= await fetch('https://jsonplaceholder.typicode.com/todos/1')
		const datos= await respuesta.json()
		console.log(datos)
	}
	catch(e) {
		console.log(e)
	}
}

obtenerUsuario()